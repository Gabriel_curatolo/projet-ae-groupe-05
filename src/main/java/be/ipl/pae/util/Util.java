package be.ipl.pae.util;

public class Util {

  /**
   * verifie si c'est un objet.
   * 
   * @param object object
   * 
   * @return false si null, true sinon
   */
  public static boolean checkObjet(Object object) {
    if (object == null) {
      return false;
    }
    return true;
  }

  /**
   * verifie si c'est un string.
   * 
   * @param string string
   * 
   * @return false si non, sinon true
   */
  public static boolean checkString(String string) {
    checkObjet(string);
    if (string.matches("\\s*")) {
      return false;
    }
    return true;
  }

  /**
   * verifie si c'est un chiffre.
   * 
   * @param string string
   * 
   * @return false si non, sinon true
   */
  public static boolean checkNumerique(String string) {
    checkString(string);
    try {
      Long.parseLong(string);

    } catch (NumberFormatException exception) {
      return false;
    }
    return true;
  }

  /**
   * verifie si c'est un positif.
   * 
   * @param nombre nombre
   * 
   * @return false si non, sinon true
   */
  public static boolean checkPositif(double nombre) {
    if (nombre <= 0) {
      return false;
    }
    return true;

  }

  /**
   * verifie si c'est positif ou null.
   * 
   * @param nombre nombre
   * 
   * @return false si non, sinon true
   */
  public static boolean checkPositiveOuNul(double nombre) {
    if (nombre < 0) {
      return false;
    }
    return true;
  }

  /**
   * verifie si c'est vide.
   * 
   * @param champ le champ
   * 
   * @return false si non, sinon true
   */
  public static boolean checkEmpty(String champ) {
    if (champ.equals("")) {
      return true;
    } else {
      return false;
    }
  }

}
