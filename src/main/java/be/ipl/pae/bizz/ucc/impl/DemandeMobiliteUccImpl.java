package be.ipl.pae.bizz.ucc.impl;

import be.ipl.pae.bizz.bizz.PartenaireBizz;
import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.ucc.interfaces.DemandeMobiliteUcc;
import be.ipl.pae.dal.dao.interfaces.DemandeMobiliteDao;
import be.ipl.pae.dal.dao.interfaces.PartenaireDao;
import be.ipl.pae.dal.dao.interfaces.PaysDao;
import be.ipl.pae.dal.services.DalServices;

import java.io.IOException;
import java.util.List;

public class DemandeMobiliteUccImpl implements DemandeMobiliteUcc {

  private DemandeMobiliteDao demMobDao;
  private PaysDao paysDao;
  private DalServices dalServices;
  private PartenaireDao partDao;

  /**
   * constructeur de DemandeMobiliteUccImpl.
   * 
   * @param demMobDao la demande de mobilite dao
   * @param paysDao le pays dao
   * @param dalServices le dalservices
   */
  public DemandeMobiliteUccImpl(DemandeMobiliteDao demMobDao, PaysDao paysDao,
      DalServices dalServices, PartenaireDao partDao) {
    super();
    this.demMobDao = demMobDao;
    this.paysDao = paysDao;
    this.dalServices = dalServices;
    this.partDao = partDao;
  }

  @Override
  public List<DemandeMobiliteDto> visualiserSesDemandes(PersonneDto persDto) {
    try {
      dalServices.start();
      List<DemandeMobiliteDto> listeDeSesDemandes = demMobDao.visualiserSesDemandes(persDto);
      return listeDeSesDemandes;
    } finally {
      dalServices.commit();
    }

  }

  @Override
  public List<DemandeMobiliteDto> visualiserToutesLesDemandes() {
    try {
      dalServices.start();
      List<DemandeMobiliteDto> listeDemandes = demMobDao.visualiserToutesLesDemandes();
      return listeDemandes;
    } finally {
      dalServices.commit();
    }

  }

  @Override
  public DemandeMobiliteDto declarerDemandeMobilite(DemandeMobiliteDto demandeEncodageDonnees) {
    try {
      dalServices.startSansTransation();
      if (!demandeEncodageDonnees.getPartenaire().getNomComplet().equals("Autre")) {
        PartenaireDto partDto =
            partDao.getPartenaireParNomComplet(demandeEncodageDonnees.getPartenaire());
        demandeEncodageDonnees.setPartenaire(partDto);
        PartenaireBizz partBizz = (PartenaireBizz) partDto;
        partBizz.verifCodePartenaire(demandeEncodageDonnees);

      } else {
        demandeEncodageDonnees.setPartenaire(null);
      }
      DemandeMobiliteDto demandeRetour = demMobDao.declarerDemandeMobilites(demandeEncodageDonnees);
      return demandeRetour;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public List<PaysDto> getAllPays() {
    try {
      dalServices.startSansTransation();
      List<PaysDto> listePays = paysDao.getAllPays();
      return listePays;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public List<String> getProgrammes() {
    try {
      dalServices.startSansTransation();
      List<String> listeProgramme = paysDao.getProgrammesPays();
      return listeProgramme;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public PaysDto getIsoParPays(String nom) {
    try {
      dalServices.startSansTransation();
      PaysDto pays = paysDao.getIsoParPays(nom);
      return pays;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public List<PartenaireDto> getPartenaires() {
    try {
      dalServices.startSansTransation();
      List<PartenaireDto> listeCodes = partDao.getPartenaires();
      return listeCodes;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public List<DemandeMobiliteDto> getDemandeMobiParAnnee(DemandeMobiliteDto demDto) {
    try {
      dalServices.startSansTransation();
      List<DemandeMobiliteDto> demMob = demMobDao.getDemandeMobiParAnnee(demDto);
      return demMob;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public StringBuilder extraireFormatCsv() throws IOException {
    try {
      dalServices.startSansTransation();
      List<DemandeMobiliteDto> list = demMobDao.visualiserToutesLesDemandes();
      String virgule = "; ";
      String tab = "\n";
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ordre de candidature; Nom; Prenom; Departement; Ordre de preference; "
          + "Type de mobilite; Stage(SMS)/Academique(SMP); Quadrimestre; Partenaire;");
      for (DemandeMobiliteDto dem : list) {
        stringBuilder.append(tab);
        stringBuilder.append(String.valueOf(dem.getNumOrdreCandidature()));
        stringBuilder.append(virgule);
        stringBuilder.append(dem.getPersonne().getNom());
        stringBuilder.append(virgule);
        stringBuilder.append(dem.getPersonne().getPrenom());
        stringBuilder.append(virgule);
        stringBuilder.append(dem.getPartenaire().getDepartement());
        stringBuilder.append(virgule);
        stringBuilder.append(String.valueOf(dem.getOrdrePreference()));
        stringBuilder.append(virgule);
        stringBuilder.append(dem.getProgramme());
        stringBuilder.append(virgule);
        stringBuilder.append(dem.getCode());
        stringBuilder.append(virgule);
        stringBuilder.append(String.valueOf(dem.getQuadrimestre()));
        stringBuilder.append(virgule);
        stringBuilder.append(dem.getPartenaire().getNomComplet());
        stringBuilder.append(tab);
      }
      return stringBuilder;
    } finally {
      dalServices.close();
    }
  }
}
