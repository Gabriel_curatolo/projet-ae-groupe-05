package be.ipl.pae.bizz.ucc.impl;

import be.ipl.pae.bizz.bizz.AdresseBizz;
import be.ipl.pae.bizz.bizz.PartenaireBizz;
import be.ipl.pae.bizz.bizz.PersonneBizz;
import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.ucc.interfaces.PersonneUcc;
import be.ipl.pae.dal.dao.interfaces.AdresseDao;
import be.ipl.pae.dal.dao.interfaces.PartenaireDao;
import be.ipl.pae.dal.dao.interfaces.PaysDao;
import be.ipl.pae.dal.dao.interfaces.PersonneDao;
import be.ipl.pae.dal.services.DalServices;
import be.ipl.pae.exceptions.MdpException;

import java.util.ArrayList;
import java.util.List;

public class PersonneUccImpl implements PersonneUcc {


  private PersonneDao persDao;
  private DalServices dalServices;
  private AdresseDao adresseDao;
  private PaysDao paysDao;
  private PartenaireDao partDao;

  /**
   * constructeur de PersonneUccImpl.
   * 
   * @param persDao persDao
   * @param dalServices dalServices
   * @param adresseDao l'adresse dao
   */
  public PersonneUccImpl(PersonneDao persDao, DalServices dalServices, AdresseDao adresseDao,
      PaysDao paysDao, PartenaireDao partDao) {
    super();
    this.persDao = persDao;
    this.dalServices = dalServices;
    this.adresseDao = adresseDao;
    this.paysDao = paysDao;
    this.partDao = partDao;
  }



  @Override
  public PersonneDto checkConnexion(PersonneDto persDto) {
    try {
      PersonneBizz persBizz = (PersonneBizz) persDto;
      persBizz.checkPseudoConnexion();
      dalServices.startSansTransation();
      PersonneDto personne = persDao.getPersonneParPseudo(persDto.getPseudo());
      persBizz.checkNullConnexion(personne);
      if (((PersonneBizz) personne).checkPwd(persDto.getMotDePasse()) == true) {
        return personne;
      } else {
        throw new MdpException("Mot de passe invalide");
      }
    } finally {
      dalServices.close();
    }
  }

  @Override
  public PersonneDto inscription(PersonneDto dtoPersInscription) {
    try {
      PersonneBizz retBizz = (PersonneBizz) (dtoPersInscription);
      dalServices.startSansTransation();
      retBizz.checkInscription();
      retBizz.hashPwd();
      List<PersonneDto> list = persDao.getAllPersonnes();
      retBizz.checkEmailPseudoExistant(list);
      persDao.inscription(retBizz);
      return retBizz;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public PersonneDto encodageDonnees(PersonneDto dtoPersEncodageDonnees) {
    PersonneBizz persBizz = (PersonneBizz) dtoPersEncodageDonnees;
    persBizz.checkEncodageDonnees();
    AdresseBizz adrBizz = (AdresseBizz) dtoPersEncodageDonnees.getAdresse();
    adrBizz.checkEncodageAdresses();
    try {
      dalServices.start();
      PaysDto paysDto = paysDao.getIsoParPays(dtoPersEncodageDonnees.getNationalite().getNom());
      AdresseDto adrDto = adresseDao.encodageDonnees(dtoPersEncodageDonnees.getAdresse());
      dtoPersEncodageDonnees.setAdresse(adrDto);
      dtoPersEncodageDonnees.setNationalite(paysDto);
      PersonneDto persEncodageBon = persDao.encodageDonnees(dtoPersEncodageDonnees);
      return persEncodageBon;
    } finally {
      dalServices.commit();

    }
  }

  @Override
  public List<PersonneDto> visualiserTousLesUtilisateurs() {
    try {
      dalServices.startSansTransation();
      List<PersonneDto> list = persDao.getAllPersonnes();
      return list;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public PersonneDto visualiserUnUtilisateur(PersonneDto persDto) {
    try {
      dalServices.startSansTransation();
      PersonneDto ret = persDao.visualiserUnUtilisateur(persDto);
      return ret;
    } finally {
      dalServices.close();
    }
  }


  @Override
  public PersonneDto getPersonneById(PersonneDto persDto) {
    try {
      dalServices.startSansTransation();
      PersonneDto personneDto = persDao.getPersonneParId(persDto);
      return personneDto;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public PersonneDto augmenterProfesseur(PersonneDto persDto) {
    try {
      dalServices.startSansTransation();
      PersonneDto dto = persDao.getPersonneParEmail(persDto.getEmail());
      PersonneBizz persBizz = (PersonneBizz) dto;
      if (persBizz.checkAugmenterProf()) {
        dto.setRole("p");
      }
      PersonneDto persDtoRet = persDao.augmenterProfesseur(dto);
      return persDtoRet;
    } finally {
      dalServices.close();
    }

  }


  @Override
  public PersonneDto getDonnees(PersonneDto persDto) {
    try {
      dalServices.startSansTransation();
      PersonneDto dto = persDao.getPersonneParId(persDto);
      if (dto.getAdresse().getIdAdresse() == 0) {
        return null;
      }
      return dto;
    } finally {
      dalServices.close();
    }

  }


  @Override
  public List<PartenaireDto> recherchePartenaires(PartenaireDto partenaire) {
    try {
      dalServices.startSansTransation();
      PartenaireBizz partBizz = (PartenaireBizz) partenaire;
      partBizz.checkRecherchePartenaire();
      List<PartenaireDto> list = new ArrayList<PartenaireDto>();
      if (partenaire.getNomComplet().equals("") && partenaire.getPays().getNom().equals("")
          && !partenaire.getAdresse().getVille().equals("")) {
        list = partDao.getPartenairesParVille(partenaire);
      } else if (partenaire.getNomComplet().equals("")
          && partenaire.getAdresse().getVille().equals("")
          && !partenaire.getPays().getNom().equals("")) {
        list = partDao.getPartenairesParNomPays(partenaire);
      } else if (partenaire.getAdresse().getVille().equals("")
          && partenaire.getPays().getNom().equals("") && !partenaire.getNomComplet().equals("")) {
        list = partDao.getPartenairesParNom(partenaire);
      } else if (partenaire.getNomComplet().equals("")
          && !partenaire.getAdresse().getVille().equals("")
          && !partenaire.getPays().getNom().equals("")) {
        list = partDao.getPartenairesParVilleEtPays(partenaire);
      } else if (partenaire.getPays().getNom().equals("")
          && !partenaire.getAdresse().getVille().equals("")
          && !partenaire.getNomComplet().equals("")) {
        list = partDao.getPartenairesParNomEtVille(partenaire);
      } else if (partenaire.getAdresse().getVille().equals("")
          && !partenaire.getNomComplet().equals("") && !partenaire.getPays().getNom().equals("")) {
        list = partDao.getPartenairesParNomEtPays(partenaire);
      } else if (!partenaire.getNomComplet().equals("") && !partenaire.getPays().getNom().equals("")
          && !partenaire.getAdresse().getVille().equals("")) {
        list = partDao.getPartenairesParNomEtPaysEtVille(partenaire);
      }
      return list;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public List<PersonneDto> recherchePersonnes(PersonneDto pers) {
    try {
      dalServices.startSansTransation();
      PersonneBizz persBizz = (PersonneBizz) pers;
      persBizz.checkRecherchePersonnes();
      List<PersonneDto> list = new ArrayList<PersonneDto>();
      if (pers.getPrenom().equals("") && !pers.getNom().equals("")) {
        list = persDao.rechercheNom(pers);
      } else if (pers.getNom().equals("") && !pers.getPrenom().equals("")) {
        list = persDao.recherchePrenom(pers);
      } else if (!pers.getNom().equals("") && !pers.getPrenom().equals("")) {
        list = persDao.rechercheNomPrenom(pers);
      }
      return list;
    } finally {
      dalServices.close();
    }
  }


  @Override
  public List<PersonneDto> completionNomPrenomEtudiantRecherche() {
    try {
      dalServices.startSansTransation();
      List<PersonneDto> list = persDao.getEtudiants();
      return list;
    } finally {
      dalServices.close();
    }
  }


  @Override
  public List<PartenaireDto> completionNomPartenairePaysVilleEtudiantRecherche() {
    try {
      dalServices.startSansTransation();
      List<PartenaireDto> list = partDao.getPartenaires();
      return list;
    } finally {
      dalServices.close();
    }
  }
}
