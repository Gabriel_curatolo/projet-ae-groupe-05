package be.ipl.pae.bizz.ucc.interfaces;

import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PersonneDto;

import java.util.List;

public interface MobiliteEtudianteUcc {

  List<MobiliteEtudianteDto> visualiserToutesLesMobilites();

  MobiliteEtudianteDto confirmerDemandeMobilite(MobiliteEtudianteDto mobiDto);

  List<MobiliteEtudianteDto> getMobilitesParEtudiant(PersonneDto persCo);

  MobiliteEtudianteDto annulerMobilite(MobiliteEtudianteDto mobDto);

  MobiliteEtudianteDto indiquerEnvoieDemandePaiement(MobiliteEtudianteDto mobDto);

  MobiliteEtudianteDto getPartenaireParEtudiant(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto indiquerEnvoieDeuxiemeDemandePaiement(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto indiquerDonneesMobiEncodeesExternes(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto confirmerDocumentsEtudiantRemplis(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto preciserDocumentsEtudiantRetour(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto indiquerMobiliteEnRemboursement(MobiliteEtudianteDto mobiDto);

  List<MobiliteEtudianteDto> getMobilitesNonAnnulees();

  List<MobiliteEtudianteDto> getListePaiements(MobiliteEtudianteDto mobiDto);

  List<MobiliteEtudianteDto> rechercheMobi(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto preciserEnvoyerdemandeHe(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto getMobiParId(MobiliteEtudianteDto mobiDto);

}
