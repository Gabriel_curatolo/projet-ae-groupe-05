package be.ipl.pae.bizz.ucc.interfaces;

import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PersonneDto;

import java.util.List;

public interface PersonneUcc {

  /*
   * verifie si le pseudo existe dans la db et si le mot de passe est correct
   * 
   * @param pseudo, mdp de la personne
   * 
   * @return une personne remplie si la connexion reussie et null si la connexion est ratée
   */
  PersonneDto checkConnexion(PersonneDto persDto);

  /*
   * verifie si le pseudo et l'email sont unique
   * 
   * @param persoInscription remplis dans AppServlet
   * 
   * une personne avec un email si l'email existe, une personne avec un pseudo si le pseudo existe,
   * null si aucun existe dans la db
   */
  PersonneDto inscription(PersonneDto persInscription);

  /*
   * appelle l'encodage de donnee dans le DAL
   * 
   * @param persEncodageDonnees, adrDto remplis dans AppServlet
   * 
   * une personne deja existante en db qui sera completee avec des informations et une adresse
   */
  PersonneDto encodageDonnees(PersonneDto persEncodageDonnees);


  List<PersonneDto> visualiserTousLesUtilisateurs();

  PersonneDto visualiserUnUtilisateur(PersonneDto persDto);

  PersonneDto getPersonneById(PersonneDto persDto);

  PersonneDto augmenterProfesseur(PersonneDto persDto);

  PersonneDto getDonnees(PersonneDto persDto);

  List<PartenaireDto> recherchePartenaires(PartenaireDto partenaire);

  List<PersonneDto> recherchePersonnes(PersonneDto pers);

  List<PersonneDto> completionNomPrenomEtudiantRecherche();

  List<PartenaireDto> completionNomPartenairePaysVilleEtudiantRecherche();

}
