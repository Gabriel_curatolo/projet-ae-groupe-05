package be.ipl.pae.bizz.ucc.interfaces;

import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;

import java.io.IOException;
import java.util.List;

public interface DemandeMobiliteUcc {

  List<DemandeMobiliteDto> visualiserSesDemandes(PersonneDto persDto);

  DemandeMobiliteDto declarerDemandeMobilite(DemandeMobiliteDto demandeEncodageDonnees);

  List<DemandeMobiliteDto> visualiserToutesLesDemandes();

  List<PaysDto> getAllPays();

  List<String> getProgrammes();

  PaysDto getIsoParPays(String nom);

  List<PartenaireDto> getPartenaires();

  List<DemandeMobiliteDto> getDemandeMobiParAnnee(DemandeMobiliteDto demDto);

  StringBuilder extraireFormatCsv() throws IOException;
}
