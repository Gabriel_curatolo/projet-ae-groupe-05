package be.ipl.pae.bizz.ucc.impl;

import be.ipl.pae.bizz.bizz.AdresseBizz;
import be.ipl.pae.bizz.bizz.MobiliteEtudianteBizz;
import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.ucc.interfaces.MobiliteEtudianteUcc;
import be.ipl.pae.dal.dao.interfaces.AdresseDao;
import be.ipl.pae.dal.dao.interfaces.DemandeMobiliteDao;
import be.ipl.pae.dal.dao.interfaces.MobiliteEtudianteDao;
import be.ipl.pae.dal.dao.interfaces.PartenaireDao;
import be.ipl.pae.dal.dao.interfaces.PaysDao;
import be.ipl.pae.dal.services.DalServices;

import java.util.ArrayList;
import java.util.List;

public class MobiliteEtudianteUccImpl implements MobiliteEtudianteUcc {

  private MobiliteEtudianteDao mobEtDao;
  private DalServices dalServices;
  private DemandeMobiliteDao demMobDao;
  private PartenaireDao partDao;
  private PaysDao paysDao;
  private AdresseDao adrDao;

  /**
   * constructeur de MobiliteEtudianteUccImpl.
   * 
   * @param mobEtDao la mobilite dao
   * @param dalServices le dalservices
   * @param demMobDao la demande mobilite dao
   */
  public MobiliteEtudianteUccImpl(MobiliteEtudianteDao mobEtDao, DalServices dalServices,
      DemandeMobiliteDao demMobDao, PartenaireDao partDao, PaysDao paysDao, AdresseDao adrDao) {
    super();
    this.mobEtDao = mobEtDao;
    this.dalServices = dalServices;
    this.demMobDao = demMobDao;
    this.partDao = partDao;
    this.paysDao = paysDao;
    this.adrDao = adrDao;
  }

  @Override
  public List<MobiliteEtudianteDto> visualiserToutesLesMobilites() {
    try {
      dalServices.startSansTransation();
      List<MobiliteEtudianteDto> listeDemandes = mobEtDao.visualiserToutesLesMobilites();
      return listeDemandes;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public MobiliteEtudianteDto confirmerDemandeMobilite(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.start();
      MobiliteEtudianteBizz mobBizz = (MobiliteEtudianteBizz) mobiDto;
      mobBizz.checkConfirmerMobilite();
      if (mobiDto.getPartenaire().getIdPartenaire() == -1) {
        PartenaireDto partDto2 = partDao.getPartenaireParNomComplet(mobiDto.getPartenaire());
        mobiDto.setPartenaire(partDto2);
        mobiDto.getDemande().setPartenaire(partDto2);
      }
      if (mobiDto.getPartenaire().getIdPartenaire() == 0) {
        AdresseBizz adrBizz = (AdresseBizz) mobiDto.getPartenaire().getAdresse();
        adrBizz.checkEncodageAdresses();
        AdresseDto adrDto1 = adrDao.encodageDonnees(mobiDto.getPartenaire().getAdresse());
        mobiDto.getPartenaire().setAdresse(adrDto1);
        PaysDto paysDto = paysDao.getIsoParPays(mobiDto.getPartenaire().getPays().getNom());
        mobiDto.getPartenaire().setPays(paysDto);
        PartenaireDto partDto1 = partDao.introduirePartenaire(mobiDto.getPartenaire());
        mobiDto.setPartenaire(partDto1);
        mobiDto.getDemande().setPartenaire(partDto1);
      }
      List<MobiliteEtudianteDto> list = mobEtDao.getMobilitesParEtudiant(mobiDto.getPersonne());
      mobBizz.checkMobiliteDejaConfirmee(list);
      demMobDao.confirmerDemandeMobilite(mobiDto.getDemande());
      mobEtDao.creerMobiliteEtudiante(mobiDto);
      return mobiDto;
    } finally {
      dalServices.commit();
    }
  }

  @Override
  public List<MobiliteEtudianteDto> getMobilitesParEtudiant(PersonneDto persCo) {
    try {
      dalServices.startSansTransation();
      List<MobiliteEtudianteDto> mobDto = mobEtDao.getMobilitesParEtudiant(persCo);
      return mobDto;
    } finally {
      dalServices.close();
    }

  }

  @Override
  public MobiliteEtudianteDto annulerMobilite(MobiliteEtudianteDto mobDto) {
    try {
      dalServices.startSansTransation();
      MobiliteEtudianteDto dto = mobEtDao.annulerMobilite(mobDto);
      return dto;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public MobiliteEtudianteDto indiquerEnvoieDemandePaiement(MobiliteEtudianteDto mobDto) {
    try {
      dalServices.startSansTransation();
      MobiliteEtudianteDto dto = mobEtDao.indiquerEnvoieDemandePaiement(mobDto);
      return dto;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public MobiliteEtudianteDto getPartenaireParEtudiant(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.startSansTransation();
      MobiliteEtudianteDto dto = mobEtDao.getPartenaireParEtudiant(mobiDto);
      return dto;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public MobiliteEtudianteDto indiquerEnvoieDeuxiemeDemandePaiement(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.startSansTransation();
      MobiliteEtudianteDto mob = mobEtDao.indiquerEnvoieDeuxiemeDemandePaiement(mobiDto);
      return mob;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public MobiliteEtudianteDto indiquerDonneesMobiEncodeesExternes(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.startSansTransation();
      MobiliteEtudianteBizz mobBizz = (MobiliteEtudianteBizz) mobiDto;
      mobBizz.checkEncodageAilleurs();
      mobEtDao.indiquerDonneesMobiEncodeesExternes(mobiDto);
      return mobiDto;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public MobiliteEtudianteDto confirmerDocumentsEtudiantRemplis(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.startSansTransation();

      MobiliteEtudianteBizz mobBizz = (MobiliteEtudianteBizz) mobiDto;
      mobBizz.checkSiContratBourseSuisse();
      mobBizz.checkSiErasmusPreuveLinguistique();

      if (mobBizz.checkAPayerSuisse()) {
        mobiDto.setEtat("en_cours");
        mobEtDao.confirmerDocumentsEtudiantRemplis(mobiDto);
      } else if (mobBizz.checkEnPreparation()) {
        mobiDto.setEtat("en_preparation");
        mobEtDao.confirmerDocumentsEtudiantRemplis(mobiDto);
      } else {
        mobiDto.setEtat("a_payer");
        mobEtDao.confirmerDocumentsEtudiantRemplis(mobiDto);
      }
      return mobiDto;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public MobiliteEtudianteDto preciserDocumentsEtudiantRetour(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.startSansTransation();
      MobiliteEtudianteBizz mobBizz = (MobiliteEtudianteBizz) mobiDto;
      mobBizz.checkSiErasmusPreuveLinguistique();
      if (mobBizz.checkTermineSuisse()) {
        mobiDto.setEtat("Termine");
        mobEtDao.preciserDocumentsEtudiantRetourAPayerSolde(mobiDto);
      } else if (mobBizz.checkPreciserDocumentsRetour()) {
        mobEtDao.preciserDocumentsEtudiantRetour(mobiDto);
      } else {
        mobiDto.setEtat("a_payer_solde");
        mobEtDao.preciserDocumentsEtudiantRetourAPayerSolde(mobiDto);
      }
      return mobiDto;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public MobiliteEtudianteDto indiquerMobiliteEnRemboursement(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.startSansTransation();
      mobEtDao.changementEtat(mobiDto);
      return mobiDto;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public List<MobiliteEtudianteDto> getMobilitesNonAnnulees() {
    try {
      dalServices.startSansTransation();
      List<MobiliteEtudianteDto> listeDemandes = mobEtDao.getMobilitesNonAnnulees();
      return listeDemandes;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public List<MobiliteEtudianteDto> getListePaiements(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.startSansTransation();
      List<MobiliteEtudianteDto> listeMobis = mobEtDao.getListePaiements(mobiDto);
      System.out.println(listeMobis.size());
      return listeMobis;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public List<MobiliteEtudianteDto> rechercheMobi(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.startSansTransation();
      MobiliteEtudianteBizz mobBizz = (MobiliteEtudianteBizz) mobiDto;
      List<MobiliteEtudianteDto> listeMobis = new ArrayList<MobiliteEtudianteDto>();
      mobBizz.checkVideRechercheMobi();
      if (mobiDto.getEtat().equals("") && mobiDto.getDemande().getAnneeAcademique() != 0) {
        listeMobis = mobEtDao.getMobiParAnneeAcademique(mobiDto);
      } else if (mobiDto.getDemande().getAnneeAcademique() == 0 && !mobiDto.getEtat().equals("")) {
        listeMobis = mobEtDao.getMobiParEtat(mobiDto);
      } else {
        listeMobis = mobEtDao.getMobiParEtatEtAnneeAcademique(mobiDto);
      }
      return listeMobis;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public MobiliteEtudianteDto preciserEnvoyerdemandeHe(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.startSansTransation();
      mobEtDao.indiquerEnvoieDemandePaiement(mobiDto);
      mobiDto.setNumVersion(mobiDto.getNumVersion() + 1);
      mobEtDao.changementEtat(mobiDto);
      return mobiDto;
    } finally {
      dalServices.close();
    }
  }

  @Override
  public MobiliteEtudianteDto getMobiParId(MobiliteEtudianteDto mobiDto) {
    try {
      dalServices.startSansTransation();
      MobiliteEtudianteDto mobi = mobEtDao.getMobiParId(mobiDto);
      return mobi;
    } finally {
      dalServices.close();
    }
  }
}
