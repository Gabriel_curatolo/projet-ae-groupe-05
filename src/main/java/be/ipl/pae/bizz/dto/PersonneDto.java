package be.ipl.pae.bizz.dto;

import java.time.LocalDate;

public interface PersonneDto {

  /*
   * Renvoie l'id de la personne
   * 
   * @return idPersonne
   */
  int getIdPersonne();

  /*
   * Renvoie l'adresse
   * 
   * @return adresse
   */
  AdresseDto getAdresse();

  /*
   * Renvoie la nationalite
   * 
   * @return nationalite
   */
  PaysDto getNationalite();

  /*
   * Renvoie le nom
   * 
   * @return nom
   */
  String getNom();

  /*
   * Renvoie le prenom
   * 
   * @return prenom
   */
  String getPrenom();

  /*
   * Renvoie le mot de passe
   * 
   * @return motDePasse
   */
  String getMotDePasse();

  /*
   * Renvoie l'email
   * 
   * @return email
   */
  String getEmail();

  /*
   * Renvoie le telephone
   * 
   * @return telephone
   */
  String getTelephone();

  /*
   * Renvoie la date d'inscription
   * 
   * @return dateInscription
   */
  LocalDate getDateInscription();

  /*
   * Renvoie le role
   * 
   * @return role
   */
  String getRole();

  /*
   * Renvoie la civilite
   * 
   * @return civilite
   */
  String getCivilite();

  /*
   * Renvoie la date de naissance
   * 
   * @return dateNaissance
   */
  String getDateNaissance();

  /*
   * Renvoie le sexe
   * 
   * @return sexe
   */
  String getSexe();

  /*
   * Renvoie le numero de compte
   * 
   * @return noCompte
   */
  String getNoCompte();

  /*
   * Renvoie le nombre d'annees reussies
   * 
   * @return nbAnneeReussie
   */
  int getNbAnneeReussies();

  /*
   * Renvoie le titulaire du compte
   * 
   * @return titulaireCompte
   */
  String getTitulaireCompte();

  /*
   * Renvoie la banque
   * 
   * @return banque
   */
  String getBanque();

  /*
   * Renvoie le code bic
   * 
   * @return codeBic
   */
  String getCodeBic();

  /*
   * Renvoie le departement
   * 
   * @return departement
   */
  String getDepartement();

  /*
   * Renvoie le pseudo
   * 
   * @return pseudo
   */
  String getPseudo();

  /*
   * Renvoie le numero de version
   * 
   * @return num_version
   */
  int getNumVersion();


  /*
   * set la version d'une personne
   * 
   * @parem numVersion le numVersion de la personne
   */
  void setNumVersion(int numVersion);


  /*
   * Set l'id de la personne
   * 
   * @param idPersonne l'id de la personne
   */

  void setIdPersonne(int idPersonne);

  /*
   * Set l'adresse
   * 
   * @param adresse l'adresse de la personne
   */
  void setAdresse(AdresseDto adrDto);

  /*
   * Set la nationalite
   * 
   * @param nationalite la nationalite de la personne
   */
  void setNationalite(PaysDto nationalite);

  /*
   * Set le pseudo
   * 
   * @param pseudo le pseudo de la personne
   */
  void setPseudo(String pseudo);

  /*
   * Set le nom
   * 
   * @param nom le nom de la personne
   */
  void setNom(String nom);

  /*
   * Set le prenom
   * 
   * @param prenom le prenom de la personne
   */
  void setPrenom(String prenom);

  /*
   * Set le mot de passe
   * 
   * @param motDePasse le mot de passe de la personne
   */
  void setMotDePasse(String motDePasse);

  /*
   * Set l'email
   * 
   * @param email l'email de la personne
   */
  void setEmail(String email);

  /*
   * Set le telephone
   * 
   * @param telephone le telephone de la personne
   */
  void setTelephone(String telephone);

  /*
   * Set la date d'inscription
   * 
   * @param dateInscription la date d'inscription de la personne
   */
  void setDateInscription(LocalDate dateInscription);

  /*
   * Set le role
   * 
   * @param role le role de la personne
   */
  void setRole(String role);

  /*
   * Set la civilite
   * 
   * @param civilite la civilite de la personne
   */
  void setCivilite(String civilite);

  /*
   * Set la date de naissance
   * 
   * @param dateNaissance la date de naissance de la personne
   */
  void setDateNaissance(String dateNaissance);

  /*
   * Set le sexe
   * 
   * @param sexe le sexe de la personne
   */
  void setSexe(String sexe);

  /*
   * Set le numero de compte
   * 
   * @param noCompte le numero de compte de la personne
   */
  void setNoCompte(String noCompte);

  /*
   * Set le nombre d'annees reussies
   * 
   * @param nbAnneeReussies le nombre d'annees reussies de la personne
   */
  void setNbAnneeReussies(int nbAnneeReussies);

  /*
   * Set le titulaire du compte
   * 
   * @param titulaireCompte le titulaire du compte de la personne
   */
  void setTitulaireCompte(String titulaireCompte);

  /*
   * Set la banque
   * 
   * @param banque la banque de la personne
   */
  void setBanque(String banque);

  /*
   * Set le code bic
   * 
   * @param codeBic le code bic de la personne
   */
  void setCodeBic(String codeBic);

  /*
   * Set le departement
   * 
   * @param departement le departement de la personne
   */
  void setDepartement(String departement);
}
