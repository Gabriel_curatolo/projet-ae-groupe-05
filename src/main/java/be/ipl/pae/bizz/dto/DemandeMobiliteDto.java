package be.ipl.pae.bizz.dto;

import java.time.LocalDate;

public interface DemandeMobiliteDto {

  /*
   * renvoie l'id de la demande
   * 
   * @return l'id de la demande
   */
  int getIdDemande();

  /*
   * renvoie le partenaire
   * 
   * @return le partenaire
   */
  PartenaireDto getPartenaire();

  /*
   * renvoie la personne
   * 
   * @return la personne
   */
  PersonneDto getPersonne();

  /*
   * renvoie le pays
   * 
   * @return le pays
   */
  PaysDto getPays();

  /*
   * renvoie si la demande est confirmee
   * 
   * @return true si la demande est confirmee, false sinon
   */
  boolean isConfirme();

  /*
   * renvoie la date d'introduction de la demande
   * 
   * @return la date d'introduction de la demande
   */
  LocalDate getDateIntroduction();

  /*
   * renvoie le programme
   * 
   * @return le programme
   */
  String getProgramme();

  /*
   * renvoie le code de la demande
   * 
   * @return le code de la demande
   */
  String getCode();

  /*
   * renvoie le quadrimestre
   * 
   * @return le quadrimestre
   */
  int getQuadrimestre();

  /*
   * renvoie le numero dans l'ordre de preference
   * 
   * @return le numero dans l'ordre de preference
   */
  int getOrdrePreference();

  /*
   * renvoie l'annee academique
   * 
   * @return l'annee academique
   */
  int getAnneeAcademique();

  /*
   * Set l'id de la demande
   * 
   * @param idDemande l'id de la demande
   */
  void setIdDemande(int idDemande);

  /*
   * Set le partenaire de la demande
   * 
   * @param partenaire le partenaire de la demande
   */
  void setPartenaire(PartenaireDto partenaire);

  /*
   * Set l'id de la personne
   * 
   * @param idPersonne l'id de la personne qui fait la demande
   */
  void setPersonne(PersonneDto personne);

  /*
   * Set le pays
   * 
   * @param pays le pays de la demande
   */
  void setPays(PaysDto pays);

  /*
   * Set la confirmation
   * 
   * @param confirme la confirmation ou non de la demande
   */
  void setConfirme(boolean confirme);

  /*
   * Set la date d'introduction de la demande
   * 
   * @param dateIntroduction la date d'introduction de la demande
   */
  void setDateIntroduction(LocalDate date);

  /*
   * Set le programme de la demande
   * 
   * @param programme le programme de la demande
   */
  void setProgramme(String programme);

  /*
   * Set le code de la demande
   * 
   * @param code le code de la demande
   */
  void setCode(String code);

  /*
   * Set quadrimestre de la demande
   * 
   * @param quadrimestre le quadrimestre de la demande
   */
  void setQuadrimestre(int quadrimestre);

  /*
   * Set l'ordre de preference
   * 
   * @param ordrePreference l'ordre de preference de la demande
   */
  void setOrdrePreference(int ordrePreference);

  /*
   * Set l'annee academique de la demande
   * 
   * @param anneeAcademique l'annee academique de la demande
   */
  void setAnneeAcademique(int anneeAcademique);

  int getNumVersion();

  void setNumVersion(int numVersion);

  int getNumOrdreCandidature();

  void setNumOrdreCandidature(int numOrdreCandidature);


}
