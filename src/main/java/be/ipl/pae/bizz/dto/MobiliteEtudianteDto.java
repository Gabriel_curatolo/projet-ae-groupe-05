package be.ipl.pae.bizz.dto;

public interface MobiliteEtudianteDto {

  int getIdMobiliteEtudiant();

  DemandeMobiliteDto getDemande();

  PersonneDto getPersonne();

  PartenaireDto getPartenaire();

  String getEtat();

  int getQuadrimestre();

  boolean isEnvoieDemandePaiement2();

  boolean isContratBourse();

  boolean isConventionStageEtude();

  boolean isCharteEtudiant();

  boolean isPreuveTestLinguistique();

  boolean isDocumentEngagement();

  boolean isAttestationSejourRecu();

  boolean isReleveNoteRecu();

  boolean isPreuveTestLinguistiqueRetour();

  boolean isRapportFinal();

  String getDernierEtat();

  String getRaisonsAnnulationRefus();

  boolean getCertificatStageRecu();

  void setIdMobiliteEtudiant(int idMobiliteEtudiant);

  void setDemande(DemandeMobiliteDto demande);

  void setPersonne(PersonneDto personne);

  void setPartenaire(PartenaireDto partenaire);

  void setEtat(String etat);

  void setQuadrimestre(int quadrimestre);

  void setEnvoieDemandePaiement2(boolean envoieDemandePaiement2);

  void setContratBourse(boolean contratBourse);

  void setConventionStageEtude(boolean conventionStageEtude);

  void setCharteEtudiant(boolean charteEtudiant);

  void setPreuveTestLinguistique(boolean preuveTestLinguistique);

  void setDocumentEngagement(boolean documentEngagement);

  void setAttestationSejourRecu(boolean attestationSejourRecu);

  void setReleveNoteRecu(boolean releveNoteRecu);

  void setPreuveTestLinguistiqueRetour(boolean preuveTestLinguistiqueRetour);

  void setRapportFinal(boolean rapportFinal);

  void setDernierEtat(String dernierEtat);

  void setRaisonsAnnulationRefus(String raisonsAnnulationRefus);

  void setCertificatStageRecu(boolean certificatStage);

  int getNumVersion();

  void setNumVersion(int numVersion);

  void setEnvoieDemandePaiement1(boolean envoieDemandePaiement1);

  boolean isEnvoieDemandePaiement1();

  boolean isEncodeProEco();

  void setEncodeProEco(boolean encodeProEco);

  boolean isEncodeMobilityTool();

  void setEncodeMobilityTool(boolean encodeMobilityTool);

  boolean isEncodeMobi();

  void setEncodeMobi(boolean encodeMobi);

  boolean isCertificatStage();

  void setCertificatStage(boolean certificatStage);

}
