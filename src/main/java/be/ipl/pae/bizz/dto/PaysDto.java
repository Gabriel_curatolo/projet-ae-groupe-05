package be.ipl.pae.bizz.dto;

public interface PaysDto {

  /*
   * renvoie le code ISO du pays
   * 
   * @return codeIso
   */
  String getCodeIso();

  /*
   * renvoie le nom du pays
   * 
   * @return nom
   */
  String getNom();

  /*
   * renvoie le programme du pays
   * 
   * @return programme
   */
  String getProgramme();

  void setCodeIso(String codeIso);

  void setNom(String nom);

  void setProgramme(String programme);

}
