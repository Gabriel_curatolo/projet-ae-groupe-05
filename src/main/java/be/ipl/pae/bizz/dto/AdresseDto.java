package be.ipl.pae.bizz.dto;

public interface AdresseDto {

  /*
   * Renvoie l'id de l'adresse
   * 
   * @return idAdresse
   */
  int getIdAdresse();

  /*
   * Renvoie la rue
   * 
   * @return rue
   */
  String getRue();

  /*
   * Renvoie le numero
   * 
   * @return numero
   */
  int getNumero();

  /*
   * Renvoie code postal
   * 
   * @return codePostal
   */
  String getCodePostal();

  /*
   * Renvoie la ville
   * 
   * @return ville
   */
  String getVille();

  /*
   * Renvoie la commune
   * 
   * @return commune
   */
  String getCommune();

  /*
   * Renvoie la boite
   * 
   * @return boite
   */
  String getBoite();

  /*
   * Renvoie la region
   * 
   * @return region
   */
  String getRegion();

  /*
   * Set l'id de l'adresse
   * 
   * @param idAdresse l'id de l'adresse
   */
  void setIdAdresse(int idAdresse);

  /*
   * Set la rue
   * 
   * @param rue la rue de l'adresse
   */
  void setRue(String rue);

  /*
   * Set le numero
   * 
   * @param numero le numero de l'adresse
   */
  void setNumero(int numero);

  /*
   * Set le code postal
   * 
   * @param codePostal le code postal de l'adresse
   */
  void setCodePostal(String codePostal);

  /*
   * Set la ville
   * 
   * @param ville la ville de l'adresse
   */
  void setVille(String ville);

  /*
   * Set la commune
   * 
   * @param commune la commune de l'adresse
   */
  void setCommune(String commune);

  /*
   * Set la boite
   * 
   * @param boite la boite de l'adresse
   */
  void setBoite(String string);

  /*
   * Set la region
   * 
   * @param region la region de l'adresse
   */
  void setRegion(String region);
}
