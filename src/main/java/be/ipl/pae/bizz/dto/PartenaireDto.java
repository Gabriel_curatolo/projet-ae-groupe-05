package be.ipl.pae.bizz.dto;

public interface PartenaireDto {

  /*
   * Renvoie l'id du partenaire
   * 
   * @return idPartenaire
   */
  int getIdPartenaire();

  /*
   * Renvoie le pays
   * 
   * @return pays
   */
  PaysDto getPays();

  /*
   * Renvoie l'adresse
   * 
   * @return adresse
   */
  AdresseDto getAdresse();

  /*
   * Renvoie le nom legal
   * 
   * @return nomLegal
   */
  String getNomLegal();

  /*
   * Renvoie le nom d'affaires
   * 
   * @return nomAffaires
   */
  String getNomAffaires();

  /*
   * Renvoie le nom complet
   * 
   * @return nomComplet
   */
  String getNomComplet();

  /*
   * Renvoie le programme
   * 
   * @return programme
   */
  String getProgramme();

  /*
   * Renvoie le code
   * 
   * @return code
   */
  String getCode();

  /*
   * Renvoie le type d'organisation
   * 
   * @return typeOrganisation
   */
  String getTypeOrganisation();

  /*
   * Renvoie le departement
   * 
   * @return departement
   */
  String getDepartement();

  /*
   * Renvoie le nombre d'employes
   * 
   * @return nbEmployes
   */
  int getNbEmployes();


  /*
   * Renvoie l'email
   * 
   * @return email
   */
  String getEmail();

  /*
   * Renvoie le site web
   * 
   * @return siteWeb
   */
  String getSiteWeb();

  /*
   * Renvoie le telephone
   * 
   * @return telephone
   */
  String getTelephone();

  /*
   * Set l'id du partenaire
   * 
   * @param idPartenaire l'id du partenaire
   */
  void setIdPartenaire(int idPartenaire);

  /*
   * Set le pays
   * 
   * @param pays le pays du partenaire
   */
  void setPays(PaysDto paysDto);

  /*
   * Set l'adresse
   * 
   * @param l'adresse l'adresse du partenaire
   */
  void setAdresse(AdresseDto adrDto);

  /*
   * 
   * Set le nom legal
   * 
   * @param nomLegal le nom legal du partenaire
   */
  void setNomLegal(String nomLegal);

  /*
   * 
   * Set le nom affaires
   * 
   * @param nomAffaires le nom d'affaires du partenaire
   */
  void setNomAffaires(String nomAffaires);

  /*
   * 
   * Set le nom complet
   * 
   * @param nomComplet le nom complet du partenaire
   */
  void setNomComplet(String nomComplet);

  /*
   * 
   * Set le programme
   * 
   * @param programme le programme du partenaire
   */
  void setProgramme(String programme);

  /*
   * 
   * Set le code
   * 
   * @param code le code du partenaire
   */
  void setCode(String code);

  /*
   * 
   * Set le type d'organisation
   * 
   * @param typeOrganisation du partenaire
   */
  void setTypeOrganisation(String typeOrganisation);

  /*
   * 
   * Set le departement
   * 
   * @param departement du partenaire
   */
  void setDepartement(String departement);

  /*
   * set le nombre d'employes
   * 
   * @param nbEmployes du partenaire
   */
  void setNbEmployes(int nbEmployes);

  /*
   * 
   * Set l'email
   * 
   * @param email l'email du partenaire
   */
  void setEmail(String email);

  /*
   * 
   * Set le site web
   * 
   * @param setWeb le site web du partenaire
   */
  void setSiteWeb(String siteWeb);

  /*
   * 
   * Set le telephone
   * 
   * @param telephone le telephone du partenaire
   */
  void setTelephone(String telephone);

}
