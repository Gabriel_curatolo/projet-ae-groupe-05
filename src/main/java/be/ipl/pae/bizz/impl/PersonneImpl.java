package be.ipl.pae.bizz.impl;

import be.ipl.pae.bizz.bizz.PersonneBizz;
import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.exceptions.BizException;
import be.ipl.pae.util.Util;

import org.mindrot.bcrypt.BCrypt;

import java.time.LocalDate;
import java.util.List;

public class PersonneImpl implements PersonneDto, PersonneBizz {
  private int idPersonne;
  private AdresseDto adresse;
  private PaysDto nationalite;
  private String pseudo;
  private String nom;
  private String prenom;
  private String motDePasse;
  private String email;
  private String telephone;
  private LocalDate dateInscription;
  private String role;
  private String civilite;
  private String dateNaissance;
  private String sexe;
  private String noCompte;
  private int nbAnneeReussies;
  private String titulaireCompte;
  private String banque;
  private String codeBic;
  private String departement;
  private int numVersion;

  public int getIdPersonne() {
    return idPersonne;
  }

  public void setIdPersonne(int idPersonne) {
    this.idPersonne = idPersonne;
  }

  public AdresseDto getAdresse() {
    return adresse;
  }

  public void setAdresse(AdresseDto adresse) {
    this.adresse = adresse;
  }

  public PaysDto getNationalite() {
    return nationalite;
  }


  public int getNumVersion() {

    return numVersion;
  }


  public void setNumVersion(int numVersion) {
    this.numVersion = numVersion;

  }

  public void setNationalite(PaysDto nationalite) {
    this.nationalite = nationalite;
  }

  public String getPseudo() {
    return pseudo;
  }

  public void setPseudo(String pseudo) {
    this.pseudo = pseudo;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getMotDePasse() {
    return motDePasse;
  }

  public void setMotDePasse(String motDePasse) {
    this.motDePasse = motDePasse;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public LocalDate getDateInscription() {
    return dateInscription;
  }

  public void setDateInscription(LocalDate dateInscription) {
    this.dateInscription = dateInscription;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getCivilite() {
    return civilite;
  }

  public void setCivilite(String civilite) {
    this.civilite = civilite;
  }

  public String getDateNaissance() {
    return dateNaissance;
  }

  public void setDateNaissance(String dateNaissance) {
    this.dateNaissance = dateNaissance;
  }

  public String getSexe() {
    return sexe;
  }

  public void setSexe(String sexe) {
    this.sexe = sexe;
  }

  public String getNoCompte() {
    return noCompte;
  }

  public void setNoCompte(String noCompte) {
    this.noCompte = noCompte;
  }

  public int getNbAnneeReussies() {
    return nbAnneeReussies;
  }

  public void setNbAnneeReussies(int nbAnneeReussies) {
    this.nbAnneeReussies = nbAnneeReussies;
  }

  public String getTitulaireCompte() {
    return titulaireCompte;
  }

  public void setTitulaireCompte(String titulaireCompte) {
    this.titulaireCompte = titulaireCompte;
  }

  public String getBanque() {
    return banque;
  }

  public void setBanque(String banque) {
    this.banque = banque;
  }

  public String getCodeBic() {
    return codeBic;
  }

  public void setCodeBic(String codeBic) {
    this.codeBic = codeBic;
  }

  public String getDepartement() {
    return departement;
  }

  public void setDepartement(String departement) {
    this.departement = departement;
  }

  @Override
  public boolean checkPwd(String mdpNonHash) {
    if (Util.checkEmpty(mdpNonHash)) {
      throw new BizException("Mot de passe vide");
    }
    return BCrypt.checkpw(mdpNonHash, motDePasse);
  }

  private String checkEmail() {
    if (Util.checkEmpty(getEmail())) {
      return "Email vide\n";
    }
    if (!email.matches("[a-zA-Z.]*@[a-zA-Z.]*.[a-zA-Z]*")) {
      return "Email mal forme\n";
    }
    return "";
  }

  private String checkNom() {
    if (Util.checkEmpty(getNom())) {
      return "Nom vide\n";
    }
    if (!nom.matches("^[^ ][a-zA-Z '\\-é]*[^ ]$")) {
      return "Nom mal forme\n";
    }
    return "";
  }

  private String checkPrenom() {
    if (Util.checkEmpty(getPrenom())) {
      return "Prenom vide\n";
    }
    if (!prenom.matches("^[^ ][a-zA-Z '\\-é]*[^ ]$")) {
      return "Prenom mal forme\n";
    }
    return "";
  }

  private String checkPseudo() {
    if (Util.checkEmpty(getPseudo())) {
      return "Pseudo vide\n";
    }
    return "";
  }

  private String checkRole() {
    if (Util.checkEmpty(getRole())) {
      return "role vide\n";
    }
    if (role != "et") {
      return "role different de et \n";
    }
    return "";
  }

  private String checkSexe() {
    if (Util.checkEmpty(getSexe())) {
      return "Sexe vide\n";
    }
    /*
     * if (sexe != "m" && sexe != "M" && sexe != "f" && sexe != "F") { return
     * "Sexe invalide (format : F/M)"; }
     */
    return "";
  }

  private String checkTelephone() {
    if (Util.checkEmpty(getTelephone())) {
      return "telephone vide\n";
    }
    /*
     * if (!telephone.matches("[/^[0-9\\-]|[\\+0-9]|[0-9\\s]|[0-9()]*$]")) { return
     * "telephone mal forme\n"; }
     */
    return "";
  }

  private String checkCivilite() {
    if (Util.checkEmpty(getCivilite())) {
      return "civilite vide\n";
    }
    /*
     * if (!civilite.matches("^[^ ][a-zA-Z '\\-.]*[^ ]$")) { return "civilite mal forme\n"; }
     */
    return "";
  }

  private String checkDateDeNaissance() {
    if (Util.checkEmpty(getDateNaissance())) {
      return "date de naissance vide\n";
    }
    /*
     * if
     * (!dateNaissance.matches("^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/([0-9][0-9])?[0-9][0-9]$")) {
     * return "date de naissance mal forme\n"; }
     */
    return "";
  }

  private String checkNoCompte() {
    if (Util.checkEmpty(getNoCompte())) {
      return "numero de compte vide\n";
    }
    if (!noCompte.matches("[A-Z]{2}[0-9]{2}(?:[ ]?[0-9]{4}){3}")) {
      return "numero de compte mal forme";
    }

    return "";
  }

  private String checkNbAnneesReussies() {
    if (getNbAnneeReussies() < 0) {
      return "Le nombre d'annees reussies ne peut pas etre negatif \n";
    }
    return "";
  }

  private String checkTitulaireCompte() {
    if (Util.checkEmpty(getTitulaireCompte())) {
      return "titulaire du compte vide\n";
    }
    if (!titulaireCompte.matches("^[^ ][a-zA-Z '\\-é]*[^ ]$")) {
      return " titulaire de compte mal forme\n";
    }
    return "";
  }

  private String checkBanque() {
    if (Util.checkEmpty(getBanque())) {
      return "banque vide\n";
    }
    if (!banque.matches("^[^ ][a-zA-Z '\\-]*[^ ]$")) {
      return "banque mal forme\n";
    }
    return "";
  }

  private String checkCodeBic() {
    if (Util.checkEmpty(getCodeBic())) {
      return "Code Bic vide\n";
    }
    return "";
  }

  private String checkDepartement() {
    if (Util.checkEmpty(getDepartement())) {
      return "departement vide\n";
    }
    /*
     * if (!departement.matches("^[^ ][a-zA-Z '\\-]*[^ ]$")) { return "departement mal forme \n"; }
     */
    return "";
  }

  @Override
  public boolean checkInscription() {
    String ret = "";
    ret += checkPrenom();
    ret += checkNom();
    ret += checkEmail();
    ret += checkPseudo();
    ret += checkRole();

    if (Util.checkEmpty(getMotDePasse())) {
      ret += "Mot de passe vide\n";
    }
    if (ret.length() > 0) {
      throw new BizException(ret);
    }
    return true;
  }

  @Override
  public boolean checkEncodageDonnees() {
    String ret = "";
    ret += checkSexe();
    ret += checkTelephone();
    ret += checkCivilite();
    ret += checkDateDeNaissance();
    ret += checkNoCompte();
    ret += checkNbAnneesReussies();
    ret += checkTitulaireCompte();
    ret += checkBanque();
    ret += checkCodeBic();
    ret += checkDepartement();

    if (ret.length() > 0) {
      throw new BizException(ret);
    }
    return true;
  }

  @Override
  public boolean hashPwd() {
    String sel = BCrypt.gensalt();
    String mdpHashe = BCrypt.hashpw(this.motDePasse, sel);
    if (mdpHashe.equals(this.motDePasse)) {
      throw new BizException("Impossible de hasher le mot de passe");
    }
    this.setMotDePasse(mdpHashe);
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((email == null) ? 0 : email.hashCode());
    result = prime * result + idPersonne;
    result = prime * result + ((pseudo == null) ? 0 : pseudo.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    PersonneImpl other = (PersonneImpl) obj;
    if (email == null) {
      if (other.email != null) {
        return false;
      }
    } else if (!email.equals(other.email)) {
      return false;
    }
    if (idPersonne != other.idPersonne) {
      return false;
    }
    if (pseudo == null) {
      if (other.pseudo != null) {
        return false;
      }
    } else if (!pseudo.equals(other.pseudo)) {
      return false;
    }
    return true;
  }

  @Override
  public boolean checkAugmenterProf() {
    if (this.getRole().equals("p") || this.getRole().equals("pr")) {
      throw new BizException("Il est deja professeur");
    }
    return true;
  }

  @Override
  public boolean checkEmailPseudoExistant(List<PersonneDto> list) {
    for (PersonneDto persDto : list) {
      if (this.getEmail().equals(persDto.getEmail()) || this.getPseudo() == persDto.getPseudo()) {
        throw new BizException("Le pseudo ou l'email est deja existant");
      }
    }
    return false;
  }

  @Override
  public boolean checkPseudoConnexion() {
    if (pseudo.length() == 0) {
      throw new BizException("Pseudo invalide");
    }
    return true;
  }

  @Override
  public boolean checkNullConnexion(PersonneDto personne) {
    if (personne == null) {
      throw new BizException("Pseudo non trouve dans la base de donnees");
    }
    return true;
  }

  @Override
  public boolean checkRecherchePersonnes() {
    if (this.getNom().equals("") && this.getPrenom().equals("")) {
      throw new BizException("Les champs sont vides");
    }
    return true;
  }
}

