package be.ipl.pae.bizz.impl;

import be.ipl.pae.bizz.bizz.AdresseBizz;
import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.exceptions.BizException;
import be.ipl.pae.util.Util;

public class AdresseImpl implements AdresseDto, AdresseBizz {
  private int idAdresse;
  private String rue;
  private int numero;
  private String codePostal;
  private String ville;
  private String commune;
  private String boite;
  private String region;


  public int getIdAdresse() {
    return idAdresse;
  }

  public void setIdAdresse(int idAdresse) {
    this.idAdresse = idAdresse;
  }

  public String getRue() {
    return rue;
  }

  public void setRue(String rue) {
    this.rue = rue;
  }

  public int getNumero() {
    return numero;
  }

  public void setNumero(int numero) {
    this.numero = numero;
  }

  public String getCodePostal() {
    return codePostal;
  }

  public void setCodePostal(String codePostal) {
    this.codePostal = codePostal;
  }

  public String getVille() {
    return ville;
  }

  public void setVille(String ville) {
    this.ville = ville;
  }

  public String getCommune() {
    return commune;
  }

  public void setCommune(String commune) {
    this.commune = commune;
  }

  public String getBoite() {
    return boite;
  }

  public void setBoite(String boite) {
    this.boite = boite;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  private String checkRue() {
    if (Util.checkEmpty(getRue())) {
      return "Rue vide\n";
    }
    if (!rue.matches("^[^ ][a-zA-Z '\\-]*[^ ]$")) {
      return "Rue mal formee\n";
    }
    return "";
  }

  private String checkVille() {
    if (Util.checkEmpty(getVille())) {
      return "Ville vide\n";
    }
    if (!ville.matches("^[^ ][a-zA-Z '\\-]*[^ ]$")) {
      return "Ville mal formee\n";
    }
    return "";
  }

  private String checkCommune() {
    /*
     * if (!commune.matches("^[^ ][a-zA-Z '\\-]*[^ ]$")) { return "Commune mal formee\n"; }
     */
    return "";
  }

  private String checkRegion() {
    /*
     * if (!region.matches("^[^ ][a-zA-Z '\\-]*[^ ]$")) { return "Region mal formee\n"; }
     */
    return "";
  }

  private String checkBoite() {
    /*
     * if (Util.checkEmpty(boite)) { return "Boite vide\n"; }
     */
    return "";
  }

  @Override
  public boolean checkEncodageAdresses() {
    String ret = "";
    ret += checkRue();
    ret += checkVille();
    ret += checkCommune();
    ret += checkRegion();
    ret += checkBoite();

    if (ret.length() > 0) {
      throw new BizException(ret);
    }
    return true;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + idAdresse;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    AdresseImpl other = (AdresseImpl) obj;
    if (idAdresse != other.idAdresse) {
      return false;
    }
    return true;
  }

}
