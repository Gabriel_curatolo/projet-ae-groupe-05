package be.ipl.pae.bizz.impl;

import be.ipl.pae.bizz.bizz.MobiliteEtudianteBizz;
import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.exceptions.BizException;
import be.ipl.pae.util.Util;

import java.util.List;

public class MobiliteEtudianteImpl implements MobiliteEtudianteDto, MobiliteEtudianteBizz {

  private int idMobiliteEtudiant;
  private DemandeMobiliteDto demande;
  private PersonneDto personne;
  private PartenaireDto partenaire;
  private String etat;
  private int quadrimestre;
  private boolean envoieDemandePaiement1;
  private boolean envoieDemandePaiement2;
  private boolean contratBourse;
  private boolean conventionStageEtude;
  private boolean charteEtudiant;
  private boolean preuveTestLinguistique;
  private boolean documentEngagement;
  private boolean encodeProEco;
  private boolean encodeMobilityTool;
  private boolean encodeMobi;
  private boolean attestationSejourRecu;
  private boolean releveNoteRecu;
  private boolean preuveTestLinguistiqueRetour;
  private boolean rapportFinal;
  private String dernierEtat;
  private String raisonsAnnulationRefus;
  private boolean certificatStage;
  private int numVersion;

  public int getIdMobiliteEtudiant() {
    return idMobiliteEtudiant;
  }

  public DemandeMobiliteDto getDemande() {
    return demande;
  }

  public PersonneDto getPersonne() {
    return personne;
  }

  public PartenaireDto getPartenaire() {
    return partenaire;
  }

  public String getEtat() {
    return etat;
  }

  public int getQuadrimestre() {
    return quadrimestre;
  }

  public boolean isEnvoieDemandePaiement2() {
    return envoieDemandePaiement2;
  }

  public boolean isContratBourse() {
    return contratBourse;
  }

  public boolean isConventionStageEtude() {
    return conventionStageEtude;
  }

  public boolean isCharteEtudiant() {
    return charteEtudiant;
  }

  public boolean isPreuveTestLinguistique() {
    return preuveTestLinguistique;
  }

  public boolean isDocumentEngagement() {
    return documentEngagement;
  }


  public boolean isAttestationSejourRecu() {
    return attestationSejourRecu;
  }

  public boolean isReleveNoteRecu() {
    return releveNoteRecu;
  }

  public boolean isPreuveTestLinguistiqueRetour() {
    return preuveTestLinguistiqueRetour;
  }

  public boolean isRapportFinal() {
    return rapportFinal;
  }

  public String getDernierEtat() {
    return dernierEtat;
  }

  public String getRaisonsAnnulationRefus() {
    return raisonsAnnulationRefus;
  }

  public boolean getCertificatStageRecu() {
    return certificatStage;
  }

  public void setIdMobiliteEtudiant(int idMobiliteEtudiant) {
    this.idMobiliteEtudiant = idMobiliteEtudiant;
  }

  public void setDemande(DemandeMobiliteDto demande) {
    this.demande = demande;
  }

  public void setPersonne(PersonneDto personne) {
    this.personne = personne;
  }

  public void setPartenaire(PartenaireDto partenaire) {
    this.partenaire = partenaire;
  }

  public void setEtat(String etat) {
    this.etat = etat;
  }

  public void setQuadrimestre(int quadrimestre) {
    this.quadrimestre = quadrimestre;
  }

  public void setEnvoieDemandePaiement2(boolean envoieDemandePaiement2) {
    this.envoieDemandePaiement2 = envoieDemandePaiement2;
  }

  public void setContratBourse(boolean contratBourse) {
    this.contratBourse = contratBourse;
  }

  public void setConventionStageEtude(boolean conventionStageEtude) {
    this.conventionStageEtude = conventionStageEtude;
  }

  public void setCharteEtudiant(boolean charteEtudiant) {
    this.charteEtudiant = charteEtudiant;
  }

  public void setPreuveTestLinguistique(boolean preuveTestLinguistique) {
    this.preuveTestLinguistique = preuveTestLinguistique;
  }

  public void setDocumentEngagement(boolean documentEngagement) {
    this.documentEngagement = documentEngagement;
  }

  public void setAttestationSejourRecu(boolean attestationSejourRecu) {
    this.attestationSejourRecu = attestationSejourRecu;
  }

  public void setReleveNoteRecu(boolean releveNoteRecu) {
    this.releveNoteRecu = releveNoteRecu;
  }

  public void setPreuveTestLinguistiqueRetour(boolean preuveTestLinguistiqueRetour) {
    this.preuveTestLinguistiqueRetour = preuveTestLinguistiqueRetour;
  }

  public void setRapportFinal(boolean rapportFinal) {
    this.rapportFinal = rapportFinal;
  }

  public void setDernierEtat(String dernierEtat) {
    this.dernierEtat = dernierEtat;
  }

  public void setRaisonsAnnulationRefus(String raisonsAnnulationRefus) {
    this.raisonsAnnulationRefus = raisonsAnnulationRefus;
  }

  @Override
  public void setCertificatStageRecu(boolean certificatStage) {
    this.certificatStage = certificatStage;

  }

  private String checkEtatConfirmer() {
    if (Util.checkEmpty(getEtat())) {
      return "etat vide \n";
    }
    if (etat != "Créée") {
      return "etat mal forme \n";
    }
    return "";
  }

  private String checkQuadrimestre() {
    if (quadrimestre != 1 && quadrimestre != 2) {
      return "quadrimestre incorrect";
    }
    return "";
  }


  @Override
  public boolean checkConfirmerMobilite() {
    String ret = "";
    ret += checkEtatConfirmer();
    ret += checkQuadrimestre();

    if (ret.length() > 0) {
      throw new BizException(ret);
    }
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + idMobiliteEtudiant;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    MobiliteEtudianteImpl other = (MobiliteEtudianteImpl) obj;
    if (idMobiliteEtudiant != other.idMobiliteEtudiant) {
      return false;
    }
    return true;
  }

  @Override
  public int getNumVersion() {
    return numVersion;
  }

  @Override
  public void setNumVersion(int numVersion) {
    this.numVersion = numVersion;
  }

  @Override
  public boolean isEnvoieDemandePaiement1() {
    return envoieDemandePaiement1;
  }

  @Override
  public void setEnvoieDemandePaiement1(boolean envoieDemandePaiement1) {
    this.envoieDemandePaiement1 = envoieDemandePaiement1;
  }

  @Override
  public boolean isEncodeProEco() {
    return encodeProEco;
  }

  @Override
  public void setEncodeProEco(boolean encodeProEco) {
    this.encodeProEco = encodeProEco;
  }

  @Override
  public boolean isEncodeMobilityTool() {
    return encodeMobilityTool;
  }

  @Override
  public void setEncodeMobilityTool(boolean encodeMobilityTool) {
    this.encodeMobilityTool = encodeMobilityTool;
  }

  @Override
  public boolean isEncodeMobi() {
    return encodeMobi;
  }

  @Override
  public void setEncodeMobi(boolean encodeMobi) {
    this.encodeMobi = encodeMobi;
  }

  @Override
  public boolean isCertificatStage() {
    return certificatStage;
  }

  @Override
  public void setCertificatStage(boolean certificatStage) {
    this.certificatStage = certificatStage;
  }

  @Override
  public boolean checkSiErasmusPreuveLinguistique() {
    if (!this.getDemande().getProgramme().equals("Erasmus+")
        && (this.isPreuveTestLinguistique() == true
            || this.isPreuveTestLinguistiqueRetour() == true)) {
      throw new BizException(
          "On ne peut pas mettre a true la preuve linguistique pour autre chose qu'un erasmus");
    }

    return true;
  }

  @Override
  public boolean checkEncodageAilleurs() {
    if (this.isEncodeMobilityTool() == true && this.isEncodeMobi() == true) {
      throw new BizException("On ne peut pas encoder dans Mobility Tool et Mobi");
    }
    return true;
  }

  @Override
  public boolean checkEnPreparation() {
    if ((this.isContratBourse() == false || this.isConventionStageEtude() == false
        || this.isCharteEtudiant() == false || this.isPreuveTestLinguistique() == false
        || this.isDocumentEngagement() == false)
        && this.getDemande().getProgramme().equals("Erasmus+")
        || (this.isContratBourse() == false || this.isConventionStageEtude() == false
            || this.isCharteEtudiant() == false || this.isDocumentEngagement() == false)
            && !this.getDemande().getProgramme().equals("Erasmus+")) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public boolean checkPreciserDocumentsRetour() {
    if ((this.isAttestationSejourRecu() == false
        || this.isReleveNoteRecu() == false && this.getCertificatStageRecu() == false
        || this.isRapportFinal() == false || this.isPreuveTestLinguistiqueRetour() == false)
        && this.getDemande().getProgramme().equals("Erasmus+")
        || (this.isAttestationSejourRecu() == false
            || this.isReleveNoteRecu() == false && this.getCertificatStageRecu() == false
            || this.isRapportFinal() == false)
            && !this.getDemande().getProgramme().equals("Erasmus+")) {
      if (this.isReleveNoteRecu() == true && this.getCertificatStageRecu() == true) {
        throw new BizException(
            "Etudiant ne peut pas avoir un releve de note et un certificat de stage");
      }
      return true;
    }

    return false;
  }

  @Override
  public boolean checkVideRechercheMobi() {
    if (this.getEtat().equals("") && this.getDemande().getAnneeAcademique() == 0) {
      throw new BizException("Aucun champ rempli");
    }
    return true;
  }

  @Override
  public boolean checkMobiliteDejaConfirmee(List<MobiliteEtudianteDto> list) {
    for (MobiliteEtudianteDto m : list) {
      if (m.getDemande().getAnneeAcademique() == this.getDemande().getAnneeAcademique()
          && m.getQuadrimestre() == this.getQuadrimestre()) {
        throw new BizException("Cet etudiant a deja une mobilite confirme");
      }
    }
    return true;
  }

  @Override
  public boolean checkSiContratBourseSuisse() {
    if (this.getDemande().getPays().getCodeIso().equals("CHE") && this.isContratBourse() == true) {
      throw new BizException("Une mobilite pour la suisse ne peut pas avoir de contrat de bourse");
    }
    return true;
  }

  @Override
  public boolean checkAPayerSuisse() {

    if (this.getDemande().getPays().getCodeIso().equals("CHE") && this.isContratBourse() == false
        && this.isConventionStageEtude() == true && this.isCharteEtudiant() == true
        && this.isDocumentEngagement() == true && this.isPreuveTestLinguistique() == false) {
      return true;
    }
    return false;
  }

  @Override
  public boolean checkTermineSuisse() {
    if (this.getDemande().getPays().getCodeIso().equals("CHE")
        && this.isAttestationSejourRecu() == true && this.isRapportFinal() == true
        && this.isPreuveTestLinguistiqueRetour() == false && this.isReleveNoteRecu() == true
        && this.isCertificatStage() == false
        || this.getDemande().getPays().getCodeIso().equals("CHE")
            && this.isAttestationSejourRecu() == true && this.isRapportFinal() == true
            && this.isPreuveTestLinguistiqueRetour() == false && this.isReleveNoteRecu() == false
            && this.isCertificatStage() == true) {
      return true;
    }
    return false;
  }

}
