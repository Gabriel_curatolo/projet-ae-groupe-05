package be.ipl.pae.bizz.impl;

import be.ipl.pae.bizz.bizz.PartenaireBizz;
import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.exceptions.BizException;

public class PartenaireImpl implements PartenaireDto, PartenaireBizz {
  private int idPartenaire;
  private PaysDto pays;
  private AdresseDto adresse;
  private String nomLegal;
  private String nomAffaires;
  private String nomComplet;
  private String programme;
  private String code;
  private String typeOrganisation;
  private String departement;
  private int nbEmployes;
  private String email;
  private String siteWeb;
  private String telephone;

  public int getIdPartenaire() {
    return idPartenaire;
  }

  public void setIdPartenaire(int idPartenaire) {
    this.idPartenaire = idPartenaire;
  }

  public PaysDto getPays() {
    return pays;
  }

  public void setPays(PaysDto pays) {
    this.pays = pays;
  }

  public AdresseDto getAdresse() {
    return adresse;
  }

  public void setAdresse(AdresseDto adresse) {
    this.adresse = adresse;
  }

  public String getNomLegal() {
    return nomLegal;
  }

  public void setNomLegal(String nomLegal) {
    this.nomLegal = nomLegal;
  }

  public String getNomAffaires() {
    return nomAffaires;
  }

  public void setNomAffaires(String nomAffaires) {
    this.nomAffaires = nomAffaires;
  }

  public String getNomComplet() {
    return nomComplet;
  }

  public void setNomComplet(String nomComplet) {
    this.nomComplet = nomComplet;
  }

  public String getProgramme() {
    return programme;
  }

  public void setProgramme(String programme) {
    this.programme = programme;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getTypeOrganisation() {
    return typeOrganisation;
  }

  public void setTypeOrganisation(String typeOrganisation) {
    this.typeOrganisation = typeOrganisation;
  }

  public String getDepartement() {
    return departement;
  }

  public void setDepartement(String departement) {
    this.departement = departement;
  }

  public int getNbEmployes() {
    return nbEmployes;
  }

  public void setNbEmployes(int nbEmployes) {
    this.nbEmployes = nbEmployes;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getSiteWeb() {
    return siteWeb;
  }

  public void setSiteWeb(String siteWeb) {
    this.siteWeb = siteWeb;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + idPartenaire;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    PartenaireImpl other = (PartenaireImpl) obj;
    if (idPartenaire != other.idPartenaire) {
      return false;
    }
    return true;
  }

  @Override
  public boolean verifCodePartenaire(DemandeMobiliteDto demDto) {
    if (!this.getCode().equals(demDto.getCode())) {
      throw new BizException("Ce code n existe pas pour ce partenaire");
    }
    return true;
  }

  @Override
  public boolean checkRecherchePartenaire() {
    if (this.getNomComplet().equals("") && this.getPays().getNom().equals("")
        && this.getAdresse().getVille().equals("")) {
      throw new BizException("Les champs sont vides");
    }
    return true;
  }

}
