package be.ipl.pae.bizz.impl;

import be.ipl.pae.bizz.bizz.PaysBizz;
import be.ipl.pae.bizz.dto.PaysDto;

public class PaysImpl implements PaysDto, PaysBizz {

  private String codeIso;
  private String nom;
  private String programme;

  public String getCodeIso() {
    return codeIso;
  }

  public String getNom() {
    return nom;
  }

  public String getProgramme() {
    return programme;
  }

  public void setCodeIso(String codeIso) {
    this.codeIso = codeIso;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public void setProgramme(String programme) {
    this.programme = programme;
  }

}
