package be.ipl.pae.bizz.impl;

import be.ipl.pae.bizz.bizz.DemandeMobiliteBizz;
import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.exceptions.BizException;
import be.ipl.pae.util.Util;

import java.time.LocalDate;

public class DemandeMobiliteImpl implements DemandeMobiliteDto, DemandeMobiliteBizz {

  private int idDemande;
  private PartenaireDto partenaire;
  private PersonneDto personne;
  private PaysDto pays;
  private boolean confirme;
  private LocalDate dateIntroduction;
  private String programme;
  private String code;
  private int quadrimestre;
  private int ordrePreference;
  private int anneeAcademique;
  private int numOrdreCandidature;
  private int numVersion;

  @Override
  public int getIdDemande() {
    return idDemande;
  }

  @Override
  public PartenaireDto getPartenaire() {
    return partenaire;
  }

  @Override
  public PersonneDto getPersonne() {
    return personne;
  }

  @Override
  public PaysDto getPays() {
    return pays;
  }

  @Override
  public boolean isConfirme() {
    return confirme;
  }

  @Override
  public LocalDate getDateIntroduction() {
    return dateIntroduction;
  }

  @Override
  public String getProgramme() {
    return programme;
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public int getQuadrimestre() {
    return quadrimestre;
  }

  @Override
  public int getOrdrePreference() {
    return ordrePreference;
  }

  @Override
  public int getAnneeAcademique() {
    return anneeAcademique;
  }


  @Override
  public int getNumVersion() {
    return numVersion;
  }

  @Override
  public void setIdDemande(int idDemande) {
    this.idDemande = idDemande;
  }

  @Override
  public void setPartenaire(PartenaireDto partenaire) {
    this.partenaire = partenaire;
  }

  @Override
  public void setPersonne(PersonneDto personne) {
    this.personne = personne;
  }

  @Override
  public void setPays(PaysDto pays) {
    this.pays = pays;
  }

  @Override
  public void setConfirme(boolean confirme) {
    this.confirme = confirme;
  }

  @Override
  public void setDateIntroduction(LocalDate dateIntroduction) {
    this.dateIntroduction = dateIntroduction;
  }

  @Override
  public void setProgramme(String programme) {
    this.programme = programme;
  }

  @Override
  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public void setQuadrimestre(int quadrimestre) {
    this.quadrimestre = quadrimestre;
  }

  @Override
  public void setOrdrePreference(int ordrePreference) {
    this.ordrePreference = ordrePreference;
  }

  @Override
  public void setAnneeAcademique(int anneeAcademique) {
    this.anneeAcademique = anneeAcademique;
  }


  @Override
  public void setNumVersion(int numVersion) {
    this.numVersion = numVersion;
  }

  private String checkOrdre() {
    if (getOrdrePreference() < 0) {
      return "l'ordre de preference doit etre positif ou null";
    }
    return "";
  }

  private String checkProgramme() {
    if (Util.checkEmpty(getProgramme())) {
      return "Programme vide\n";
    }

    if (!programme.equals("Erasmus+") && !programme.equals("Erabel") && !programme.equals("Fame")
        && !programme.equals("Autre")) {
      return "Le programme n'est pas correct";
    }
    return "";
  }

  private String checkCode() {
    if (Util.checkEmpty(getCode())) {
      return "Code vide\n";
    }

    if (!code.equals("SMS") && !code.equals("SMP")) {
      return "Le code n'est pas correcte";
    }
    return "";
  }

  private String checkConfirmeDeclarer() {
    if (isConfirme() == true) {
      return "Vous ne pouvez pas mettre confirme a true";
    }
    return "";
  }

  private String checkVille() {
    /*
     * if (!destination.matches("^[^ ][a-zA-Z '\\-]*[^ ]$")) { return "Ville male formee\n"; }
     */
    return "";
  }


  private String checkQuadrimestre() {
    if (getQuadrimestre() != 1 && getQuadrimestre() != 2) {
      return "le quadrimestre doit etre 1 ou 2\n";
    }
    return "";
  }

  private String checkPersonne() {
    if (getPersonne() == null) {
      return "La personne est vide \n";
    }
    return "";
  }

  @Override
  public boolean checkDeclarerDemandeMobilites() {
    String ret = "";
    ret += checkOrdre();
    ret += checkProgramme();
    ret += checkCode();
    ret += checkVille();
    ret += checkQuadrimestre();
    ret += checkPersonne();
    ret += checkConfirmeDeclarer();

    if (ret.length() > 0) {
      throw new BizException(ret);
    }
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + idDemande;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    DemandeMobiliteImpl other = (DemandeMobiliteImpl) obj;
    if (idDemande != other.idDemande) {
      return false;
    }
    return true;
  }

  @Override
  public int getNumOrdreCandidature() {
    return numOrdreCandidature;
  }

  @Override
  public void setNumOrdreCandidature(int numOrdreCandidature) {
    this.numOrdreCandidature = numOrdreCandidature;
  }


}
