package be.ipl.pae.bizz.bizz;

import be.ipl.pae.bizz.dto.DemandeMobiliteDto;

public interface DemandeMobiliteBizz extends DemandeMobiliteDto {

  boolean checkDeclarerDemandeMobilites();

}
