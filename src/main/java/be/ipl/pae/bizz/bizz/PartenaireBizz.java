package be.ipl.pae.bizz.bizz;

import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;

public interface PartenaireBizz extends PartenaireDto {

  boolean verifCodePartenaire(DemandeMobiliteDto demDto);

  boolean checkRecherchePartenaire();

}
