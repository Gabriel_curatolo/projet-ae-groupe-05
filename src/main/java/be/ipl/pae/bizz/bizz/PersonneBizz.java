package be.ipl.pae.bizz.bizz;

import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.exceptions.BizException;

import java.util.List;

public interface PersonneBizz extends PersonneDto {

  /*
   * verifie si mot de passe n'est pas vide, et verifie ensuite si le mot de passe correspond a
   * celui en db apres l avoir hashe
   * 
   * @param mot de passe rentre pour se connecte
   * 
   * @throw BizException si le mot de passe est vide
   * 
   * @return true si le mot de passe est correct, false sinon
   */
  boolean checkPwd(String mdpNonHash) throws BizException;

  /*
   * Hash le password du PersonneBiz.
   * 
   * @return true le hashage a fonctionne,false sinon
   */
  boolean hashPwd() throws BizException;

  /*
   * verifie tous les champs remplis pour l'inscription
   * 
   * @throw BizException si un champ est mal forme
   * 
   * @return true dans tous les autres cas
   */
  boolean checkInscription() throws BizException;


  /*
   * verifie tous les champs remplis pour l'encodage de donnees
   * 
   * @throw BizException si un champ est mal forme
   * 
   * @return true dans tous les autres cas
   */
  boolean checkEncodageDonnees();

  boolean checkAugmenterProf();

  boolean checkEmailPseudoExistant(List<PersonneDto> list);

  boolean checkPseudoConnexion();

  boolean checkNullConnexion(PersonneDto personne);

  boolean checkRecherchePersonnes();


}
