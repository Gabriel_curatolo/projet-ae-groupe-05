package be.ipl.pae.bizz.bizz;

import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;

import java.util.List;

public interface MobiliteEtudianteBizz extends MobiliteEtudianteDto {

  boolean checkConfirmerMobilite();

  boolean checkSiErasmusPreuveLinguistique();

  boolean checkEncodageAilleurs();

  boolean checkEnPreparation();

  boolean checkPreciserDocumentsRetour();

  boolean checkVideRechercheMobi();

  boolean checkMobiliteDejaConfirmee(List<MobiliteEtudianteDto> list);

  boolean checkSiContratBourseSuisse();

  boolean checkAPayerSuisse();

  boolean checkTermineSuisse();

}
