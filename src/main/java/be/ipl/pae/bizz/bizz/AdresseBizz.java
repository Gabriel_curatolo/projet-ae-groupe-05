package be.ipl.pae.bizz.bizz;

import be.ipl.pae.bizz.dto.AdresseDto;

public interface AdresseBizz extends AdresseDto {

  /*
   * verifie tous les champs remplis pour l'encodage d'adresses
   * 
   * @throw BizException si un champ est mal forme
   * 
   * @return true dans tous les autres cas
   */
  boolean checkEncodageAdresses();

}
