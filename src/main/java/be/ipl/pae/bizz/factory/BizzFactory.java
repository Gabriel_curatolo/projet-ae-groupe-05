package be.ipl.pae.bizz.factory;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;

public interface BizzFactory {

  /*
   * renvoie une enveloppe d'une personne vide
   * 
   * @return personne une enveloppe vide de personne
   */
  PersonneDto getPersonne();

  /*
   * renvoie une enveloppe d'une adresse vide
   * 
   * @return adresse une adresse vide de adresse
   */
  AdresseDto getAdresse();

  DemandeMobiliteDto getDemandeMobilite();

  PartenaireDto getPartenaire();

  MobiliteEtudianteDto getMobiliteEtudiante();

  PaysDto getPays();

}
