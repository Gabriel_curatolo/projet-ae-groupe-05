package be.ipl.pae.bizz.factory;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.impl.AdresseImpl;
import be.ipl.pae.bizz.impl.DemandeMobiliteImpl;
import be.ipl.pae.bizz.impl.MobiliteEtudianteImpl;
import be.ipl.pae.bizz.impl.PartenaireImpl;
import be.ipl.pae.bizz.impl.PaysImpl;
import be.ipl.pae.bizz.impl.PersonneImpl;

import com.owlike.genson.annotation.JsonCreator;

public class BizzFactoryImpl implements BizzFactory {

  @Override
  @JsonCreator
  public PersonneDto getPersonne() {
    return new PersonneImpl();
  }

  @Override
  @JsonCreator
  public AdresseDto getAdresse() {
    return new AdresseImpl();
  }

  @Override
  public DemandeMobiliteDto getDemandeMobilite() {
    return new DemandeMobiliteImpl();
  }

  @Override
  public PartenaireDto getPartenaire() {
    return new PartenaireImpl();
  }

  @Override
  public MobiliteEtudianteDto getMobiliteEtudiante() {
    return new MobiliteEtudianteImpl();
  }

  @Override
  public PaysDto getPays() {
    return new PaysImpl();
  }


}
