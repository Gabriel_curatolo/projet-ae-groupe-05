package be.ipl.pae.bizz.factory;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.impl.AdresseImpl;
import be.ipl.pae.bizz.impl.DemandeMobiliteImpl;
import be.ipl.pae.bizz.impl.MobiliteEtudianteImpl;
import be.ipl.pae.bizz.impl.PartenaireImpl;
import be.ipl.pae.bizz.impl.PaysImpl;
import be.ipl.pae.bizz.impl.PersonneImpl;

import java.time.LocalDate;

public class BizzFactoryStub implements BizzFactory {

  @Override
  public PersonneDto getPersonne() {
    // TODO Auto-generated method stub
    PersonneDto ret = new PersonneImpl();
    ret.setPseudo("JeanFoufou");
    ret.setPrenom("Jean");
    ret.setEmail("jean@gmail.com");
    ret.setNom("Fouquot");
    ret.setIdPersonne(1);
    ret.setDateInscription(LocalDate.now());
    ret.setRole("et");
    ret.setMotDePasse("123");
    ret.setSexe("M");
    ret.setTelephone("029876532");
    ret.setCivilite("M.");
    ret.setDateNaissance("01/03/1999");
    ret.setNoCompte("BE29 9262 8487 4875");
    ret.setNbAnneeReussies(2);
    ret.setTitulaireCompte("Romain Truca");
    ret.setBanque("Bnp paris bas");
    ret.setCodeBic("BE5649");
    ret.setDepartement("Informatique");

    PaysDto pays = this.getPays();
    ret.setNationalite(pays);

    AdresseDto adr = this.getAdresse();
    ret.setAdresse(adr);
    ret.setNumVersion(1);
    return ret;
  }

  @Override
  public AdresseDto getAdresse() {
    AdresseDto ret = new AdresseImpl();
    ret.setIdAdresse(1);
    ret.setRue("Rue des petits reux");
    ret.setNumero(5);
    ret.setCodePostal("de65de");
    ret.setVille("Bruxelles");
    ret.setCommune("Uccle");
    ret.setBoite("25b");
    ret.setRegion("Hainaut");
    return ret;
  }

  @Override
  public DemandeMobiliteDto getDemandeMobilite() {
    DemandeMobiliteDto ret = new DemandeMobiliteImpl();
    ret.setIdDemande(1);
    PartenaireDto part = this.getPartenaire();
    ret.setPartenaire(part);
    PaysDto pays = this.getPays();
    ret.setPays(pays);
    PersonneDto pers = this.getPersonne();
    ret.setPersonne(pers);
    ret.setConfirme(false);
    ret.setDateIntroduction(LocalDate.now());
    ret.setProgramme("Erabel");
    ret.setCode("SMS");
    ret.setQuadrimestre(2);
    ret.setOrdrePreference(1);
    ret.setAnneeAcademique(2019);
    ret.setNumVersion(1);

    return ret;
  }

  @Override
  public PartenaireDto getPartenaire() {
    PartenaireDto part = new PartenaireImpl();
    part.setIdPartenaire(1);
    part.setCode("SMS");
    part.setDepartement("informatique");
    part.setEmail("strudel@gmail.com");
    part.setNbEmployes(200);
    part.setNomAffaires("Strudel");
    part.setNomComplet("Wölfel Engineering GmbH");
    part.setNomLegal("Strobel Sosse gmbh");
    part.setSiteWeb("studel.de");
    part.setTelephone("+33473881344");
    part.setTypeOrganisation("Multinationale");
    AdresseDto adr = this.getAdresse();
    part.setAdresse(adr);
    PaysDto pays = this.getPays();
    part.setPays(pays);
    return part;
  }

  @Override
  public MobiliteEtudianteDto getMobiliteEtudiante() {
    MobiliteEtudianteDto ret = new MobiliteEtudianteImpl();
    ret.setIdMobiliteEtudiant(1);
    DemandeMobiliteDto dem = new DemandeMobiliteImpl();
    dem.setIdDemande(1);
    dem.setProgramme("FAME");
    dem.setAnneeAcademique(2019);
    PaysDto pays = this.getPays();
    dem.setPays(pays);
    ret.setDemande(dem);
    PersonneDto pers = this.getPersonne();
    ret.setPersonne(pers);
    PartenaireDto part = this.getPartenaire();
    part.setIdPartenaire(1);
    ret.setPartenaire(part);
    ret.setEtat("Créée");
    ret.setQuadrimestre(2);
    ret.setReleveNoteRecu(false);
    ret.setAttestationSejourRecu(false);
    ret.setEnvoieDemandePaiement1(false);
    ret.setEnvoieDemandePaiement2(false);
    ret.setContratBourse(false);
    ret.setConventionStageEtude(false);
    ret.setCharteEtudiant(false);
    ret.setPreuveTestLinguistique(false);
    ret.setDocumentEngagement(false);
    ret.setEncodeProEco(false);
    ret.setEncodeMobilityTool(false);
    ret.setEncodeMobi(false);
    ret.setPreuveTestLinguistiqueRetour(false);
    ret.setRapportFinal(false);
    ret.setDernierEtat("cree");
    ret.setCertificatStageRecu(false);
    return ret;
  }


  @Override
  public PaysDto getPays() {
    PaysDto pays = new PaysImpl();
    pays.setCodeIso("DEU");
    pays.setNom("Allemagne");
    pays.setProgramme("Erabel");
    return pays;
  }

}
