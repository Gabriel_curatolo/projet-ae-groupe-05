package be.ipl.pae.exceptions;

public class DalException extends RuntimeException {

  public DalException() {
    super();
  }

  public DalException(String message) {
    super(message);
  }

  public DalException(Throwable message) {
    super(message);
  }
}
