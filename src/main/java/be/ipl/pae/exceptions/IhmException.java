package be.ipl.pae.exceptions;

public class IhmException extends RuntimeException {
  public IhmException() {
    super();
  }

  public IhmException(String message) {
    super(message);
  }
}
