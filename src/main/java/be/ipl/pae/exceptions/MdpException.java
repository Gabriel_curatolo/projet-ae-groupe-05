package be.ipl.pae.exceptions;

public class MdpException extends RuntimeException {
  public MdpException() {
    super();
  }

  public MdpException(String message) {
    super(message);
  }
}
