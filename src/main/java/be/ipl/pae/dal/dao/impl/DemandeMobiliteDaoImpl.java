package be.ipl.pae.dal.dao.impl;

import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.dal.dao.interfaces.DemandeMobiliteDao;
import be.ipl.pae.dal.services.DalBackEndServices;
import be.ipl.pae.exceptions.DalException;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DemandeMobiliteDaoImpl implements DemandeMobiliteDao {

  private BizzFactory bizzFactory;
  private DalBackEndServices dalBes;

  /**
   * constructeur de AdresseDaoImpl.
   * 
   * @param dalBes le dalbackendServices
   * @param bizzFactory le bizzFactory
   */
  public DemandeMobiliteDaoImpl(DalBackEndServices dalBes, BizzFactory bizzFactory) {
    super();
    this.dalBes = dalBes;
    this.bizzFactory = bizzFactory;
  }

  @Override
  public List<DemandeMobiliteDto> visualiserSesDemandes(PersonneDto persDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.demandes_mobilite AS DEM"
            + " LEFT OUTER JOIN projetpae.personnes AS PER ON DEM.id_personne = PER.id_personne"
            + " LEFT OUTER JOIN projetpae.partenaires AS PAR"
            + " ON DEM.id_partenaire = PAR.id_partenaire  LEFT OUTER JOIN projetpae.pays AS PAY"
            + " ON DEM.pays = PAY.code_iso  WHERE DEM.id_personne = ? AND confirme = false ");
    List<DemandeMobiliteDto> listeDemandes = new ArrayList<DemandeMobiliteDto>();
    try {
      ps.setInt(1, persDto.getIdPersonne());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        DemandeMobiliteDto dto = creerDemandeMobiliteAvecResultSet(rs);
        listeDemandes.add(dto);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return listeDemandes;
  }



  @Override
  public DemandeMobiliteDto declarerDemandeMobilites(DemandeMobiliteDto demandeEncodageDonnees) {

    PreparedStatement ps = dalBes
        .prepareStatement("INSERT INTO projetpae.demandes_mobilite (id_partenaire, id_personne, "
            + "pays, confirme, date_introduction, programme_demande, code_demande, "
            + "quadrimestre_demande, ordre_preference, annee_academique, "
            + "num_version_demande_mobilite, num_ordre_candidature) "
            + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?);");
    try {
      if (demandeEncodageDonnees.getPartenaire() != null) {
        ps.setInt(1, demandeEncodageDonnees.getPartenaire().getIdPartenaire());
      } else {
        ps.setObject(1, demandeEncodageDonnees.getPartenaire());
      }
      ps.setInt(2, demandeEncodageDonnees.getPersonne().getIdPersonne());
      ps.setString(3, demandeEncodageDonnees.getPays().getCodeIso());
      ps.setBoolean(4, demandeEncodageDonnees.isConfirme());
      Date date = Date.valueOf(LocalDate.now());
      ps.setDate(5, date);
      ps.setString(6, demandeEncodageDonnees.getProgramme());
      ps.setString(7, demandeEncodageDonnees.getCode());
      if (demandeEncodageDonnees.getQuadrimestre() > 0) {
        ps.setInt(8, demandeEncodageDonnees.getQuadrimestre());
      } else {
        ps.setNull(8, java.sql.Types.INTEGER);
      }
      ps.setInt(9, demandeEncodageDonnees.getOrdrePreference());
      ps.setInt(10, demandeEncodageDonnees.getAnneeAcademique());
      ps.setInt(11, 1);
      ps.setInt(12, demandeEncodageDonnees.getNumOrdreCandidature());

      if (ps.executeUpdate() == 0) {
        return null;
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    demandeEncodageDonnees.setNumVersion(1);
    return demandeEncodageDonnees;
  }

  @Override
  public List<DemandeMobiliteDto> visualiserToutesLesDemandes() {
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.demandes_mobilite AS DEM"
            + " LEFT OUTER JOIN projetpae.personnes AS PER ON DEM.id_personne = PER.id_personne"
            + " LEFT OUTER JOIN projetpae.partenaires AS PAR ON DEM.id_partenaire = "
            + "PAR.id_partenaire LEFT OUTER JOIN projetpae.pays AS PAY ON DEM.pays = "
            + "PAY.code_iso WHERE confirme=false ORDER BY DEM.num_ordre_candidature ASC");
    List<DemandeMobiliteDto> listeDemandes = new ArrayList<DemandeMobiliteDto>();
    try {
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        DemandeMobiliteDto dto = creerDemandeMobiliteAvecResultSet(rs);
        listeDemandes.add(dto);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return listeDemandes;
  }

  @Override
  public DemandeMobiliteDto confirmerDemandeMobilite(DemandeMobiliteDto demDto) {
    PreparedStatement ps = dalBes.prepareStatement(
        "UPDATE projetpae.demandes_mobilite SET(confirme, num_version_demande_mobilite, "
            + "id_partenaire, quadrimestre_demande)"
            + " = (?,?,?,?) WHERE id_demande = ? AND num_version_demande_mobilite = ?");
    try {
      ps.setBoolean(1, demDto.isConfirme());
      ps.setInt(2, demDto.getNumVersion() + 1);
      ps.setInt(3, demDto.getPartenaire().getIdPartenaire());
      ps.setInt(4, demDto.getQuadrimestre());
      ps.setInt(5, demDto.getIdDemande());
      ps.setInt(6, demDto.getNumVersion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      demDto.setNumVersion(demDto.getNumVersion() + 1);
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return demDto;
  }



  private DemandeMobiliteDto creerDemandeMobiliteAvecResultSet(ResultSet rs) throws SQLException {
    DemandeMobiliteDto dto = bizzFactory.getDemandeMobilite();
    dto.setIdDemande(rs.getInt("id_demande"));
    PartenaireDto partDto = bizzFactory.getPartenaire();
    PaysDto paysDto = bizzFactory.getPays();
    partDto.setIdPartenaire(rs.getInt("id_partenaire"));
    paysDto.setCodeIso(rs.getString("code_iso"));
    paysDto.setNom(rs.getString("nom_pays"));
    paysDto.setProgramme("programme_pays");
    partDto.setPays(paysDto);
    partDto.setNomLegal(rs.getString("nom_legal"));
    partDto.setNomAffaires(rs.getString("nom_affaires"));
    partDto.setNomComplet(rs.getString("nom_complet"));
    partDto.setCode(rs.getString("code_partenaire"));
    dto.setPartenaire(partDto);
    PersonneDto persDtoTemp = bizzFactory.getPersonne();
    persDtoTemp.setIdPersonne(rs.getInt("id_personne"));
    persDtoTemp.setPseudo(rs.getString("pseudo"));
    persDtoTemp.setNom(rs.getString("nom_personne"));
    persDtoTemp.setPrenom(rs.getString("prenom"));
    persDtoTemp.setEmail(rs.getString("email_personne"));
    persDtoTemp.setRole(rs.getString("role"));
    dto.setPersonne(persDtoTemp);
    dto.setPays(paysDto);
    dto.setConfirme(rs.getBoolean("confirme"));
    dto.setDateIntroduction(rs.getDate("date_introduction").toLocalDate());
    dto.setProgramme(rs.getString("programme_demande"));
    dto.setCode(rs.getString("code_demande"));
    dto.setQuadrimestre(rs.getInt("quadrimestre_demande"));
    dto.setOrdrePreference(rs.getInt("ordre_preference"));
    dto.setAnneeAcademique(rs.getInt("annee_academique"));
    dto.setNumOrdreCandidature(rs.getInt("num_ordre_candidature"));
    dto.setNumVersion(rs.getInt("num_version_demande_mobilite"));
    return dto;
  }

  @Override
  public List<DemandeMobiliteDto> getDemandeMobiParAnnee(DemandeMobiliteDto demDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.demandes_mobilite AS DEM"
            + " LEFT OUTER JOIN projetpae.personnes AS PER ON DEM.id_personne = PER.id_personne"
            + " LEFT OUTER JOIN projetpae.partenaires AS PAR"
            + " ON DEM.id_partenaire = PAR.id_partenaire  LEFT OUTER JOIN projetpae.pays AS PAY"
            + " ON DEM.pays = PAY.code_iso  WHERE DEM.annee_academique = ?");
    List<DemandeMobiliteDto> listeDemandes = new ArrayList<DemandeMobiliteDto>();
    try {
      ps.setInt(1, demDto.getAnneeAcademique());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        DemandeMobiliteDto dto = creerDemandeMobiliteAvecResultSet(rs);
        listeDemandes.add(dto);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return listeDemandes;
  }

  @Override
  public int getNumeroCandidature() {
    PreparedStatement ps = dalBes
        .prepareStatement("SELECT MAX(num_ordre_candidature) FROM projetpae.demandes_mobilite");
    try {
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        return rs.getInt(1);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return 0;
  }
}
