package be.ipl.pae.dal.dao.stub;

import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.dal.dao.interfaces.DemandeMobiliteDao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DemandeMobiliteDaoStub implements DemandeMobiliteDao {

  private Set<DemandeMobiliteDto> demandesMobilites;

  /**
   * constructeur de DemandeMobiliteDaoStub.
   */
  public DemandeMobiliteDaoStub() {
    demandesMobilites = new HashSet<DemandeMobiliteDto>();
  }

  @Override
  public List<DemandeMobiliteDto> visualiserSesDemandes(PersonneDto persDto) {
    List<DemandeMobiliteDto> demandes = new ArrayList<DemandeMobiliteDto>();
    for (DemandeMobiliteDto dem : demandesMobilites) {
      if (dem.getPersonne().getIdPersonne() == persDto.getIdPersonne()) {
        demandes.add(dem);
      }
    }
    return demandes;
  }

  @Override
  public List<DemandeMobiliteDto> visualiserToutesLesDemandes() {
    List<DemandeMobiliteDto> demandes = new ArrayList<DemandeMobiliteDto>();
    for (DemandeMobiliteDto dem : demandesMobilites) {
      demandes.add(dem);
    }
    return demandes;
  }

  @Override
  public DemandeMobiliteDto declarerDemandeMobilites(DemandeMobiliteDto demandeEncodageDonnees) {
    demandesMobilites.add(demandeEncodageDonnees);
    demandeEncodageDonnees.setIdDemande(demandesMobilites.size());
    return demandeEncodageDonnees;
  }

  @Override
  public DemandeMobiliteDto confirmerDemandeMobilite(DemandeMobiliteDto demDto) {
    for (DemandeMobiliteDto dem : demandesMobilites) {
      if (dem.getIdDemande() == demDto.getIdDemande()
          && dem.getNumVersion() == demDto.getIdDemande()) {
        demDto.setNumVersion(demDto.getNumVersion() + 1);
        demDto.setConfirme(true);
        return demDto;
      }
    }
    return null;
  }


  public void reset() {
    demandesMobilites.clear();
  }

  @Override
  public List<DemandeMobiliteDto> getDemandeMobiParAnnee(DemandeMobiliteDto demDto) {
    List<DemandeMobiliteDto> demandes = new ArrayList<DemandeMobiliteDto>();
    for (DemandeMobiliteDto dem : demandesMobilites) {
      if (dem.getAnneeAcademique() == demDto.getAnneeAcademique()) {
        demandes.add(dem);
      }
    }
    return demandes;
  }

  @Override
  public int getNumeroCandidature() {
    int max = 0;
    for (DemandeMobiliteDto dem : demandesMobilites) {
      if (dem.getNumOrdreCandidature() > max) {
        max = dem.getNumOrdreCandidature();
      }
    }
    return max;
  }

}
