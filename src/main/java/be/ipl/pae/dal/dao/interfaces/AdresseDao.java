package be.ipl.pae.dal.dao.interfaces;

import be.ipl.pae.bizz.dto.AdresseDto;

public interface AdresseDao {

  AdresseDto encodageDonnees(AdresseDto adrDto);

}
