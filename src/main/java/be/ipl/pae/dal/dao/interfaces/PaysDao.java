package be.ipl.pae.dal.dao.interfaces;

import be.ipl.pae.bizz.dto.PaysDto;

import java.util.List;

public interface PaysDao {

  List<PaysDto> getAllPays();

  List<String> getProgrammesPays();

  PaysDto getIsoParPays(String nom);

}
