package be.ipl.pae.dal.dao.interfaces;

import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.PersonneDto;

import java.util.List;

public interface DemandeMobiliteDao {

  List<DemandeMobiliteDto> visualiserSesDemandes(PersonneDto persDto);

  List<DemandeMobiliteDto> visualiserToutesLesDemandes();

  DemandeMobiliteDto declarerDemandeMobilites(DemandeMobiliteDto demandeEncodageDonnees);

  DemandeMobiliteDto confirmerDemandeMobilite(DemandeMobiliteDto demDto);

  List<DemandeMobiliteDto> getDemandeMobiParAnnee(DemandeMobiliteDto demDto);

  int getNumeroCandidature();

}
