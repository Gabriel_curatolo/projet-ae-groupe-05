package be.ipl.pae.dal.dao.impl;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.dal.dao.interfaces.PersonneDao;
import be.ipl.pae.dal.services.DalBackEndServices;
import be.ipl.pae.exceptions.DalException;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PersonneDaoImpl implements PersonneDao {

  private DalBackEndServices dalBes;
  private BizzFactory bizzFactory;

  /**
   * constructeur de PersonneDaoImpl.
   * 
   * @param dalBes dalBackEndServices
   * 
   * @param bizzFactory bizzFactory
   */
  public PersonneDaoImpl(DalBackEndServices dalBes, BizzFactory bizzFactory) {
    super();
    this.dalBes = dalBes;
    this.bizzFactory = bizzFactory;
  }


  @Override
  public PersonneDto getPersonneParPseudo(String pseudo) {

    PreparedStatement ps = dalBes.prepareStatement("SELECT * FROM projetpae.personnes AS PER "
        + "LEFT OUTER JOIN projetpae.adresses ADR ON PER.adresse = ADR.id_adresse "
        + "LEFT OUTER JOIN projetpae.pays AS PAY ON PER.nationalite = PAY.code_iso "
        + "WHERE pseudo = ?");
    try {
      ps.setString(1, pseudo);
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        PersonneDto ret = creerPersonneAvecResultSet(rs);
        ret.setMotDePasse(rs.getString("mot_de_passe"));
        return ret;
      }

    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return null;
  }



  @Override
  public boolean inscription(PersonneDto nouvelInscrit) {
    PreparedStatement ps = dalBes.prepareStatement(
        "INSERT INTO projetpae.personnes (pseudo, nom_personne, prenom, mot_de_passe, "
            + "email_personne, date_inscription, role, num_version_personne) Values "
            + "(?,?,?,?,?,?,?,?)",
        Statement.RETURN_GENERATED_KEYS);
    PreparedStatement ps2 =
        dalBes.prepareStatement("SELECT * FROM projetpae.personnes WHERE id_personne = 1");
    try {
      ResultSet rs = ps2.executeQuery();
      if (!rs.next()) {
        ps.setString(1, nouvelInscrit.getPseudo());
        ps.setString(2, nouvelInscrit.getNom());
        ps.setString(3, nouvelInscrit.getPrenom());
        ps.setString(4, nouvelInscrit.getMotDePasse());
        ps.setString(5, nouvelInscrit.getEmail());
        Date date = Date.valueOf(LocalDate.now());
        ps.setDate(6, date);
        ps.setString(7, "pr");
        ps.setInt(8, nouvelInscrit.getNumVersion());
        if (ps.executeUpdate() == 0) {
          return false;
        }
      } else {
        ps.setString(1, nouvelInscrit.getPseudo());
        ps.setString(2, nouvelInscrit.getNom());
        ps.setString(3, nouvelInscrit.getPrenom());
        ps.setString(4, nouvelInscrit.getMotDePasse());
        ps.setString(5, nouvelInscrit.getEmail());
        Date date = Date.valueOf(LocalDate.now());
        ps.setDate(6, date);
        ps.setString(7, "et");
        ps.setInt(8, nouvelInscrit.getNumVersion());
        if (ps.executeUpdate() == 0) {
          return false;
        }
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return true;
  }

  @Override
  public PersonneDto getPersonneParEmail(String email) {
    PreparedStatement ps = dalBes.prepareStatement("SELECT * FROM projetpae.personnes AS PER "
        + " LEFT OUTER JOIN projetpae.adresses ADR ON PER.adresse = ADR.id_adresse"
        + " LEFT OUTER JOIN projetpae.pays AS PAY ON PER.nationalite = PAY.code_iso "
        + "WHERE email_personne = ?");
    try {
      ps.setString(1, email);
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        PersonneDto ret = creerPersonneAvecResultSet(rs);
        return ret;
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return null;
  }

  @Override
  public PersonneDto encodageDonnees(PersonneDto dtoPersEncodageDonnees) {
    PreparedStatement ps = dalBes.prepareStatement(
        "UPDATE projetpae.personnes SET(telephone_personne, civilite, date_naissance,"
            + " sexe, no_compte, nb_annees_reussies, titulaire_compte, banque, code_BIC, "
            + "departement_personne, nationalite, adresse, num_version_personne)"
            + " = (?,?,?,?,?,?,?,?,?,?,?,?,?) WHERE id_personne = ? AND num_version_personne = ?");
    try {
      ps.setString(1, dtoPersEncodageDonnees.getTelephone());
      ps.setString(2, dtoPersEncodageDonnees.getCivilite());
      ps.setString(3, dtoPersEncodageDonnees.getDateNaissance());
      ps.setString(4, dtoPersEncodageDonnees.getSexe());
      ps.setString(5, dtoPersEncodageDonnees.getNoCompte());
      ps.setInt(6, dtoPersEncodageDonnees.getNbAnneeReussies());
      ps.setString(7, dtoPersEncodageDonnees.getTitulaireCompte());
      ps.setString(8, dtoPersEncodageDonnees.getBanque());
      ps.setString(9, dtoPersEncodageDonnees.getCodeBic());
      ps.setString(10, dtoPersEncodageDonnees.getDepartement());
      ps.setString(11, dtoPersEncodageDonnees.getNationalite().getCodeIso());
      ps.setInt(12, dtoPersEncodageDonnees.getAdresse().getIdAdresse());
      ps.setInt(13, dtoPersEncodageDonnees.getNumVersion() + 1);
      ps.setInt(14, dtoPersEncodageDonnees.getIdPersonne());
      ps.setInt(15, dtoPersEncodageDonnees.getNumVersion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    dtoPersEncodageDonnees.setNumVersion(dtoPersEncodageDonnees.getNumVersion() + 1);
    return dtoPersEncodageDonnees;
  }


  @Override
  public List<PersonneDto> getAllPersonnes() {
    PreparedStatement ps = dalBes.prepareStatement("SELECT * FROM projetpae.personnes AS PER LEFT"
        + " OUTER JOIN projetpae.adresses ADR ON" + " PER.adresse = ADR.id_adresse LEFT OUTER JOIN "
        + "projetpae.pays AS PAY ON PER.nationalite = PAY.code_iso ORDER BY nom_personne ASC");
    List<PersonneDto> list = new ArrayList<PersonneDto>();
    try {
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PersonneDto ret = creerPersonneAvecResultSet(rs);
        list.add(ret);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;
  }


  @Override
  public PersonneDto visualiserUnUtilisateur(PersonneDto persDto) {
    PreparedStatement ps = dalBes.prepareStatement("SELECT * FROM projetpae.personnes AS PER "
        + "LEFT OUTER JOIN projetpae.adresses ADR ON PER.adresse = ADR.id_adresse "
        + "LEFT OUTER JOIN projetpae.pays AS PAY ON PER.nationalite = PAY.code_iso "
        + "WHERE nom_personne = ? AND prenom = ?");
    try {
      ps.setString(1, persDto.getNom());
      ps.setString(2, persDto.getPrenom());
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        PersonneDto ret = creerPersonneAvecResultSet(rs);
        return ret;
      } else {
        return null;
      }
    } catch (Exception exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public List<PersonneDto> rechercheNom(PersonneDto persDto) {
    PreparedStatement ps = dalBes.prepareStatement("SELECT * FROM projetpae.personnes AS PER "
        + "LEFT OUTER JOIN projetpae.adresses ADR ON PER.adresse = ADR.id_adresse "
        + "LEFT OUTER JOIN projetpae.pays AS PAY ON PER.nationalite = PAY.code_iso "
        + "WHERE nom_personne = ? ");
    try {
      ps.setString(1, persDto.getNom());
      ResultSet rs = ps.executeQuery();
      List<PersonneDto> listePers = new ArrayList<PersonneDto>();
      while (rs.next()) {
        PersonneDto ret = creerPersonneAvecResultSet(rs);
        listePers.add(ret);
      }
      return listePers;

    } catch (Exception exception) {
      throw new DalException(exception);
    }
  }


  @Override
  public List<PersonneDto> recherchePrenom(PersonneDto persDto) {
    PreparedStatement ps = dalBes.prepareStatement("SELECT * FROM projetpae.personnes AS PER "
        + "LEFT OUTER JOIN projetpae.adresses ADR ON PER.adresse = ADR.id_adresse "
        + "LEFT OUTER JOIN projetpae.pays AS PAY ON PER.nationalite = PAY.code_iso "
        + "WHERE prenom = ? ");
    try {
      ps.setString(1, persDto.getPrenom());
      ResultSet rs = ps.executeQuery();
      List<PersonneDto> listePers = new ArrayList<PersonneDto>();
      while (rs.next()) {
        PersonneDto ret = creerPersonneAvecResultSet(rs);
        listePers.add(ret);
      }
      return listePers;

    } catch (Exception exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public List<PersonneDto> rechercheNomPrenom(PersonneDto persDto) {
    PreparedStatement ps = dalBes.prepareStatement("SELECT * FROM projetpae.personnes AS PER "
        + "LEFT OUTER JOIN projetpae.adresses ADR ON PER.adresse = ADR.id_adresse "
        + "LEFT OUTER JOIN projetpae.pays AS PAY ON PER.nationalite = PAY.code_iso "
        + "WHERE nom_personne = ? AND prenom = ? ");
    try {
      ps.setString(1, persDto.getNom());
      ps.setString(2, persDto.getPrenom());
      ResultSet rs = ps.executeQuery();
      List<PersonneDto> listePers = new ArrayList<PersonneDto>();
      while (rs.next()) {
        PersonneDto ret = creerPersonneAvecResultSet(rs);
        listePers.add(ret);
      }
      return listePers;
    } catch (Exception exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public PersonneDto getPersonneParId(PersonneDto persDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.personnes AS PER LEFT OUTER JOIN "
            + "projetpae.adresses ADR ON PER.adresse = "
            + "ADR.id_adresse LEFT OUTER JOIN projetpae.pays "
            + "AS PAY ON PER.nationalite = PAY.code_iso" + " WHERE id_personne = ?");
    try {
      ps.setInt(1, persDto.getIdPersonne());
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        PersonneDto ret = creerPersonneAvecResultSet(rs);
        return ret;
      } else {
        return null;
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }


  @Override
  public PersonneDto augmenterProfesseur(PersonneDto persDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("UPDATE projetpae.personnes SET(role, num_version_personne) = (?,?)"
            + " WHERE id_personne = ? AND num_version_personne = ?");
    try {
      ps.setString(1, persDto.getRole());
      ps.setInt(2, persDto.getNumVersion() + 1);
      ps.setInt(3, persDto.getIdPersonne());
      ps.setInt(4, persDto.getNumVersion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    persDto.setNumVersion(persDto.getNumVersion() + 1);
    return persDto;
  }

  private PersonneDto creerPersonneAvecResultSet(ResultSet rs) throws SQLException {
    PersonneDto ret = bizzFactory.getPersonne();
    ret.setIdPersonne(rs.getInt("id_personne"));
    ret.setPseudo(rs.getString("pseudo"));
    ret.setNom(rs.getString("nom_personne"));
    ret.setPrenom(rs.getString("prenom"));
    ret.setEmail(rs.getString("email_personne"));
    ret.setTelephone(rs.getString("telephone_personne"));
    ret.setDateInscription(rs.getDate("date_inscription").toLocalDate());
    ret.setRole(rs.getString("role"));
    ret.setCivilite(rs.getString("civilite"));
    ret.setDateNaissance(rs.getString("date_naissance"));
    ret.setSexe(rs.getString("sexe"));
    ret.setNoCompte(rs.getString("no_compte"));
    ret.setNbAnneeReussies(rs.getInt("nb_annees_reussies"));
    ret.setTitulaireCompte(rs.getString("titulaire_compte"));
    ret.setBanque(rs.getString("banque"));
    ret.setCodeBic(rs.getString("code_bic"));
    ret.setDepartement(rs.getString("departement_personne"));
    PaysDto paysDto = bizzFactory.getPays();
    paysDto.setCodeIso(rs.getString("nationalite"));
    if (paysDto.getCodeIso() != null) {
      paysDto.setProgramme(rs.getString("programme_pays"));
      paysDto.setNom(rs.getString("nom_pays"));
    }
    AdresseDto adrDto = bizzFactory.getAdresse();
    adrDto.setIdAdresse(rs.getInt("adresse"));
    if (adrDto.getIdAdresse() != 0) {
      adrDto.setRue(rs.getString("rue"));
      adrDto.setNumero(rs.getInt("numero"));
      adrDto.setCodePostal(rs.getString("code_postal"));
      adrDto.setVille(rs.getString("ville"));
      adrDto.setCommune(rs.getString("commune"));
      adrDto.setBoite(rs.getString("boite"));
      adrDto.setRegion(rs.getString("region"));
    }
    ret.setNationalite(paysDto);
    ret.setAdresse(adrDto);
    ret.setNumVersion(rs.getInt("num_version_personne"));
    return ret;
  }


  @Override
  public List<PersonneDto> getEtudiants() {
    PreparedStatement ps = dalBes.prepareStatement("SELECT * FROM projetpae.personnes AS PER "
        + "LEFT OUTER JOIN projetpae.adresses ADR ON PER.adresse = ADR.id_adresse "
        + "LEFT OUTER JOIN projetpae.pays AS PAY ON PER.nationalite = PAY.code_iso "
        + "WHERE role = 'et'");
    List<PersonneDto> list = new ArrayList<PersonneDto>();
    try {
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PersonneDto ret = creerPersonneAvecResultSet(rs);
        list.add(ret);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;
  }



}
