package be.ipl.pae.dal.dao.stub;

import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.dal.dao.interfaces.PersonneDao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PersonneDaoStub implements PersonneDao {

  private Set<PersonneDto> personnes;

  /**
   * constructeur de PersonneDaoStub.
   * 
   */
  public PersonneDaoStub() {
    super();
    personnes = new HashSet<PersonneDto>();
  }

  @Override
  public PersonneDto getPersonneParPseudo(String pseudo) {
    for (PersonneDto pers : personnes) {
      if (pers.getPseudo().equals(pseudo)) {
        return pers;
      }
    }
    return null;
  }

  @Override
  public PersonneDto getPersonneParEmail(String email) {
    for (PersonneDto pers : personnes) {
      if (pers.getEmail().equals(email)) {
        return pers;
      }
    }
    return null;
  }

  @Override
  public boolean inscription(PersonneDto nouvelInscrit) {
    personnes.add(nouvelInscrit);
    nouvelInscrit.setIdPersonne(personnes.size());
    return true;
  }

  @Override
  public PersonneDto encodageDonnees(PersonneDto dtoEncode) {
    for (PersonneDto p : personnes) {
      if (p.getIdPersonne() == dtoEncode.getIdPersonne()) {
        personnes.remove(p);
        personnes.add(dtoEncode);
        dtoEncode.setIdPersonne(p.getIdPersonne());
        dtoEncode.setNumVersion(dtoEncode.getNumVersion() + 1);
        return dtoEncode;
      }
    }
    return null;
  }

  @Override
  public List<PersonneDto> getAllPersonnes() {
    return new ArrayList<PersonneDto>(personnes);
  }

  @Override
  public PersonneDto visualiserUnUtilisateur(PersonneDto persDto) {
    for (PersonneDto pers : personnes) {
      if (pers.getNom().equals(persDto.getNom()) && pers.getPrenom().equals(persDto.getPrenom())) {
        return pers;
      }
    }
    return null;
  }


  @Override
  public PersonneDto getPersonneParId(PersonneDto persDto) {
    for (PersonneDto pers : personnes) {
      if (pers.getIdPersonne() == persDto.getIdPersonne()) {
        return pers;
      }
    }
    return null;
  }

  /**
   * reset reset la liste de personnes.
   */
  public void reset() {
    personnes.clear();
  }

  @Override
  public PersonneDto augmenterProfesseur(PersonneDto persDto) {
    for (PersonneDto pers : personnes) {
      if (pers.getIdPersonne() == persDto.getIdPersonne()) {
        pers.setRole("p");
        return pers;
      }
    }
    return null;
  }

  @Override
  public List<PersonneDto> rechercheNom(PersonneDto persDto) {
    List<PersonneDto> listPers = new ArrayList<PersonneDto>();
    for (PersonneDto pers : personnes) {
      if (pers.getNom() == persDto.getNom()) {
        listPers.add(pers);
      }
    }
    return listPers;
  }


  @Override
  public List<PersonneDto> recherchePrenom(PersonneDto persDto) {
    List<PersonneDto> listPers = new ArrayList<PersonneDto>();
    for (PersonneDto pers : personnes) {
      if (pers.getPrenom() == persDto.getPrenom()) {
        listPers.add(pers);
      }
    }
    return listPers;
  }

  @Override
  public List<PersonneDto> rechercheNomPrenom(PersonneDto persDto) {
    List<PersonneDto> listPers = new ArrayList<PersonneDto>();
    for (PersonneDto pers : personnes) {
      if (pers.getNom() == persDto.getNom() && pers.getPrenom() == persDto.getPrenom()) {
        listPers.add(pers);
      }
    }
    return listPers;
  }

  @Override
  public List<PersonneDto> getEtudiants() {
    List<PersonneDto> listPers = new ArrayList<PersonneDto>();
    for (PersonneDto pers : personnes) {
      if (pers.getRole().equals("et")) {
        listPers.add(pers);
      }
    }
    return listPers;
  }


}
