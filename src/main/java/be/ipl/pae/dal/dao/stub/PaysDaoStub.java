package be.ipl.pae.dal.dao.stub;

import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.dal.dao.interfaces.PaysDao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PaysDaoStub implements PaysDao {

  private Set<PaysDto> setPays;

  /**
   * constructeur de PaysDaoStub.
   * 
   */
  public PaysDaoStub(BizzFactory bizzFactory) {
    super();
    this.setPays = new HashSet<PaysDto>();
    PaysDto pays = bizzFactory.getPays();

    setPays.add(pays);
  }

  @Override
  public List<PaysDto> getAllPays() {
    List<PaysDto> pays = new ArrayList<PaysDto>();
    for (PaysDto pa : setPays) {
      pays.add(pa);
    }
    return pays;
  }

  @Override
  public List<String> getProgrammesPays() {

    List<String> ret = new ArrayList<String>();
    for (PaysDto pays : setPays) {
      ret.add(pays.getProgramme());
    }
    return ret;
  }

  @Override
  public PaysDto getIsoParPays(String nom) {

    for (PaysDto pays : setPays) {
      if (pays.getNom().equals(nom)) {
        return pays;
      }
    }
    return null;
  }


}
