package be.ipl.pae.dal.dao.interfaces;

import be.ipl.pae.bizz.dto.PersonneDto;

import java.util.List;

public interface PersonneDao {

  /*
   * recupere une personne par son pseudo
   * 
   * @param pseudo de la personne
   * 
   * @return une enveloppe remplie si reussie, null si ratee
   */
  PersonneDto getPersonneParPseudo(String pseudo);

  /*
   * inscris une personne dans la bd
   * 
   * @param nouvelInscrit le nouvel inscrit
   * 
   * @return true si l'inscription a reussie, false si ratee
   */
  boolean inscription(PersonneDto nouvelInscrit);

  /*
   * recupere une personne par son email
   * 
   * @param email de la personne
   * 
   * @return personne si elle existe, null sinon
   */
  PersonneDto getPersonneParEmail(String email);

  /*
   * 
   * 
   * @param
   * 
   * @return
   */
  PersonneDto encodageDonnees(PersonneDto dtoPersEncodageDonnees);

  List<PersonneDto> getAllPersonnes();

  PersonneDto visualiserUnUtilisateur(PersonneDto persDto);

  PersonneDto getPersonneParId(PersonneDto persDto);

  PersonneDto augmenterProfesseur(PersonneDto persDto);

  List<PersonneDto> rechercheNom(PersonneDto persDto);

  List<PersonneDto> recherchePrenom(PersonneDto persDto);

  List<PersonneDto> rechercheNomPrenom(PersonneDto persDto);

  List<PersonneDto> getEtudiants();

}
