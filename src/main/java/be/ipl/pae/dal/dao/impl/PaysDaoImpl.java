package be.ipl.pae.dal.dao.impl;

import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.dal.dao.interfaces.PaysDao;
import be.ipl.pae.dal.services.DalBackEndServices;
import be.ipl.pae.exceptions.DalException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PaysDaoImpl implements PaysDao {

  private DalBackEndServices dalBes;
  private BizzFactory bizzFactory;

  /**
   * constructeur de PaysDaoImpl.
   * 
   * @param dalBes dalBackEndServices
   * 
   * @param bizzFactory bizzFactory
   */
  public PaysDaoImpl(DalBackEndServices dalBes, BizzFactory bizzFactory) {
    super();
    this.dalBes = dalBes;
    this.bizzFactory = bizzFactory;
  }

  @Override
  public List<PaysDto> getAllPays() {
    PreparedStatement ps = dalBes.prepareStatement("Select * FROM projetpae.pays");
    List<PaysDto> list = new ArrayList<PaysDto>();
    try {
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PaysDto ret = creerPaysAvecResultSet(rs);
        list.add(ret);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }

    return list;
  }

  @Override
  public List<String> getProgrammesPays() {
    PreparedStatement ps =
        dalBes.prepareStatement("Select DISTINCT programme_pays FROM projetpae.pays");
    List<String> list = new ArrayList<String>();

    try {
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        String programme = rs.getString("programme_pays");
        list.add(programme);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;
  }

  @Override
  public PaysDto getIsoParPays(String nom) {
    PreparedStatement ps = dalBes.prepareStatement("Select * FROM projetpae.pays WHERE nom_pays=?");

    try {
      ps.setString(1, nom);
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        PaysDto ret = creerPaysAvecResultSet(rs);
        return ret;
      }
    } catch (SQLException exception) {
      throw new DalException();
    }
    return null;
  }

  private PaysDto creerPaysAvecResultSet(ResultSet rs) throws SQLException {
    PaysDto ret = bizzFactory.getPays();
    ret.setCodeIso(rs.getString("code_iso"));
    ret.setNom(rs.getString("nom_pays"));
    ret.setProgramme(rs.getString("programme_pays"));
    return ret;
  }
}
