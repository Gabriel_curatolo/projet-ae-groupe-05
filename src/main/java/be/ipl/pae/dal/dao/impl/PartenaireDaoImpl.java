package be.ipl.pae.dal.dao.impl;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.dal.dao.interfaces.PartenaireDao;
import be.ipl.pae.dal.services.DalBackEndServices;
import be.ipl.pae.exceptions.DalException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PartenaireDaoImpl implements PartenaireDao {

  private DalBackEndServices dalBackServices;
  private BizzFactory bizFact;

  public PartenaireDaoImpl(DalBackEndServices dalBackServices, BizzFactory bizFact) {
    this.dalBackServices = dalBackServices;
    this.bizFact = bizFact;
  }

  @Override
  public List<PartenaireDto> getPartenaires() {
    List<PartenaireDto> list = new ArrayList<PartenaireDto>();
    PreparedStatement ps = dalBackServices.prepareStatement(
        "SELECT * FROM projetpae.partenaires AS PAR LEFT OUTER JOIN projetpae.pays "
            + "AS PAYS ON PAR.pays = PAYS.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse");
    try {
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PartenaireDto partDto = bizFact.getPartenaire();
        partDto = creerPartenaire(rs);
        list.add(partDto);
      }

    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;

  }

  @Override
  public PartenaireDto getPartenaireParNomComplet(PartenaireDto partDto) {
    PartenaireDto ret = bizFact.getPartenaire();
    PreparedStatement ps = dalBackServices
        .prepareStatement("Select * FROM projetpae.partenaires WHERE nom_complet = ?");
    try {
      ps.setString(1, partDto.getNomComplet());
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        ret.setIdPartenaire(rs.getInt("id_partenaire"));
        ret.setCode(rs.getString("code_partenaire"));
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return ret;
  }


  @Override
  public PartenaireDto introduirePartenaire(PartenaireDto partDto) {
    PartenaireDto ret = bizFact.getPartenaire();
    PreparedStatement ps = dalBackServices
        .prepareStatement("INSERT INTO projetpae.partenaires (pays, adresse, nom_legal, "
            + "nom_affaires, nom_complet, code_partenaire, type_organisation,"
            + " departement_partenaire, "
            + "nb_employes, email_partenaire, site_web, telephone_partenaire) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
    try {
      ps.setString(1, partDto.getPays().getCodeIso());
      ps.setInt(2, partDto.getAdresse().getIdAdresse());
      ps.setString(3, partDto.getNomLegal());
      ps.setString(4, partDto.getNomAffaires());
      ps.setString(5, partDto.getNomComplet());
      ps.setString(6, partDto.getCode());
      ps.setString(7, partDto.getTypeOrganisation());
      ps.setString(8, partDto.getDepartement());
      ps.setInt(9, partDto.getNbEmployes());
      ps.setString(10, partDto.getEmail());
      ps.setString(11, partDto.getSiteWeb());
      ps.setString(12, partDto.getTelephone());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      ResultSet rs = ps.getGeneratedKeys();
      if (rs.next()) {
        ret.setIdPartenaire(rs.getInt("id_partenaire"));

      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return ret;
  }

  @Override
  public List<PartenaireDto> getPartenairesParVille(PartenaireDto partenaire) {
    List<PartenaireDto> list = new ArrayList<PartenaireDto>();
    PreparedStatement ps = dalBackServices.prepareStatement(
        "SELECT * FROM projetpae.partenaires AS PAR LEFT OUTER JOIN projetpae.pays "
            + "AS PAYS ON PAR.pays = PAYS.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse  WHERE ADR.ville = ?");
    try {
      ps.setString(1, partenaire.getAdresse().getVille());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PartenaireDto partDto = creerPartenaire(rs);
        list.add(partDto);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;

  }

  @Override
  public List<PartenaireDto> getPartenairesParNomPays(PartenaireDto partenaire) {
    List<PartenaireDto> list = new ArrayList<PartenaireDto>();
    PreparedStatement ps = dalBackServices.prepareStatement(
        "SELECT * FROM projetpae.partenaires AS PAR LEFT OUTER JOIN projetpae.pays "
            + "AS PAYS ON PAR.pays = PAYS.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse WHERE PAYS.nom_pays = ?");
    try {
      ps.setString(1, partenaire.getPays().getNom());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PartenaireDto partDto = creerPartenaire(rs);
        list.add(partDto);
      }

    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;

  }

  @Override
  public List<PartenaireDto> getPartenairesParNom(PartenaireDto partenaire) {
    List<PartenaireDto> list = new ArrayList<PartenaireDto>();
    PreparedStatement ps = dalBackServices.prepareStatement(
        "SELECT * FROM projetpae.partenaires AS PAR LEFT OUTER JOIN projetpae.pays "
            + "AS PAYS ON PAR.pays = PAYS.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse WHERE PAR.nom_complet = ?");
    try {
      ps.setString(1, partenaire.getNomComplet());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PartenaireDto partDto = creerPartenaire(rs);
        list.add(partDto);
      }

    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;

  }

  private PartenaireDto creerPartenaire(ResultSet rs) throws SQLException {
    PartenaireDto partDto = bizFact.getPartenaire();
    PaysDto paysDto = bizFact.getPays();
    partDto.setIdPartenaire(rs.getInt("id_partenaire"));
    paysDto.setCodeIso(rs.getString("code_iso"));
    paysDto.setProgramme(rs.getString("programme_pays"));
    paysDto.setNom(rs.getString("nom_pays"));
    partDto.setPays(paysDto);
    AdresseDto adrDto = bizFact.getAdresse();
    adrDto.setIdAdresse(rs.getInt("adresse"));
    adrDto.setRue(rs.getString("rue"));
    adrDto.setNumero(rs.getInt("numero"));
    adrDto.setCodePostal(rs.getString("code_postal"));
    adrDto.setVille(rs.getString("ville"));
    adrDto.setCommune(rs.getString("commune"));
    adrDto.setBoite(rs.getString("boite"));
    adrDto.setRegion(rs.getString("region"));
    partDto.setAdresse(adrDto);
    partDto.setNomLegal(rs.getString("nom_legal"));
    partDto.setNomAffaires(rs.getString("nom_affaires"));
    partDto.setNomComplet(rs.getString("nom_complet"));
    partDto.setCode(rs.getString("code_partenaire"));
    partDto.setTelephone(rs.getString("telephone_partenaire"));
    partDto.setTypeOrganisation(rs.getString("type_organisation"));
    partDto.setDepartement(rs.getString("departement_partenaire"));
    partDto.setNbEmployes(rs.getInt("nb_employes"));
    partDto.setEmail(rs.getString("email_partenaire"));
    partDto.setSiteWeb(rs.getString("site_web"));
    return partDto;

  }

  @Override
  public List<PartenaireDto> getPartenairesParVilleEtPays(PartenaireDto partenaire) {
    List<PartenaireDto> list = new ArrayList<PartenaireDto>();
    PreparedStatement ps = dalBackServices.prepareStatement(
        "Select * FROM projetpae.partenaires AS PART LEFT OUTER JOIN projetpae.pays AS PAYS "
            + "ON PART.pays = PAYS.code_iso LEFT OUTER JOIN projetpae.adresses AS ADR"
            + "ON PART.adresse = ADR.id_adresse WHERE PAYS.nom_pays = ? AND ADR.ville = ?");
    try {
      ps.setString(1, partenaire.getPays().getNom());
      ps.setString(2, partenaire.getAdresse().getVille());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PartenaireDto partDto = creerPartenaire(rs);
        list.add(partDto);
      }

    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;
  }

  @Override
  public List<PartenaireDto> getPartenairesParNomEtVille(PartenaireDto partenaire) {
    List<PartenaireDto> list = new ArrayList<PartenaireDto>();
    PreparedStatement ps = dalBackServices.prepareStatement(
        "Select * FROM projetpae.partenaires AS PART LEFT OUTER JOIN projetpae.pays AS PAYS "
            + "ON PART.pays = PAYS.code_iso LEFT OUTER JOIN projetpae.adresses AS ADR"
            + "ON PART.adresse = ADR.id_adresse WHERE PART.nom_complet = ? AND ADR.ville = ?");
    try {
      ps.setString(1, partenaire.getNomComplet());
      ps.setString(2, partenaire.getAdresse().getVille());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PartenaireDto partDto = creerPartenaire(rs);
        list.add(partDto);
      }

    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;
  }

  @Override
  public List<PartenaireDto> getPartenairesParNomEtPays(PartenaireDto partenaire) {
    List<PartenaireDto> list = new ArrayList<PartenaireDto>();
    PreparedStatement ps = dalBackServices.prepareStatement(
        "Select * FROM projetpae.partenaires AS PART LEFT OUTER JOIN projetpae.pays AS PAYS "
            + "ON PART.pays = PAYS.code_iso LEFT OUTER JOIN projetpae.adresses AS ADR"
            + "ON PART.adresse = ADR.id_adresse WHERE PART.nom_complet = ? AND PAYS.nom_pays = ?");
    try {
      ps.setString(1, partenaire.getNomComplet());
      ps.setString(2, partenaire.getPays().getNom());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PartenaireDto partDto = creerPartenaire(rs);
        list.add(partDto);
      }

    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;
  }

  @Override
  public List<PartenaireDto> getPartenairesParNomEtPaysEtVille(PartenaireDto partenaire) {
    List<PartenaireDto> list = new ArrayList<PartenaireDto>();
    PreparedStatement ps = dalBackServices.prepareStatement(
        "Select * FROM projetpae.partenaires AS PART LEFT OUTER JOIN projetpae.pays AS PAYS "
            + "ON PART.pays = PAYS.code_iso LEFT OUTER JOIN projetpae.adresses AS ADR"
            + "ON PART.adresse = ADR.id_adresse WHERE PART.nom_complet = ? AND PAYS.nom_pays = ?"
            + "AND ADR.ville = ?");
    try {
      ps.setString(1, partenaire.getNomComplet());
      ps.setString(2, partenaire.getPays().getNom());
      ps.setString(3, partenaire.getAdresse().getVille());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PartenaireDto partDto = creerPartenaire(rs);
        list.add(partDto);
      }

    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return list;
  }
}
