package be.ipl.pae.dal.dao.stub;

import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.dal.dao.interfaces.PartenaireDao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PartenaireDaoStub implements PartenaireDao {

  private Set<PartenaireDto> partenaires;
  private BizzFactory bizFact;

  /**
   * constructeur de PartenaireDaoStub.
   * 
   * @param bizFact le bizzFactory
   */
  public PartenaireDaoStub(BizzFactory bizFact) {
    partenaires = new HashSet<PartenaireDto>();
    this.bizFact = bizFact;
  }


  @Override
  public List<PartenaireDto> getPartenaires() {
    List<PartenaireDto> part = new ArrayList<PartenaireDto>();
    for (PartenaireDto partenaire : partenaires) {
      part.add(partenaire);
    }
    return part;
  }

  @Override
  public PartenaireDto getPartenaireParNomComplet(PartenaireDto partDto) {
    PartenaireDto partenaire = bizFact.getPartenaire();
    for (PartenaireDto pa : partenaires) {
      if (pa.getNomComplet().equals(partDto.getNomComplet())) {
        partenaire.setIdPartenaire(pa.getIdPartenaire());
        partenaire.setCode(pa.getCode());
      }
    }
    return partenaire;
  }


  @Override
  public PartenaireDto introduirePartenaire(PartenaireDto partDto) {
    partenaires.add(partDto);
    partDto.setIdPartenaire(partenaires.size());
    return partDto;
  }


  public void reset() {
    partenaires.clear();
  }


  @Override
  public List<PartenaireDto> getPartenairesParVille(PartenaireDto partenaire) {
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    for (PartenaireDto part : partenaires) {
      if (part.getAdresse().getVille().equals(partenaire.getAdresse().getVille())) {
        listPart.add(part);
      }
    }
    return listPart;
  }


  @Override
  public List<PartenaireDto> getPartenairesParNomPays(PartenaireDto partenaire) {
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    for (PartenaireDto part : partenaires) {
      if (part.getPays().getNom().equals(partenaire.getPays().getNom())) {
        listPart.add(part);
      }
    }
    return listPart;
  }


  @Override
  public List<PartenaireDto> getPartenairesParNom(PartenaireDto partenaire) {
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    for (PartenaireDto part : partenaires) {
      if (part.getNomComplet().equals(partenaire.getNomComplet())) {
        listPart.add(part);
      }
    }
    return listPart;
  }


  @Override
  public List<PartenaireDto> getPartenairesParVilleEtPays(PartenaireDto partenaire) {
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    for (PartenaireDto part : partenaires) {
      if (part.getPays().getNom().equals(partenaire.getPays().getNom())
          && part.getAdresse().getVille().equals(partenaire.getAdresse().getVille())) {
        listPart.add(part);
      }
    }
    return listPart;
  }


  @Override
  public List<PartenaireDto> getPartenairesParNomEtVille(PartenaireDto partenaire) {
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    for (PartenaireDto part : partenaires) {
      if (part.getNomComplet().equals(partenaire.getNomComplet())
          && part.getAdresse().getVille().equals(partenaire.getAdresse().getVille())) {
        listPart.add(part);
      }
    }
    return listPart;
  }


  @Override
  public List<PartenaireDto> getPartenairesParNomEtPays(PartenaireDto partenaire) {
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    for (PartenaireDto part : partenaires) {
      if (part.getPays().getNom().equals(partenaire.getPays().getNom())
          && part.getNomComplet().equals(partenaire.getNomComplet())) {
        listPart.add(part);
      }
    }
    return listPart;
  }


  @Override
  public List<PartenaireDto> getPartenairesParNomEtPaysEtVille(PartenaireDto partenaire) {
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    for (PartenaireDto part : partenaires) {
      if (part.getPays().getNom().equals(partenaire.getPays().getNom())
          && part.getNomComplet().equals(partenaire.getNomComplet())
          && part.getAdresse().getVille().equals(partenaire.getAdresse().getVille())) {
        listPart.add(part);
      }
    }
    return listPart;
  }

}
