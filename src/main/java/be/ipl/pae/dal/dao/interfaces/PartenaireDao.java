package be.ipl.pae.dal.dao.interfaces;

import be.ipl.pae.bizz.dto.PartenaireDto;

import java.util.List;

public interface PartenaireDao {

  List<PartenaireDto> getPartenaires();

  PartenaireDto getPartenaireParNomComplet(PartenaireDto partDto);

  PartenaireDto introduirePartenaire(PartenaireDto partDto);

  List<PartenaireDto> getPartenairesParVille(PartenaireDto partenaire);

  List<PartenaireDto> getPartenairesParNomPays(PartenaireDto partenaire);

  List<PartenaireDto> getPartenairesParNom(PartenaireDto partenaire);

  List<PartenaireDto> getPartenairesParVilleEtPays(PartenaireDto partenaire);

  List<PartenaireDto> getPartenairesParNomEtVille(PartenaireDto partenaire);

  List<PartenaireDto> getPartenairesParNomEtPays(PartenaireDto partenaire);

  List<PartenaireDto> getPartenairesParNomEtPaysEtVille(PartenaireDto partenaire);


}
