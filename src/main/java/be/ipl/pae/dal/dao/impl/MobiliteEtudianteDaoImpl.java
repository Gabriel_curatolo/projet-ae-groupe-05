package be.ipl.pae.dal.dao.impl;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.dal.dao.interfaces.MobiliteEtudianteDao;
import be.ipl.pae.dal.services.DalBackEndServices;
import be.ipl.pae.exceptions.DalException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MobiliteEtudianteDaoImpl implements MobiliteEtudianteDao {
  private DalBackEndServices dalBes;
  private BizzFactory bizzFactory;

  /**
   * constructeur de AdresseDaoImpl.
   * 
   * @param bizzFactory le bizzFactory
   * @param dalBes le dalbackendServices
   */
  public MobiliteEtudianteDaoImpl(DalBackEndServices dalBes, BizzFactory bizzFactory) {
    super();
    this.dalBes = dalBes;
    this.bizzFactory = bizzFactory;
  }

  @Override
  public List<MobiliteEtudianteDto> visualiserToutesLesMobilites() {
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.mobilite_etudiants AS MOB LEFT OUTER JOIN "
            + "projetpae.demandes_mobilite AS DEM ON MOB.id_demande = "
            + "DEM.id_demande LEFT OUTER JOIN projetpae.personnes AS "
            + "PER ON MOB.id_personne = PER.id_personne LEFT OUTER JOIN"
            + " projetpae.partenaires AS PAR ON MOB.id_partenaire = "
            + "PAR.id_partenaire LEFT OUTER JOIN projetpae.pays AS PAY ON "
            + "DEM.pays = PAY.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse");
    List<MobiliteEtudianteDto> listeDemandes = new ArrayList<MobiliteEtudianteDto>();
    try {
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        MobiliteEtudianteDto dto = creerMobiliteParResultSet(rs);
        listeDemandes.add(dto);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return listeDemandes;
  }

  @Override
  public MobiliteEtudianteDto creerMobiliteEtudiante(MobiliteEtudianteDto mobiliteDto) {
    PreparedStatement ps = dalBes
        .prepareStatement("INSERT INTO projetpae.mobilite_etudiants (id_demande, id_personne, "
            + "id_partenaire, etat, quadrimestre_mobilite, "
            + "envoie_demande_paiement1, envoie_demande_paiement2, contrat_bourse,"
            + " convention_stage_etude, charte_etudiant, document_engagement, encode_pro_eco"
            + ", encode_mobility_tool, encode_mobi,  "
            + "attestation_sejour_recu, releve_note_recu, certification_stage_recu, rapport_final, "
            + "num_version_mobilite_etudiants) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
    try {
      ps.setInt(1, mobiliteDto.getDemande().getIdDemande());
      ps.setInt(2, mobiliteDto.getPersonne().getIdPersonne());
      ps.setInt(3, mobiliteDto.getPartenaire().getIdPartenaire());
      ps.setString(4, mobiliteDto.getEtat());
      ps.setInt(5, mobiliteDto.getQuadrimestre());
      ps.setBoolean(6, mobiliteDto.isEnvoieDemandePaiement1());
      ps.setBoolean(7, mobiliteDto.isEnvoieDemandePaiement2());
      ps.setBoolean(8, mobiliteDto.isContratBourse());
      ps.setBoolean(9, mobiliteDto.isConventionStageEtude());
      ps.setBoolean(10, mobiliteDto.isCharteEtudiant());
      ps.setBoolean(11, mobiliteDto.isDocumentEngagement());
      ps.setBoolean(12, mobiliteDto.isEncodeProEco());
      ps.setBoolean(13, mobiliteDto.isEncodeMobilityTool());
      ps.setBoolean(14, mobiliteDto.isEncodeMobi());
      ps.setBoolean(15, mobiliteDto.isAttestationSejourRecu());
      ps.setBoolean(16, mobiliteDto.isReleveNoteRecu());
      ps.setBoolean(17, mobiliteDto.getCertificatStageRecu());
      ps.setBoolean(18, mobiliteDto.isRapportFinal());
      ps.setInt(19, 1);
      if (ps.executeUpdate() == 0) {
        return null;
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    mobiliteDto.setNumVersion(1);
    return mobiliteDto;
  }

  @Override
  public List<MobiliteEtudianteDto> getMobilitesParEtudiant(PersonneDto personneDto) {
    List<MobiliteEtudianteDto> list = new ArrayList<MobiliteEtudianteDto>();

    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.mobilite_etudiants AS MOB LEFT OUTER JOIN "
            + "projetpae.demandes_mobilite AS DEM ON MOB.id_demande = "
            + "DEM.id_demande LEFT OUTER JOIN projetpae.personnes AS "
            + "PER ON MOB.id_personne = PER.id_personne LEFT OUTER JOIN"
            + " projetpae.partenaires AS PAR ON MOB.id_partenaire = "
            + "PAR.id_partenaire LEFT OUTER JOIN projetpae.pays AS PAY ON "
            + "DEM.pays = PAY.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse WHERE MOB.id_personne = ? ");
    try {
      ps.setInt(1, personneDto.getIdPersonne());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        MobiliteEtudianteDto dto = creerMobiliteParResultSet(rs);
        list.add(dto);
      }
      return list;
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  private MobiliteEtudianteDto creerMobiliteParResultSet(ResultSet rs) throws SQLException {
    MobiliteEtudianteDto dto = bizzFactory.getMobiliteEtudiante();
    dto.setIdMobiliteEtudiant(rs.getInt("id_mobilite_etudiants"));
    PaysDto paysDto = bizzFactory.getPays();
    paysDto.setCodeIso(rs.getString("pays"));
    paysDto.setNom(rs.getString("nom_pays"));
    paysDto.setProgramme(rs.getString("programme_pays"));
    DemandeMobiliteDto demandeDto = bizzFactory.getDemandeMobilite();
    demandeDto.setPays(paysDto);
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setIdPersonne(rs.getInt("id_personne"));
    persDto.setPrenom(rs.getString("prenom"));
    persDto.setNom(rs.getString("nom_personne"));
    persDto.setPseudo(rs.getString("pseudo"));
    dto.setPersonne(persDto);
    AdresseDto adrDto = bizzFactory.getAdresse();
    adrDto.setIdAdresse(rs.getInt("id_adresse"));
    adrDto.setVille(rs.getString("ville"));
    adrDto.setRue(rs.getString("rue"));
    adrDto.setNumero(rs.getInt("numero"));
    adrDto.setCodePostal(rs.getString("code_postal"));
    PartenaireDto partDto = bizzFactory.getPartenaire();
    partDto.setAdresse(adrDto);
    partDto.setIdPartenaire(rs.getInt("id_partenaire"));
    partDto.setNomComplet(rs.getString("nom_complet"));
    partDto.setNomLegal(rs.getString("nom_affaires"));
    partDto.setCode(rs.getString("code_partenaire"));
    demandeDto.setIdDemande(rs.getInt("id_demande"));
    demandeDto.setPersonne(persDto);
    demandeDto.setConfirme(rs.getBoolean("confirme"));
    demandeDto.setDateIntroduction(rs.getDate("date_introduction").toLocalDate());
    demandeDto.setProgramme(rs.getString("programme_demande"));
    demandeDto.setCode(rs.getString("code_demande"));
    demandeDto.setQuadrimestre(rs.getInt("quadrimestre_demande"));
    demandeDto.setOrdrePreference(rs.getInt("ordre_preference"));
    demandeDto.setAnneeAcademique(rs.getInt("annee_academique"));
    demandeDto.setNumOrdreCandidature(rs.getInt("num_ordre_candidature"));
    demandeDto.setNumVersion(rs.getInt("num_version_demande_mobilite"));
    dto.setDemande(demandeDto);
    dto.setPartenaire(partDto);
    dto.setEtat(rs.getString("etat"));
    dto.setQuadrimestre(rs.getInt("quadrimestre_mobilite"));
    dto.setEnvoieDemandePaiement1(rs.getBoolean("envoie_demande_paiement1"));
    dto.setEnvoieDemandePaiement2(rs.getBoolean("envoie_demande_paiement2"));
    dto.setContratBourse(rs.getBoolean("contrat_bourse"));
    dto.setConventionStageEtude(rs.getBoolean("convention_stage_etude"));
    dto.setCharteEtudiant(rs.getBoolean("charte_etudiant"));
    dto.setPreuveTestLinguistique(rs.getBoolean("preuve_test_linguistique"));
    dto.setDocumentEngagement(rs.getBoolean("document_engagement"));
    dto.setEncodeProEco(rs.getBoolean("encode_pro_eco"));
    dto.setEncodeMobilityTool(rs.getBoolean("encode_mobility_tool"));
    dto.setEncodeMobi(rs.getBoolean("encode_mobi"));
    dto.setAttestationSejourRecu(rs.getBoolean("attestation_sejour_recu"));
    dto.setReleveNoteRecu(rs.getBoolean("releve_note_recu"));
    dto.setCertificatStageRecu(rs.getBoolean("certification_stage_recu"));
    dto.setPreuveTestLinguistiqueRetour(rs.getBoolean("preuve_test_linguistique_retour"));
    dto.setRapportFinal(rs.getBoolean("rapport_final"));
    dto.setDernierEtat(rs.getString("dernier_etat"));
    dto.setRaisonsAnnulationRefus(rs.getString("raisons_annulation_refus"));
    dto.setNumVersion(rs.getInt("num_version_mobilite_etudiants"));
    return dto;
  }

  @Override
  public MobiliteEtudianteDto annulerMobilite(MobiliteEtudianteDto mobDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("UPDATE projetpae.mobilite_etudiants SET etat = ?, dernier_etat = ?"
            + ", raisons_annulation_refus = ?, num_version_mobilite_etudiants = ? "
            + "WHERE id_mobilite_etudiants = ? " + "AND num_version_mobilite_etudiants = ?");
    try {
      ps.setString(1, mobDto.getEtat());
      ps.setString(2, mobDto.getDernierEtat());
      ps.setString(3, mobDto.getRaisonsAnnulationRefus());
      ps.setInt(4, mobDto.getNumVersion() + 1);
      ps.setInt(5, mobDto.getIdMobiliteEtudiant());
      ps.setInt(6, mobDto.getNumVersion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      mobDto.setNumVersion(mobDto.getNumVersion() + 1);
      return mobDto;
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public MobiliteEtudianteDto indiquerEnvoieDemandePaiement(MobiliteEtudianteDto mobDto) {
    PreparedStatement ps = dalBes
        .prepareStatement("UPDATE projetpae.mobilite_etudiants SET envoie_demande_paiement1 = ?,"
            + " num_version_mobilite_etudiants = ? WHERE id_mobilite_etudiants"
            + " = ? AND num_version_mobilite_etudiants = ?");
    try {
      ps.setBoolean(1, mobDto.isEnvoieDemandePaiement1());
      ps.setInt(2, mobDto.getNumVersion() + 1);
      ps.setInt(3, mobDto.getIdMobiliteEtudiant());
      ps.setInt(4, mobDto.getNumVersion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      mobDto.setNumVersion(mobDto.getNumVersion() + 1);
      return mobDto;
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public MobiliteEtudianteDto getPartenaireParEtudiant(MobiliteEtudianteDto mobiDto) {
    PartenaireDto partDto = bizzFactory.getPartenaire();
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.mobilite_etudiants WHERE id_personne = ?");
    try {
      ps.setInt(1, mobiDto.getPersonne().getIdPersonne());
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        partDto.setIdPartenaire(rs.getInt("id_partenaire"));
        mobiDto.setPartenaire(partDto);
        return mobiDto;
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return null;
  }

  @Override
  public MobiliteEtudianteDto indiquerEnvoieDeuxiemeDemandePaiement(MobiliteEtudianteDto mobDto) {
    PreparedStatement ps = dalBes
        .prepareStatement("UPDATE projetpae.mobilite_etudiants SET envoie_demande_paiement2 = ?, "
            + " num_version_mobilite_etudiants = ?, etat = ? "
            + "WHERE id_mobilite_etudiants = ? AND num_version_mobilite_etudiants = ?");
    try {
      ps.setBoolean(1, mobDto.isEnvoieDemandePaiement2());
      ps.setInt(2, mobDto.getNumVersion() + 1);
      ps.setString(3, mobDto.getEtat());
      ps.setInt(4, mobDto.getIdMobiliteEtudiant());
      ps.setInt(5, mobDto.getNumVersion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      mobDto.setNumVersion(mobDto.getNumVersion() + 1);
      return mobDto;
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public MobiliteEtudianteDto indiquerDonneesMobiEncodeesExternes(MobiliteEtudianteDto mobiDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("UPDATE projetpae.mobilite_etudiants SET encode_pro_eco = ?,"
            + " encode_mobility_tool = ?, encode_mobi = ?, num_version_mobilite_etudiants = ?"
            + "WHERE id_mobilite_etudiants = ? AND num_version_mobilite_etudiants = ?");
    try {
      ps.setBoolean(1, mobiDto.isEncodeProEco());
      ps.setBoolean(2, mobiDto.isEncodeMobilityTool());
      ps.setBoolean(3, mobiDto.isEncodeMobi());
      ps.setInt(4, mobiDto.getNumVersion() + 1);
      ps.setInt(5, mobiDto.getIdMobiliteEtudiant());
      ps.setInt(6, mobiDto.getNumVersion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      mobiDto.setNumVersion(mobiDto.getNumVersion() + 1);
      return mobiDto;
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public MobiliteEtudianteDto confirmerDocumentsEtudiantRemplis(MobiliteEtudianteDto mobiDto) {
    PreparedStatement ps = dalBes.prepareStatement(
        "UPDATE projetpae.mobilite_etudiants SET num_version_mobilite_etudiants = ?, "
            + "contrat_bourse = ?, etat = ?, convention_stage_etude = ?, "
            + "charte_etudiant = ?, preuve_test_linguistique = ?, document_engagement = ?  "
            + " WHERE num_version_mobilite_etudiants = ? AND id_mobilite_etudiants = ? ");
    try {
      ps.setInt(1, mobiDto.getNumVersion() + 1);
      ps.setBoolean(2, mobiDto.isContratBourse());
      ps.setString(3, mobiDto.getEtat());
      ps.setBoolean(4, mobiDto.isConventionStageEtude());
      ps.setBoolean(5, mobiDto.isCharteEtudiant());
      ps.setBoolean(6, mobiDto.isPreuveTestLinguistique());
      ps.setBoolean(7, mobiDto.isDocumentEngagement());
      ps.setInt(8, mobiDto.getNumVersion());
      ps.setInt(9, mobiDto.getIdMobiliteEtudiant());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      mobiDto.setNumVersion(mobiDto.getNumVersion() + 1);
      return mobiDto;
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public MobiliteEtudianteDto preciserDocumentsEtudiantRetour(MobiliteEtudianteDto mobiDto) {
    PreparedStatement ps = dalBes
        .prepareStatement("UPDATE projetpae.mobilite_etudiants SET attestation_sejour_recu = ?, "
            + "releve_note_recu = ?,  certification_stage_recu = ?,"
            + " preuve_test_linguistique_retour = ?, rapport_final = ?, "
            + "num_version_mobilite_etudiants = ? "
            + "WHERE id_mobilite_etudiants = ? AND num_version_mobilite_etudiants = ?");
    try {
      ps.setBoolean(1, mobiDto.isAttestationSejourRecu());
      ps.setBoolean(2, mobiDto.isReleveNoteRecu());
      ps.setBoolean(3, mobiDto.getCertificatStageRecu());
      ps.setBoolean(4, mobiDto.isPreuveTestLinguistiqueRetour());
      ps.setBoolean(5, mobiDto.isRapportFinal());
      ps.setInt(6, mobiDto.getNumVersion() + 1);
      ps.setInt(7, mobiDto.getIdMobiliteEtudiant());
      ps.setInt(8, mobiDto.getNumVersion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      mobiDto.setNumVersion(mobiDto.getNumVersion() + 1);
      return mobiDto;
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public MobiliteEtudianteDto preciserDocumentsEtudiantRetourAPayerSolde(
      MobiliteEtudianteDto mobiDto) {
    PreparedStatement ps = dalBes
        .prepareStatement("UPDATE projetpae.mobilite_etudiants SET attestation_sejour_recu = ?, "
            + "releve_note_recu = ?,  certification_stage_recu = ?, "
            + "preuve_test_linguistique_retour = ?, "
            + "rapport_final = ?, etat = ?, num_version_mobilite_etudiants = ? "
            + "WHERE id_mobilite_etudiants = ? AND num_version_mobilite_etudiants = ?");
    try {
      ps.setBoolean(1, mobiDto.isAttestationSejourRecu());
      ps.setBoolean(2, mobiDto.isReleveNoteRecu());
      ps.setBoolean(3, mobiDto.getCertificatStageRecu());
      ps.setBoolean(4, mobiDto.isPreuveTestLinguistiqueRetour());
      ps.setBoolean(5, mobiDto.isRapportFinal());
      ps.setString(6, mobiDto.getEtat());
      ps.setInt(7, mobiDto.getNumVersion() + 1);
      ps.setInt(8, mobiDto.getIdMobiliteEtudiant());
      ps.setInt(9, mobiDto.getNumVersion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      mobiDto.setNumVersion(mobiDto.getNumVersion() + 1);
      return mobiDto;
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public MobiliteEtudianteDto changementEtat(MobiliteEtudianteDto mobiDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("UPDATE projetpae.mobilite_etudiants SET etat = ?, "
            + " num_version_mobilite_etudiants = ? "
            + "WHERE id_mobilite_etudiants = ? AND num_version_mobilite_etudiants = ?");
    try {
      ps.setString(1, mobiDto.getEtat());
      ps.setInt(2, mobiDto.getNumVersion() + 1);
      ps.setInt(3, mobiDto.getIdMobiliteEtudiant());
      ps.setInt(4, mobiDto.getNumVersion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      mobiDto.setNumVersion(mobiDto.getNumVersion() + 1);
      return mobiDto;
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public List<MobiliteEtudianteDto> getMobilitesNonAnnulees() {
    PreparedStatement ps = dalBes.prepareStatement("SELECT * FROM projetpae.mobilite_etudiants "
        + "AS MOB LEFT OUTER JOIN " + "projetpae.demandes_mobilite AS DEM ON MOB.id_demande = "
        + "DEM.id_demande LEFT OUTER JOIN projetpae.personnes AS "
        + "PER ON MOB.id_personne = PER.id_personne LEFT OUTER JOIN"
        + " projetpae.partenaires AS PAR ON MOB.id_partenaire = "
        + "PAR.id_partenaire LEFT OUTER JOIN projetpae.pays AS PAY ON "
        + "DEM.pays = PAY.code_iso LEFT OUTER JOIN projetpae.adresses "
        + "AS ADR ON PAR.adresse = ADR.id_adresse WHERE MOB.etat != 'annule' "
        + "AND MOB.etat != 'refuse'");
    List<MobiliteEtudianteDto> listeDemandes = new ArrayList<MobiliteEtudianteDto>();
    try {
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        MobiliteEtudianteDto dto = creerMobiliteParResultSet(rs);
        listeDemandes.add(dto);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return listeDemandes;
  }

  @Override
  public List<MobiliteEtudianteDto> getListePaiements(MobiliteEtudianteDto mobiDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.mobilite_etudiants AS MOB LEFT OUTER JOIN "
            + "projetpae.demandes_mobilite AS DEM ON MOB.id_demande = "
            + "DEM.id_demande LEFT OUTER JOIN projetpae.personnes AS "
            + "PER ON MOB.id_personne = PER.id_personne LEFT OUTER JOIN"
            + " projetpae.partenaires AS PAR ON MOB.id_partenaire = "
            + "PAR.id_partenaire LEFT OUTER JOIN projetpae.pays AS PAY ON "
            + "DEM.pays = PAY.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse "
            + "WHERE DEM.annee_academique = ? AND envoie_demande_paiement1 IS TRUE");
    List<MobiliteEtudianteDto> listePaiements = new ArrayList<MobiliteEtudianteDto>();
    try {
      ps.setInt(1, mobiDto.getDemande().getAnneeAcademique());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        MobiliteEtudianteDto dto = creerMobiliteParResultSet(rs);
        listePaiements.add(dto);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return listePaiements;
  }

  @Override
  public List<MobiliteEtudianteDto> getMobiParAnneeAcademique(MobiliteEtudianteDto mobiDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.mobilite_etudiants AS MOB LEFT OUTER JOIN "
            + "projetpae.demandes_mobilite AS DEM ON MOB.id_demande = "
            + "DEM.id_demande LEFT OUTER JOIN projetpae.personnes AS "
            + "PER ON MOB.id_personne = PER.id_personne LEFT OUTER JOIN"
            + " projetpae.partenaires AS PAR ON MOB.id_partenaire = "
            + "PAR.id_partenaire LEFT OUTER JOIN projetpae.pays AS PAY ON "
            + "DEM.pays = PAY.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse WHERE DEM.annee_academique = ?");
    List<MobiliteEtudianteDto> listeMobis = new ArrayList<MobiliteEtudianteDto>();
    try {
      ps.setInt(1, mobiDto.getDemande().getAnneeAcademique());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        MobiliteEtudianteDto dto = creerMobiliteParResultSet(rs);
        listeMobis.add(dto);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return listeMobis;
  }

  @Override
  public List<MobiliteEtudianteDto> getMobiParEtat(MobiliteEtudianteDto mobiDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.mobilite_etudiants AS MOB LEFT OUTER JOIN "
            + "projetpae.demandes_mobilite AS DEM ON MOB.id_demande = "
            + "DEM.id_demande LEFT OUTER JOIN projetpae.personnes AS "
            + "PER ON MOB.id_personne = PER.id_personne LEFT OUTER JOIN"
            + " projetpae.partenaires AS PAR ON MOB.id_partenaire = "
            + "PAR.id_partenaire LEFT OUTER JOIN projetpae.pays AS PAY ON "
            + "DEM.pays = PAY.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse WHERE MOB.etat = ?");
    List<MobiliteEtudianteDto> listeMobis = new ArrayList<MobiliteEtudianteDto>();
    try {
      ps.setString(1, mobiDto.getEtat());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        MobiliteEtudianteDto dto = creerMobiliteParResultSet(rs);
        listeMobis.add(dto);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return listeMobis;
  }

  @Override
  public MobiliteEtudianteDto getMobiParId(MobiliteEtudianteDto mobiDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.mobilite_etudiants AS MOB LEFT OUTER JOIN "
            + "projetpae.demandes_mobilite AS DEM ON MOB.id_demande = "
            + "DEM.id_demande LEFT OUTER JOIN projetpae.personnes AS "
            + "PER ON MOB.id_personne = PER.id_personne LEFT OUTER JOIN"
            + " projetpae.partenaires AS PAR ON MOB.id_partenaire = "
            + "PAR.id_partenaire LEFT OUTER JOIN projetpae.pays AS PAY ON "
            + "DEM.pays = PAY.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse WHERE MOB.id_mobilite_etudiants = ?");
    try {
      ps.setInt(1, mobiDto.getIdMobiliteEtudiant());
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        MobiliteEtudianteDto mob = creerMobiliteParResultSet(rs);
        System.out.println(mob.getEtat());
        System.out.println(mob.getDemande());
        System.out.println(mob.getPersonne());
        return mob;
      } else {
        return null;
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public List<MobiliteEtudianteDto> getMobiParEtatEtAnneeAcademique(MobiliteEtudianteDto mobiDto) {
    PreparedStatement ps =
        dalBes.prepareStatement("SELECT * FROM projetpae.mobilite_etudiants AS MOB LEFT OUTER JOIN "
            + "projetpae.demandes_mobilite AS DEM ON MOB.id_demande = "
            + "DEM.id_demande LEFT OUTER JOIN projetpae.personnes AS "
            + "PER ON MOB.id_personne = PER.id_personne LEFT OUTER JOIN"
            + " projetpae.partenaires AS PAR ON MOB.id_partenaire = "
            + "PAR.id_partenaire LEFT OUTER JOIN projetpae.pays AS PAY ON "
            + "DEM.pays = PAY.code_iso LEFT OUTER JOIN projetpae.adresses "
            + "AS ADR ON PAR.adresse = ADR.id_adresse"
            + " WHERE MOB.etat = ? AND DEM.annee_academique = ?");
    List<MobiliteEtudianteDto> listeMobis = new ArrayList<MobiliteEtudianteDto>();
    try {
      ps.setString(1, mobiDto.getEtat());
      ps.setInt(2, mobiDto.getDemande().getAnneeAcademique());
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        MobiliteEtudianteDto dto = creerMobiliteParResultSet(rs);
        listeMobis.add(dto);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return listeMobis;
  }
}
