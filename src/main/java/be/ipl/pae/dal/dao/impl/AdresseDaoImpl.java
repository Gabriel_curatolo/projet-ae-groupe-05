package be.ipl.pae.dal.dao.impl;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.dal.dao.interfaces.AdresseDao;
import be.ipl.pae.dal.services.DalBackEndServices;
import be.ipl.pae.exceptions.DalException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AdresseDaoImpl implements AdresseDao {

  private DalBackEndServices dalBes;

  /**
   * constructeur de AdresseDaoImpl.
   * 
   * @param dalBes le dalbackendServices
   */
  public AdresseDaoImpl(DalBackEndServices dalBes) {
    this.dalBes = dalBes;
  }

  @Override
  public AdresseDto encodageDonnees(AdresseDto adrDtoEncodageDonnees) {
    PreparedStatement ps = dalBes.prepareStatement(
        "INSERT INTO projetpae.adresses (id_adresse, rue, numero,"
            + " code_postal, ville, commune, boite, region) VALUES(DEFAULT,?,?,?,?,?,?,?);",
        Statement.RETURN_GENERATED_KEYS);
    try {
      ps.setString(1, adrDtoEncodageDonnees.getRue());
      ps.setInt(2, adrDtoEncodageDonnees.getNumero());
      ps.setString(3, adrDtoEncodageDonnees.getCodePostal());
      ps.setString(4, adrDtoEncodageDonnees.getVille());
      ps.setString(5, adrDtoEncodageDonnees.getCommune());
      ps.setString(6, adrDtoEncodageDonnees.getBoite());
      ps.setString(7, adrDtoEncodageDonnees.getRegion());
      if (ps.executeUpdate() == 0) {
        return null;
      }
      ResultSet rs = ps.getGeneratedKeys();
      if (rs.next()) {
        adrDtoEncodageDonnees.setIdAdresse(rs.getInt(1));
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
    return adrDtoEncodageDonnees;
  }
}
