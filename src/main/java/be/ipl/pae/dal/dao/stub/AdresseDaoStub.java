package be.ipl.pae.dal.dao.stub;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.dal.dao.interfaces.AdresseDao;

import java.util.ArrayList;
import java.util.List;

public class AdresseDaoStub implements AdresseDao {

  private List<AdresseDto> adresses;

  /**
   * constructeur de AdresseDaoStub.
   */
  public AdresseDaoStub() {
    super();
    this.adresses = new ArrayList<AdresseDto>();
  }

  @Override
  public AdresseDto encodageDonnees(AdresseDto adrDto) {
    adresses.add(adrDto);
    adrDto.setIdAdresse(adresses.size());
    return adrDto;
  }


}
