package be.ipl.pae.dal.dao.stub;

import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.dal.dao.interfaces.MobiliteEtudianteDao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MobiliteEtudianteDaoStub implements MobiliteEtudianteDao {

  private BizzFactory bizFact;
  private Set<MobiliteEtudianteDto> mobilitesEtudiantes;

  /**
   * constructeur de MobiliteEtudianteDaoStub.
   * 
   * @param bizFact le bizzFactory
   */
  public MobiliteEtudianteDaoStub(BizzFactory bizFact) {
    super();
    this.bizFact = bizFact;
    mobilitesEtudiantes = new HashSet<MobiliteEtudianteDto>();
  }

  @Override
  public List<MobiliteEtudianteDto> visualiserToutesLesMobilites() {
    List<MobiliteEtudianteDto> mobs = new ArrayList<MobiliteEtudianteDto>();
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      mobs.add(m);
    }
    return mobs;
  }

  @Override
  public MobiliteEtudianteDto creerMobiliteEtudiante(MobiliteEtudianteDto mobiliteDto) {
    mobilitesEtudiantes.add(mobiliteDto);
    mobiliteDto.setIdMobiliteEtudiant(mobilitesEtudiantes.size());
    return mobiliteDto;
  }

  @Override
  public List<MobiliteEtudianteDto> getMobilitesParEtudiant(PersonneDto persCo) {
    List<MobiliteEtudianteDto> mobEtud = new ArrayList<MobiliteEtudianteDto>();
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getPersonne().getIdPersonne() == persCo.getIdPersonne()) {
        mobEtud.add(m);
      }
    }
    return mobEtud;
  }

  @Override
  public MobiliteEtudianteDto annulerMobilite(MobiliteEtudianteDto mobDto) {
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getIdMobiliteEtudiant() == mobDto.getIdMobiliteEtudiant()
          && m.getNumVersion() == mobDto.getNumVersion()) {
        m.setEtat("annule");
        m.setRaisonsAnnulationRefus("etudiant indiscipline");
        return mobDto;
      }
    }
    return mobDto;
  }

  @Override
  public MobiliteEtudianteDto indiquerEnvoieDemandePaiement(MobiliteEtudianteDto mobDto) {
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getIdMobiliteEtudiant() == mobDto.getIdMobiliteEtudiant()
          && m.getNumVersion() == mobDto.getNumVersion()) {
        m.setEtat("a_payer");
        return mobDto;
      }
    }
    return null;
  }

  @Override
  public MobiliteEtudianteDto getPartenaireParEtudiant(MobiliteEtudianteDto mobiDto) {
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getPersonne().getIdPersonne() == mobiDto.getPersonne().getIdPersonne()) {
        PartenaireDto partDto = bizFact.getPartenaire();
        partDto.setIdPartenaire(mobiDto.getPartenaire().getIdPartenaire());
        mobiDto.setPartenaire(partDto);
        return mobiDto;
      }
    }
    return null;
  }

  @Override
  public MobiliteEtudianteDto indiquerEnvoieDeuxiemeDemandePaiement(MobiliteEtudianteDto mobDto) {
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getIdMobiliteEtudiant() == mobDto.getIdMobiliteEtudiant()
          && m.getNumVersion() == mobDto.getNumVersion()) {
        mobDto.setEnvoieDemandePaiement2(true);
        return mobDto;
      }
    }
    return null;
  }

  public void reset() {
    mobilitesEtudiantes.clear();
  }

  @Override
  public MobiliteEtudianteDto indiquerDonneesMobiEncodeesExternes(MobiliteEtudianteDto mobiDto) {
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getIdMobiliteEtudiant() == mobiDto.getIdMobiliteEtudiant()
          && m.getNumVersion() == mobiDto.getNumVersion()) {
        mobiDto.setEncodeProEco(true);
      }
    }
    return mobiDto;
  }

  @Override
  public MobiliteEtudianteDto confirmerDocumentsEtudiantRemplis(MobiliteEtudianteDto mobiDto) {
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getIdMobiliteEtudiant() == mobiDto.getIdMobiliteEtudiant()
          && m.getNumVersion() == mobiDto.getNumVersion()) {
        mobiDto.setContratBourse(true);
        mobiDto.setConventionStageEtude(true);
        mobiDto.setCharteEtudiant(true);
        mobiDto.setPreuveTestLinguistique(true);
        mobiDto.setDocumentEngagement(false);
        mobiDto.setEtat("a_payer");
      }
    }
    return mobiDto;
  }

  @Override
  public MobiliteEtudianteDto preciserDocumentsEtudiantRetour(MobiliteEtudianteDto mobiDto) {
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getIdMobiliteEtudiant() == mobiDto.getIdMobiliteEtudiant()
          && m.getNumVersion() == mobiDto.getNumVersion()) {
        mobiDto.setAttestationSejourRecu(true);
        mobiDto.setReleveNoteRecu(true);
        mobiDto.setCertificatStageRecu(true);
        mobiDto.setPreuveTestLinguistique(true);
        mobiDto.setRapportFinal(false);
      }
    }
    return mobiDto;
  }

  @Override
  public MobiliteEtudianteDto changementEtat(MobiliteEtudianteDto mobiDto) {
    boolean modifie = false;
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getIdMobiliteEtudiant() == mobiDto.getIdMobiliteEtudiant()
          && m.getNumVersion() == mobiDto.getNumVersion()) {
        modifie = true;
        m.setContratBourse(mobiDto.isContratBourse());
        m.setConventionStageEtude(mobiDto.isConventionStageEtude());
        m.setCharteEtudiant(mobiDto.isCharteEtudiant());
        m.setPreuveTestLinguistique(mobiDto.isPreuveTestLinguistique());
        m.setDocumentEngagement(mobiDto.isDocumentEngagement());
        m.setEtat(mobiDto.getEtat());
        m.setNumVersion(mobiDto.getNumVersion() + 1);
        mobiDto.setNumVersion(m.getNumVersion());
      }
    }
    if (modifie) {

      return mobiDto;
    }
    return null;
  }

  @Override
  public List<MobiliteEtudianteDto> getMobilitesNonAnnulees() {
    List<MobiliteEtudianteDto> mobEtud = new ArrayList<MobiliteEtudianteDto>();
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (!m.getEtat().equals("refuse") && !m.getEtat().equals("annule")) {
        mobEtud.add(m);
      }
    }
    return mobEtud;
  }

  @Override
  public List<MobiliteEtudianteDto> getListePaiements(MobiliteEtudianteDto mobiDto) {
    List<MobiliteEtudianteDto> mobEtud = new ArrayList<MobiliteEtudianteDto>();
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getDemande().getAnneeAcademique() == mobiDto.getDemande().getAnneeAcademique()
          && m.isEnvoieDemandePaiement1()) {
        mobEtud.add(mobiDto);
      }
    }
    return mobEtud;
  }

  @Override
  public List<MobiliteEtudianteDto> getMobiParAnneeAcademique(MobiliteEtudianteDto mobiDto) {
    List<MobiliteEtudianteDto> mobEtud = new ArrayList<MobiliteEtudianteDto>();
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getDemande().getAnneeAcademique() == mobiDto.getDemande().getAnneeAcademique()) {
        mobEtud.add(mobiDto);
      }
    }
    return mobEtud;
  }

  @Override
  public List<MobiliteEtudianteDto> getMobiParEtat(MobiliteEtudianteDto mobiDto) {
    List<MobiliteEtudianteDto> mobEtud = new ArrayList<MobiliteEtudianteDto>();
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getEtat() == mobiDto.getEtat()) {
        mobEtud.add(mobiDto);
      }
    }
    return mobEtud;
  }

  @Override
  public MobiliteEtudianteDto preciserDocumentsEtudiantRetourAPayerSolde(
      MobiliteEtudianteDto mobiDto) {
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getIdMobiliteEtudiant() == mobiDto.getIdMobiliteEtudiant()
          && m.getNumVersion() == mobiDto.getNumVersion()) {
        mobiDto.setAttestationSejourRecu(true);
        mobiDto.setReleveNoteRecu(true);
        mobiDto.setCertificatStageRecu(false);
        mobiDto.setPreuveTestLinguistiqueRetour(true);
        mobiDto.setRapportFinal(true);
        mobiDto.setEtat("a_payer_solde");
      }
    }
    return mobiDto;
  }


  @Override
  public MobiliteEtudianteDto getMobiParId(MobiliteEtudianteDto mobiDto) {
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getIdMobiliteEtudiant() == mobiDto.getIdMobiliteEtudiant()) {
        return m;
      }
    }
    return null;
  }

  @Override
  public List<MobiliteEtudianteDto> getMobiParEtatEtAnneeAcademique(MobiliteEtudianteDto mobiDto) {
    List<MobiliteEtudianteDto> mobEtud = new ArrayList<MobiliteEtudianteDto>();
    for (MobiliteEtudianteDto m : mobilitesEtudiantes) {
      if (m.getEtat() == mobiDto.getEtat()
          && m.getDemande().getAnneeAcademique() == mobiDto.getDemande().getAnneeAcademique()) {
        mobEtud.add(mobiDto);
      }
    }
    return mobEtud;
  }


}
