package be.ipl.pae.dal.dao.interfaces;

import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PersonneDto;

import java.util.List;

public interface MobiliteEtudianteDao {

  List<MobiliteEtudianteDto> visualiserToutesLesMobilites();

  MobiliteEtudianteDto creerMobiliteEtudiante(MobiliteEtudianteDto mobiliteDto);

  List<MobiliteEtudianteDto> getMobilitesParEtudiant(PersonneDto persCo);

  MobiliteEtudianteDto annulerMobilite(MobiliteEtudianteDto mobDto);

  MobiliteEtudianteDto indiquerEnvoieDemandePaiement(MobiliteEtudianteDto mobDto);

  MobiliteEtudianteDto getPartenaireParEtudiant(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto indiquerEnvoieDeuxiemeDemandePaiement(MobiliteEtudianteDto mobDto);

  MobiliteEtudianteDto indiquerDonneesMobiEncodeesExternes(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto confirmerDocumentsEtudiantRemplis(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto preciserDocumentsEtudiantRetour(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto preciserDocumentsEtudiantRetourAPayerSolde(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto changementEtat(MobiliteEtudianteDto mobiDto);

  List<MobiliteEtudianteDto> getMobilitesNonAnnulees();

  List<MobiliteEtudianteDto> getListePaiements(MobiliteEtudianteDto mobiDto);

  List<MobiliteEtudianteDto> getMobiParAnneeAcademique(MobiliteEtudianteDto mobiDto);

  List<MobiliteEtudianteDto> getMobiParEtat(MobiliteEtudianteDto mobiDto);

  MobiliteEtudianteDto getMobiParId(MobiliteEtudianteDto mobiDto);

  List<MobiliteEtudianteDto> getMobiParEtatEtAnneeAcademique(MobiliteEtudianteDto mobiDto);


}
