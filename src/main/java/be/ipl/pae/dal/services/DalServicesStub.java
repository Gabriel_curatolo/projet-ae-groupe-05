package be.ipl.pae.dal.services;

public class DalServicesStub implements DalServices {

  @Override
  public void commit() {

  }

  @Override
  public void rollBack() {

  }

  @Override
  public void start() {

  }

  @Override
  public void close() {}

  @Override
  public void startSansTransation() {}

}
