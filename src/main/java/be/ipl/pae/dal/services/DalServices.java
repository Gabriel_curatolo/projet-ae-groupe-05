package be.ipl.pae.dal.services;

public interface DalServices {

  void commit();

  void rollBack();

  void start();

  void close();

  void startSansTransation();

}
