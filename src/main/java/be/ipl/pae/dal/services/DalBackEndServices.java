package be.ipl.pae.dal.services;

import java.sql.PreparedStatement;

public interface DalBackEndServices {

  PreparedStatement prepareStatement(String query);

  PreparedStatement prepareStatement(String string, int returnGeneratedKeys);

}
