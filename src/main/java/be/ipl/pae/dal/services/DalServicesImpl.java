package be.ipl.pae.dal.services;

import be.ipl.pae.exceptions.DalException;
import be.ipl.pae.main.Config;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DalServicesImpl implements DalServices, DalBackEndServices {

  private ThreadLocal<Connection> connections = new ThreadLocal<>();
  private BasicDataSource dataSource = new BasicDataSource();
  private String url;

  /**
   * constructeur de DalServicesImpl.
   * 
   * @param config la config
   * @throws SQLException si erreur
   */
  public DalServicesImpl(Config config) throws SQLException {
    super();
    url = config.getProperties("url");

    // test driver
    try {
      Class.forName("org.postgresql.Driver");
    } catch (ClassNotFoundException exception) {
      System.out.println("Driver PostgreSQL manquant !");
      System.exit(1);
    }

    dataSource.setUsername(config.getProperties("nom"));
    dataSource.setPassword(config.getProperties("mdp"));
    dataSource.setUrl(url);
  }

  @Override
  public PreparedStatement prepareStatement(String query) {
    try {
      Connection conn = connections.get();
      return conn.prepareStatement(query);
    } catch (Exception exception) {
      close();
      throw new DalException(exception);
    }
  }

  @Override
  public PreparedStatement prepareStatement(String query, int id) {
    try {
      Connection conn = connections.get();
      return conn.prepareStatement(query, id);
    } catch (Exception exception) {
      close();
      throw new DalException(exception);
    }
  }

  @Override
  public void commit() {
    try {
      Connection conn = connections.get();
      conn.commit();
      conn.close();
      connections.remove();
    } catch (SQLException exception) {
      rollBack();
      throw new DalException(exception);
    }
  }

  @Override
  public void rollBack() {
    try {
      Connection conn = connections.get();
      conn.rollback();
      conn.close();
      connections.remove();
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public void start() {
    try {
      if (connections.get() == null) {
        Connection conn = dataSource.getConnection();
        this.connections.set(conn);
        conn.setAutoCommit(false);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public void startSansTransation() {
    try {
      if (connections.get() == null) {
        Connection conn = dataSource.getConnection();
        this.connections.set(conn);
      }
    } catch (SQLException exception) {
      throw new DalException(exception);
    }
  }

  @Override
  public void close() {
    try {
      Connection conn = connections.get();
      conn.close();
      connections.remove();
    } catch (SQLException exception) {
      throw new DalException();
    }
  }
}

