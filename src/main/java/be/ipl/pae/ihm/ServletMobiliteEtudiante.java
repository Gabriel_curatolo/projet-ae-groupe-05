package be.ipl.pae.ihm;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.ucc.interfaces.MobiliteEtudianteUcc;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletMobiliteEtudiante {

  private BizzFactory bizzFactory;
  private MobiliteEtudianteUcc mobEtUcc;
  private Genson genson;



  /**
   * Constructeur de la ServletMobiliteEtudiante.
   * 
   * @param bizzFactory Une bizz factory
   * 
   * @param mobEtUcc Une mobilite etudiante ucc
   */
  public ServletMobiliteEtudiante(BizzFactory bizzFactory, MobiliteEtudianteUcc mobEtUcc) {
    super();
    this.bizzFactory = bizzFactory;
    this.mobEtUcc = mobEtUcc;
    this.genson = new GensonBuilder().useRuntimeType(true).create();
  }

  /**
   * Permet de visualiser toutes les mobilites de la base de donnees.
   * 
   * @param resp la reponse
   */
  public void visualiserToutesLesMobilites(HttpServletResponse resp) throws IOException {
    List<MobiliteEtudianteDto> list = mobEtUcc.visualiserToutesLesMobilites();
    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Permet de renvoyer la mobilite de la personne connectee.
   * 
   * @param resp la reponse
   * 
   * @param persCo la personne connectee
   */
  public void getMobiliteEtudiantConnecte(HttpServletResponse resp, PersonneDto persCo)
      throws IOException {
    List<MobiliteEtudianteDto> mobDto = mobEtUcc.getMobilitesParEtudiant(persCo);
    if (mobDto == null) {
      resp.setStatus(401); // Pas d'acces à l'affichage mobilité
      return;
    }
    String json = genson.serialize(mobDto);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de renvoyer la mobilite pour un etudiant.
   * 
   * @param resp la reponse
   * 
   * @param req la requete et ses donnees
   */
  public void getMobiliteParEtudiant(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setIdPersonne(Integer.parseInt(req.getParameter("id_personne")));
    List<MobiliteEtudianteDto> mobDto = mobEtUcc.getMobilitesParEtudiant(persDto);
    String json = genson.serialize(mobDto);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Permet de renvoyer toutes les mobilites de la base de donnees qui ne sont pas annulees.
   * 
   * @param resp la reponse
   */
  public void getMobilitesNonAnnulees(HttpServletResponse resp) throws IOException {
    List<MobiliteEtudianteDto> mobDto = mobEtUcc.getMobilitesNonAnnulees();
    String json = genson.serialize(mobDto);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de confirmer une demande de mobilite.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void confirmerDemandeMobilite(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {

    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setQuadrimestre(Integer.parseInt(req.getParameter("quadri")));
    creerMobi(mobiDto);


    DemandeMobiliteDto demDto = bizzFactory.getDemandeMobilite();
    demDto.setConfirme(true);
    demDto.setQuadrimestre(mobiDto.getQuadrimestre());
    demDto.setIdDemande(Integer.parseInt(req.getParameter("id_demande")));
    demDto.setNumVersion(Integer.parseInt(req.getParameter("num_version_demande_mobilite")));
    mobiDto.setDemande(demDto);

    PartenaireDto partDto = bizzFactory.getPartenaire();
    partDto.setIdPartenaire(-1);
    partDto.setNomComplet(req.getParameter("id_partenaire"));
    mobiDto.setPartenaire(partDto);
    mobiDto.getDemande().setPartenaire(partDto);


    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setIdPersonne(Integer.parseInt(req.getParameter("id_personne")));
    mobiDto.setPersonne(persDto);


    MobiliteEtudianteDto ret = mobEtUcc.confirmerDemandeMobilite(mobiDto);
    String json = genson.serialize(ret);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  private void creerMobi(MobiliteEtudianteDto mobiDto) {
    mobiDto.setEtat("Créée");
    mobiDto.setEnvoieDemandePaiement1(false);
    mobiDto.setEnvoieDemandePaiement2(false);
    mobiDto.setContratBourse(false);
    mobiDto.setEncodeProEco(false);
    mobiDto.setEncodeMobilityTool(false);
    mobiDto.setEncodeMobi(false);
    mobiDto.setConventionStageEtude(false);
    mobiDto.setCharteEtudiant(false);
    mobiDto.setDocumentEngagement(false);
    mobiDto.setAttestationSejourRecu(false);
    mobiDto.setReleveNoteRecu(false);
    mobiDto.setCertificatStageRecu(false);
    mobiDto.setRapportFinal(false);
  }

  /**
   * Permet de confirmer une demande de mobilite avec un nouveau partenaire.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void confirmerDemandeMobiliteAvecNouveauPartenaire(HttpServletResponse resp,
      HttpServletRequest req) throws IOException {

    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setQuadrimestre(Integer.parseInt(req.getParameter("quadri")));
    creerMobi(mobiDto);
    DemandeMobiliteDto demDto = bizzFactory.getDemandeMobilite();
    demDto.setConfirme(true);
    demDto.setQuadrimestre(mobiDto.getQuadrimestre());
    demDto.setIdDemande(Integer.parseInt(req.getParameter("id_demande")));
    demDto.setNumVersion(Integer.parseInt(req.getParameter("num_version_demande_mobilite")));
    mobiDto.setDemande(demDto);


    AdresseDto adrDto = bizzFactory.getAdresse();
    adrDto.setRue(req.getParameter("rue"));
    adrDto.setCodePostal(req.getParameter("code_postal"));
    adrDto.setNumero(Integer.parseInt(req.getParameter("numero")));
    adrDto.setBoite(req.getParameter("boite"));
    adrDto.setVille(req.getParameter("ville"));
    adrDto.setCommune(req.getParameter("commune"));
    adrDto.setRegion(req.getParameter("region"));

    PartenaireDto partDto = bizzFactory.getPartenaire();
    PaysDto paysDto = bizzFactory.getPays();
    paysDto.setNom(req.getParameter("pays"));
    partDto.setIdPartenaire(0);
    partDto.setPays(paysDto);
    partDto.setAdresse(adrDto);
    partDto.setNomLegal(req.getParameter("nom_legal"));
    partDto.setNomAffaires(req.getParameter("nom_affaires"));
    partDto.setNomComplet(req.getParameter("nom_complet"));
    partDto.setCode(req.getParameter("code"));
    partDto.setTypeOrganisation(req.getParameter("type_organisation"));
    partDto.setDepartement(req.getParameter("departement"));
    partDto.setNbEmployes(Integer.parseInt(req.getParameter("nb_employes")));
    partDto.setEmail(req.getParameter("email"));
    partDto.setSiteWeb(req.getParameter("site_web"));
    partDto.setTelephone(req.getParameter("telephone"));
    partDto.setEmail(req.getParameter("email"));

    mobiDto.setPartenaire(partDto);


    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setIdPersonne(Integer.parseInt(req.getParameter("id_personne")));
    mobiDto.setPersonne(persDto);


    MobiliteEtudianteDto ret = mobEtUcc.confirmerDemandeMobilite(mobiDto);
    String json = genson.serialize(ret);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Permet d'annuler une mobilite.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void annulerMobilite(HttpServletResponse resp, HttpServletRequest req) throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setEtat(req.getParameter("etat"));
    mobiDto.setDernierEtat(req.getParameter("dernier_etat"));
    mobiDto.setNumVersion(Integer.parseInt(req.getParameter("num_version")));
    mobiDto.setIdMobiliteEtudiant(Integer.parseInt(req.getParameter("id_mobilite_etudiante")));
    mobiDto.setRaisonsAnnulationRefus(req.getParameter("raison_annulation_refus"));
    mobEtUcc.annulerMobilite(mobiDto);
    String json = genson.serialize(mobiDto);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet d indiquer l envoie de la demande de paiemment pour une demande de mobilite.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void indiquerEnvoieDemandePaiement(HttpServletResponse resp, HttpServletRequest req)
      throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setEnvoieDemandePaiement1(true);
    mobiDto.setNumVersion(Integer.parseInt(req.getParameter("num_version")));
    mobiDto.setIdMobiliteEtudiant(Integer.parseInt(req.getParameter("id_mobilite_etudiante")));
    mobEtUcc.indiquerEnvoieDemandePaiement(mobiDto);
    String json = genson.serialize(mobiDto);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de verifier si une mobilite est encodee dans le systeme.
   * 
   * @param req la requete
   * 
   * @param resp la reponse
   * 
   * @param persCo la personne connectee
   */
  public void estEncodeMobilite(HttpServletResponse resp, HttpServletRequest req,
      PersonneDto persCo) throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setPersonne(persCo);
    MobiliteEtudianteDto dtoReturn = mobEtUcc.getPartenaireParEtudiant(mobiDto);

    String json = genson.serialize(dtoReturn);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet d indiquer l envoie de la deuxieme demande de paiemment pour une mobilite.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void indiquerEnvoieDeuxiemeDemandePaiement(HttpServletResponse resp,
      HttpServletRequest req) throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setEnvoieDemandePaiement2(true);
    mobiDto.setEtat("Termine");
    mobiDto.setNumVersion(Integer.parseInt(req.getParameter("num_version")));
    mobiDto.setIdMobiliteEtudiant(Integer.parseInt(req.getParameter("id_mobilite_etudiante")));
    mobiDto.setAttestationSejourRecu(Boolean.valueOf(req.getParameter("attestation_sejour")));
    mobiDto.setReleveNoteRecu(Boolean.valueOf(req.getParameter("releve_notes")));
    mobiDto.setCertificatStageRecu(
        Boolean.valueOf(req.getParameter("certificat_stage")).booleanValue());
    mobiDto.setPreuveTestLinguistiqueRetour(
        Boolean.valueOf(req.getParameter("rapport_final")).booleanValue());
    mobiDto
        .setRapportFinal(Boolean.valueOf(req.getParameter("preuve_passage_langue")).booleanValue());
    System.out.println(mobiDto.getCertificatStageRecu() + "certificat");
    System.out.println(mobiDto.isAttestationSejourRecu() + "attestation");
    System.out.println(mobiDto.isReleveNoteRecu() + "releve");
    System.out.println(mobiDto.isRapportFinal() + "rapport");
    System.out.println(mobiDto.isPreuveTestLinguistiqueRetour() + "preuve");
    mobEtUcc.indiquerEnvoieDeuxiemeDemandePaiement(mobiDto);
    String json = genson.serialize(mobiDto);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Permet de preciser que les donnnes ont ete encodee dans un logiciel externe.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void preciserDonneesEncodeesLogicielExternes(HttpServletRequest req,
      HttpServletResponse resp) throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setEncodeProEco(Boolean.valueOf(req.getParameter("encode_pro_eco")).booleanValue());
    mobiDto.setEncodeMobilityTool(
        Boolean.valueOf(req.getParameter("encode_mobility_tool")).booleanValue());
    mobiDto.setEncodeMobi(Boolean.valueOf(req.getParameter("encode_mobi")).booleanValue());
    mobiDto.setIdMobiliteEtudiant(Integer.parseInt(req.getParameter("id_mobilite")));
    mobiDto.setNumVersion(Integer.parseInt(req.getParameter("num_version")) + 1);
    mobEtUcc.indiquerDonneesMobiEncodeesExternes(mobiDto);
    String json = genson.serialize(mobiDto);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de confirmer que les documents d un etudiant concernant sa mobilite sont remplis.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void confirmerDocumentsEtudiantRemplis(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setContratBourse(Boolean.valueOf(req.getParameter("contrat_bourse")).booleanValue());
    mobiDto.setConventionStageEtude(
        Boolean.valueOf(req.getParameter("convention_stage_etude")).booleanValue());
    mobiDto.setCharteEtudiant(Boolean.valueOf(req.getParameter("charte_etudiant")).booleanValue());
    mobiDto.setPreuveTestLinguistique(
        Boolean.valueOf(req.getParameter("preuve_test_linguistique")).booleanValue());
    mobiDto.setDocumentEngagement(
        Boolean.valueOf(req.getParameter("document_engagement")).booleanValue());
    mobiDto.setIdMobiliteEtudiant(Integer.parseInt(req.getParameter("id_mobilite")));
    mobiDto.setNumVersion(Integer.parseInt(req.getParameter("num_version")));
    DemandeMobiliteDto demande = bizzFactory.getDemandeMobilite();
    demande.setProgramme(req.getParameter("programme"));

    PaysDto pays = bizzFactory.getPays();
    pays.setCodeIso(req.getParameter("pays"));
    demande.setPays(pays);

    mobiDto.setDemande(demande);

    MobiliteEtudianteDto dto = mobEtUcc.confirmerDocumentsEtudiantRemplis(mobiDto);

    String json = genson.serialize(dto);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de preciser que les documents de retour de l etudiant sont recus.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void preciserDocumentsEtudiantRetour(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setAttestationSejourRecu(
        Boolean.valueOf(req.getParameter("attestation_sejour_recu")).booleanValue());
    mobiDto.setReleveNoteRecu(Boolean.valueOf(req.getParameter("releve_note_recu")).booleanValue());
    mobiDto.setCertificatStageRecu(
        Boolean.valueOf(req.getParameter("charte_etudiant")).booleanValue());
    mobiDto.setPreuveTestLinguistiqueRetour(
        Boolean.valueOf(req.getParameter("preuve_test_linguistique_retour")).booleanValue());
    mobiDto.setRapportFinal(Boolean.valueOf(req.getParameter("rapport_final")).booleanValue());
    mobiDto.setIdMobiliteEtudiant(Integer.parseInt(req.getParameter("id_mobilite")));
    mobiDto.setNumVersion(Integer.parseInt(req.getParameter("num_version")));
    PaysDto pays = bizzFactory.getPays();
    pays.setCodeIso(req.getParameter("pays"));
    DemandeMobiliteDto demande = bizzFactory.getDemandeMobilite();
    demande.setPays(pays);
    demande.setProgramme(req.getParameter("programme"));
    mobiDto.setDemande(demande);

    mobEtUcc.preciserDocumentsEtudiantRetour(mobiDto);
    String json = genson.serialize(mobiDto);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Permet d'indiquer que la mobilite est desormais en remboursement.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void indiquerMobiliteEnRemboursement(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setEtat("en_remboursement");
    mobiDto.setIdMobiliteEtudiant(Integer.parseInt(req.getParameter("id_mobilite")));
    mobiDto.setNumVersion(Integer.parseInt(req.getParameter("num_version")));

    mobEtUcc.indiquerMobiliteEnRemboursement(mobiDto);
    String json = genson.serialize(mobiDto);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Renvoie la liste des paiements.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void getListePaiements(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    DemandeMobiliteDto demDto = bizzFactory.getDemandeMobilite();
    demDto.setAnneeAcademique(Integer.parseInt(req.getParameter("annee_academique")));
    mobiDto.setDemande(demDto);

    List<MobiliteEtudianteDto> list = mobEtUcc.getListePaiements(mobiDto);

    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de rechercher une mobilite.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void rechercheMobi(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    DemandeMobiliteDto demDto = bizzFactory.getDemandeMobilite();
    demDto.setAnneeAcademique(Integer.parseInt(req.getParameter("annee_academique")));
    mobiDto.setDemande(demDto);
    mobiDto.setEtat(req.getParameter("etat"));

    List<MobiliteEtudianteDto> list = mobEtUcc.rechercheMobi(mobiDto);

    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet d envoyer la demande de paiement a la Haute Ecole.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void preciserEnvoyerDemandeHe(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setEtat("en_cours");
    mobiDto.setEnvoieDemandePaiement1(true);
    mobiDto.setIdMobiliteEtudiant(Integer.parseInt(req.getParameter("id_mobilite")));
    mobiDto.setNumVersion(Integer.parseInt(req.getParameter("num_version")));
    mobEtUcc.preciserEnvoyerdemandeHe(mobiDto);
    String json = genson.serialize(mobiDto);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de recevoir une mobilite a partir de son ID.
   * 
   * @param resp la reponse
   * 
   * @param req la requete
   */
  public void getMobiParId(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    MobiliteEtudianteDto mobiDto = bizzFactory.getMobiliteEtudiante();
    mobiDto.setIdMobiliteEtudiant(Integer.parseInt(req.getParameter("id_mobi")));

    MobiliteEtudianteDto dto = mobEtUcc.getMobiParId(mobiDto);

    String json = genson.serialize(dto);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }
}
