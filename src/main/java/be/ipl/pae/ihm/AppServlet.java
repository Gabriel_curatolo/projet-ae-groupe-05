package be.ipl.pae.ihm;

import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.exceptions.BizException;
import be.ipl.pae.exceptions.DalException;
import be.ipl.pae.exceptions.IhmException;
import be.ipl.pae.exceptions.MdpException;
import be.ipl.pae.main.Config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import org.eclipse.jetty.servlet.DefaultServlet;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AppServlet extends DefaultServlet {

  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = Logger.getLogger(AppServlet.class.getName());
  private static FileHandler fileHandler = null;

  private BizzFactory bizzFactory;
  private String secretServlet;

  ServletDemandeMobilite demandeMobiServ;
  ServletMobiliteEtudiante mobiEtudianteServ;
  ServletPersonne persServ;

  /**
   * Constructeur de l'appServlet.
   * 
   * @param bizzFactory Une bizz factory
   * 
   * @param config Un fichier config
   * 
   * @param demandeMobiServ la ServletDemandeMobilite
   * 
   * @param mobiEtudianteServ la ServletMobiliteEtudiante
   * 
   * @param persServ la ServletPersonne
   * 
   * @throws IOException when failed or interrupt I/O operation
   * 
   * @throws SecurityException when security violation
   * 
   */
  public AppServlet(BizzFactory bizzFactory, Config config, ServletDemandeMobilite demandeMobiServ,
      ServletMobiliteEtudiante mobiEtudianteServ, ServletPersonne persServ)
      throws SecurityException, IOException {
    super();
    this.bizzFactory = bizzFactory;

    this.secretServlet = config.getProperties("secret");
    this.demandeMobiServ = demandeMobiServ;
    this.mobiEtudianteServ = mobiEtudianteServ;
    this.persServ = persServ;
    initLog();

  }

  /**
   * Initialise les loggers.
   */
  public static void initLog() {
    try {
      fileHandler = new FileHandler("loggerTestMain.log", false);
    } catch (IOException exception) {
      exception.printStackTrace();
    }
    Logger logger = Logger.getLogger("");
    fileHandler.setFormatter(new SimpleFormatter());
    logger.addHandler(fileHandler);
    fileHandler.setLevel(Level.INFO);
  }


  // va servir pour charger les differentes parties de l'HTML(header,footer,body) et les assembler
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    if (req.getServletPath().equals("/")) {
      resp.setContentType("text/html;charset=UTF-8");
      resp.getOutputStream()
          .write(new String(Files.readAllBytes(Paths.get("www/html/header.html"))).getBytes());
      resp.getOutputStream()
          .write(new String(Files.readAllBytes(Paths.get("www/html/connexion_inscription.html")))
              .getBytes());
      resp.getOutputStream()
          .write(new String(Files.readAllBytes(Paths.get("www/html/etudiant.html"))).getBytes());
      resp.getOutputStream()
          .write(new String(Files.readAllBytes(Paths.get("www/html/professeur.html"))).getBytes());
      resp.getOutputStream()
          .write(new String(Files.readAllBytes(Paths.get("www/html/professeur_responsable.html")))
              .getBytes());
      resp.getOutputStream()
          .write(new String(Files.readAllBytes(Paths.get("www/html/recherche.html"))).getBytes());
      resp.getOutputStream()
          .write(new String(Files.readAllBytes(Paths.get("www/html/mesInfos.html"))).getBytes());
      resp.getOutputStream()
          .write(new String(Files.readAllBytes(Paths.get("www/html/footer.html"))).getBytes());
      resp.setStatus(200);
    } else {
      super.doGet(req, resp);
    }
  }


  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {
      String ret = null;

      PersonneDto persCo = getPersonneConnectee(req); // persDto ou NULL si pas connecte
      resp.setCharacterEncoding("UTF-8");

      if (req.getParameter("action") == null) {
        throw new IhmException("requete mal formee");
      }

      resp.setContentType("text/html");
      resp.setCharacterEncoding("utf-8");

      switch (req.getParameter("action")) {
        case "refresh":
          if (persCo != null) {
            // refresh(resp, persCo);
            resp.setStatus(200);
          } else {
            resp.setStatus(401);
          }
          break;
        case "connect":
          LOGGER.info("Un utilisateur veut se connecter");
          if (persCo != null) { // Deja connecte pourquoi aller plus loin ?
            resp.getOutputStream().write("{}".getBytes());
            resp.setStatus(200);

            return;
          }
          persServ.connexion(req, resp);
          break;
        case "professeur":
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          professeur(resp, persCo);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " va sur l'onglet professeur");
          break;
        case "etudiant":
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          etudiant(resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " va sur l'onglet etudiant");
          break;
        case "recherche":
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          etudiant(resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " va sur l'onglet recherche");
          break;
        case "prof_responsable":
          if (persCo == null) {
            throw new BizException("Acces refuse:utilisateur non connecte");
          }
          professeurResponsable(resp, persCo);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " va sur l'onglet professeur responsable");
          break;
        case "mes_infos":
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          etudiant(resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " va sur l'onglet mes infos");
          break;
        case "disconnect":
          deconnexion(resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " se deconnecte ");
          break;
        case "inscription":
          persServ.inscription(req, resp);
          LOGGER.info("Un utilisateur veut s'inscrire");
          break;
        // TODO
        case "modif_etat_mobi":
          break;
        /*
         * TOUT LES UCS L'ETUDIANT
         */
        case "encodage_donnees":
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          persServ.encodageDonnees(req, persCo, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " encode ses donnees");
          break;
        case "visualiser_ses_demandes":
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          demandeMobiServ.visualiserSesDemandes(resp, persCo);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " visualise ses mobilites");
          break;

        case "declarer_demande":
          persCo = getPersonneConnectee(req);
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          demandeMobiServ.declarerDemandeMobilite(req, resp, persCo);
          LOGGER.info(
              "l'utilisateur " + persCo.getIdPersonne() + " declare une demande de mobilite ");
          break;
        case "completion_nom_prenom_etudiant_recherche":
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          persServ.completionNomPrenomEtudiantRecherche(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " recherche un etudiant a partir de son nom et prenom,"
              + " pour cela nous realisons une completion");
          break;
        case "completion_nom_partenaire_pays_ville_recherche":
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          persServ.completionNomPartenairePaysVilleEtudiantRecherche(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " fais une recherche, pour ce faire nous realisons une completion"
              + " du nom du partenaire, du pays et de la ville recherchee");
          break;
        /*
         * TOUT LES UCS PROFESSEUR
         */
        case "extraire_csv":
          demandeMobiServ.extraireFormatCsv(resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " veut extraire les données");
          break;
        case "get_mobi_par_id":
          mobiEtudianteServ.getMobiParId(req, resp);

          LOGGER.info(
              "l'utilisateur " + persCo.getIdPersonne() + " est dans la methode get_mobi_par_id");
          break;

        case "indiquer_envoie_deuxieme_demande_paiement":
          mobiEtudianteServ.indiquerEnvoieDeuxiemeDemandePaiement(resp, req);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " indique l'envoie de la deuxieme demande de paiement de la mobilite : "
              + req.getParameter("id_mobilite_etudiante"));
          break;
        case "confirmer_demande_mobilite_avec_nouveau_partenaire":
          mobiEtudianteServ.confirmerDemandeMobiliteAvecNouveauPartenaire(resp, req);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " confirme une demande de mobilite avec un nouveau partenaire pour l'etudiant : "
              + req.getParameter("id_etudiant"));
          break;
        case "annuler_refuser_mobilite_etudiante":
          mobiEtudianteServ.annulerMobilite(resp, req);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " refuse la mobilite de  "
              + req.getParameter("id_mobilite_etudiante"));
          break;
        case "indiquer_envoie_demande_paiement":
          mobiEtudianteServ.indiquerEnvoieDemandePaiement(resp, req);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " indique l'envoie de la premiere demande de paiement de la mobilite :"
              + req.getParameter("id_mobilite_etudiante"));
          break;
        case "visualiser_tous_les_utilisateurs":
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          persServ.visualiserTousLesUtilisateurs(resp);
          LOGGER
              .info("l'utilisateur " + persCo.getIdPersonne() + " visualise tout les utilisateurs");
          break;
        case "confirmer_document_etudiant_rempli":
          mobiEtudianteServ.confirmerDocumentsEtudiantRemplis(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " confirme que les documents d un etudiant sont remplis pour la mobilite : "
              + req.getParameter("id_mobilite"));
          break;
        case "preciser_donnees_encodees_logiciel_externes":
          mobiEtudianteServ.preciserDonneesEncodeesLogicielExternes(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " indique l'encodage des donnees externes pour la mobilite : "
              + req.getParameter("id_mobilite"));
          break;
        case "preciser_document_etudiant_retour_recu":
          mobiEtudianteServ.preciserDocumentsEtudiantRetour(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " indique recu document etudiant retour pour la mobilite : "
              + req.getParameter("id_mobilite"));
          break;
        case "preciser_envoyer_demande_he":
          mobiEtudianteServ.preciserEnvoyerDemandeHe(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " indique l'envoie de la première demande a HE pour la mobilite : "
              + req.getParameter("id_mobilite"));
          break;
        // PAS SUR QUE CE SOIT UTILE, POURQUOI PAS FAIRE UNE SEULE REQUETE DE CHANGEMENT D ETAT?
        // TA PAS TORD
        case "indiquer_mobilite_en_remboursement":
          mobiEtudianteServ.indiquerMobiliteEnRemboursement(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " indique en remboursement la mobilite : " + req.getParameter("id_mobilite"));
          break;
        case "get_demande_mobi_par_annee":
          demandeMobiServ.getDemandeMobiParAnnee(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " regarde les mobilites pour l'annee : " + req.getParameter("annee"));
          break;
        case "get_liste_paiements":
          mobiEtudianteServ.getListePaiements(req, resp);
          LOGGER
              .info("l'utilisateur " + persCo.getIdPersonne() + " regarde la liste des paiements");
          break;
        case "recherche_partenaire":
          persServ.recherchePartenaires(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " recherche les partenaires");
          break;
        case "recherche_mobilite":
          mobiEtudianteServ.rechercheMobi(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " recherche les mobilites");
          break;
        case "recherche_etudiant":
          persServ.rechercheEtudiant(req, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " recherche un etudiant");
          break;
        case "get_mobilite_etudiant_connecte":
          mobiEtudianteServ.getMobiliteEtudiantConnecte(resp, persCo);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " get ses propres mobilites");
          break;
        case "get_mobilites_non_annulees":
          mobiEtudianteServ.getMobilitesNonAnnulees(resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " get mobilites non annulees");
          break;
        case "visualiser_toutes_les_demandes":
          demandeMobiServ.visualiserToutesLesDemandes(resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " visualise toutes les demandes");
          break;
        case "visualiser_toutes_les_mobilites":
          mobiEtudianteServ.visualiserToutesLesMobilites(resp);
          LOGGER
              .info("l'utilisateur " + persCo.getIdPersonne() + " visualise toutes les mobilites");
          break;
        case "confirmer_demande_mobilite":
          mobiEtudianteServ.confirmerDemandeMobilite(req, resp);
          LOGGER
              .info("l'utilisateur " + persCo.getIdPersonne() + " confirme la demande de mobilite");
          break;
        case "get_personne_par_id":
          PersonneDto persId = getPersonneConnectee(req);
          if (persId == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          ret = persServ.getPersonneParId(persId.getIdPersonne());
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne()
              + " cherche une personne a partir de son id");
          break;
        case "visualiser_un_utilisateur":
          persCo = getPersonneConnectee(req);
          if (persCo == null) {
            throw new BizException("Acces refuse : utilisateur non connecte");
          }
          persServ.visualiserUnUtilisateur(req, resp);
          LOGGER
              .info("l'utilisateur " + persCo.getIdPersonne() + " veut visualiser un utilisateur");
          break;
        case "get_mobilite_par_etudiant":
          mobiEtudianteServ.getMobiliteParEtudiant(req, resp);
          LOGGER.info(
              "l'utilisateur " + persCo.getIdPersonne() + " get les mobilites par l etudiant");
          break;
        /*
         * TOUT LES UC PROFESSEUR RESPONSABLE
         */

        case "augmenter_professeur":
          persServ.augmenterProfesseur(resp, req);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " veut augmenter un professeur");
          break;
        /*
         * INITIALISATION PAGE
         */
        case "get_pays":
          ret = demandeMobiServ.getPays();
          if (ret != null) {
            resp.getOutputStream().write(ret.getBytes());
          }
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " get les pays");
          break;
        case "get_programme":
          ret = demandeMobiServ.getProgrammes();
          if (ret != null) {
            resp.getOutputStream().write(ret.getBytes());
          }
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " get les programmes");
          break;
        case "get_codes_partenaires":
          demandeMobiServ.getCodesPartenaires(resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " get les codes de partenaires");
          break;
        case "get_noms_partenaires":
          demandeMobiServ.getNomsPartenaires(resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " get les noms de partenaire");
          break;
        case "get_personne":
          persServ.getPersonnesParId(persCo, resp);
          LOGGER.info("l'utilisateur " + persCo.getIdPersonne() + " get la personne par son ID");
          break;
        case "est_encode_personne":
          persServ.estEncodePersonne(resp, req, persCo);
          LOGGER.info(
              "l'on check si la personne  " + persCo.getIdPersonne() + " a ses donnees encodees");
          break;
        case "est_encode_partenaire":
          mobiEtudianteServ.estEncodeMobilite(resp, req, persCo);
          LOGGER.info(
              "l'utilisateur " + persCo.getIdPersonne() + " regarde si le partenaire est encodee");
          break;

        default:
          break;
      }

    } catch (IhmException exception) {
      resp.setStatus(400);
      resp.getOutputStream().write(exception.getMessage().getBytes());
      LOGGER.warning("IHM exception");
      exception.printStackTrace();
    } catch (BizException exception) {
      resp.getOutputStream().write(exception.getMessage().getBytes());
      resp.setStatus(412);
      LOGGER.severe("Exception Bizz");
      exception.printStackTrace();
    } catch (MdpException exception) {
      resp.setStatus(401);
      LOGGER.warning("Utilisateur a entrer le mauvais mot de passe");
      exception.printStackTrace();
    } catch (DalException exception) {
      resp.setStatus(500);
      LOGGER.severe("Exception DAL");
      exception.printStackTrace();
    } catch (Exception exception) {
      resp.setStatus(500);
      LOGGER.severe("Exception basique");
      exception.printStackTrace();
    }

  }

  private void etudiant(HttpServletResponse resp) throws IOException {
    resp.getOutputStream().write("reussi".getBytes());
    resp.setStatus(200);
  }

  private void professeur(HttpServletResponse resp, PersonneDto persCo) throws IOException {
    String ret = persServ.getPersonneParId(persCo.getIdPersonne());
    resp.getOutputStream().write(ret.getBytes());
    resp.setStatus(200);
  }

  private void professeurResponsable(HttpServletResponse resp, PersonneDto persCo)
      throws IOException {
    String ret = persServ.getPersonneParId(persCo.getIdPersonne());
    resp.getOutputStream().write(ret.getBytes());
    resp.setStatus(200);
  }


  private void deconnexion(HttpServletResponse resp) {
    Cookie cookie = new Cookie("tokenUser", null);
    cookie.setMaxAge(0);
    resp.addCookie(cookie);
    resp.setStatus(200);
  }

  /**
   * Utilise la requete pour trouver l'utilisateur connecte via le JWT stocke dans le cookie.
   * 
   * @param req la requete envoyee par le client
   * @return personneDto complet si connecte, null sinon
   */
  private PersonneDto getPersonneConnectee(HttpServletRequest req) {
    String tok = null;
    if (req.getCookies() == null) {
      return null;
    }
    for (Cookie cookie : req.getCookies()) {
      if (cookie.getName().equals("tokenUser")) {
        tok = cookie.getValue();
      }
    }
    if (tok != null) {
      Algorithm algorithm = Algorithm.HMAC256(secretServlet);
      JWTVerifier verifier = JWT.require(algorithm).build();
      DecodedJWT jwt = verifier.verify(tok);
      int persId = jwt.getClaim("id").asInt();

      /*
       * Si besoin, remplir ptemp avec tout les attributs de personne grace a une nouvelle methode
       * getPersonneByID()
       */
      PersonneDto ptemp = bizzFactory.getPersonne();
      ptemp.setIdPersonne(persId);
      return ptemp;
    }
    return null;
  }
}
