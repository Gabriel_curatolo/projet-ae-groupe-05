package be.ipl.pae.ihm;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.ucc.interfaces.PersonneUcc;
import be.ipl.pae.exceptions.IhmException;
import be.ipl.pae.main.Config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletPersonne {

  private PersonneUcc persUcc;
  private BizzFactory bizzFactory;
  private String secretServlet;
  private Genson genson;

  /**
   * Constructeur de la ServletPersonne.
   * 
   * @param persUcc Une personne ucc
   * 
   * @param bizzFactory Une bizz factory
   * 
   * @param config Un fichier config
   */
  public ServletPersonne(PersonneUcc persUcc, BizzFactory bizzFactory, Config config) {
    super();
    this.secretServlet = config.getProperties("secret");
    this.persUcc = persUcc;
    this.bizzFactory = bizzFactory;
    this.genson = new GensonBuilder().useRuntimeType(true).create();
  }

  /**
   * Permet a l utilisateur de se connecter.
   * 
   * @Param resp reponse de la methode
   * 
   * @param req requete de la methode
   * 
   */
  public void connexion(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    checkNull(req.getParameter("pseudo"));
    checkNull(req.getParameter("mdp"));
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setPseudo(req.getParameter("pseudo"));
    persDto.setMotDePasse(req.getParameter("mdp"));
    PersonneDto tempPerson = persUcc.checkConnexion(persDto);
    if (tempPerson == null) { // Mauvais pseudo ou identifiant
      resp.setStatus(404);
      return;
    }

    tempPerson.setMotDePasse("");

    // Envoi cookie au client
    String jwt = creerJwt(tempPerson.getIdPersonne());
    Cookie cookie = new Cookie("tokenUser", jwt);
    cookie.setMaxAge(60 * 60); // SET MAXAGE SUR UNE HEURE
    resp.addCookie(cookie);

    // Envoi dto au client
    String ret = genson.serialize(tempPerson);
    resp.getOutputStream().write(ret.getBytes());
    resp.setStatus(200);

  }

  /**
   * Création d'un JWT.
   * 
   * @Param id : id de l'utilisateur
   * 
   * @return tokenJWT String créé
   * 
   */
  public String creerJwt(int id) {
    String token = null;
    try {
      Algorithm algorithm = Algorithm.HMAC256(secretServlet);
      token = JWT.create().withClaim("id", id).sign(algorithm);
    } catch (JWTCreationException exception) {
      throw new IhmException(exception.getMessage());
    }
    return token;
  }

  /**
   * inscription d une personne dans l application.
   * 
   * @Param req requete de l utilisateur
   * 
   * @Param resp reponse a l utilisateur
   * 
   */
  public void inscription(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    checkNull(req.getParameter("pseudo_i"));
    checkNull(req.getParameter("nom"));
    checkNull(req.getParameter("prenom"));
    checkNull(req.getParameter("email"));
    checkNull(req.getParameter("mdp_i"));

    PersonneDto persInscription = bizzFactory.getPersonne();

    persInscription.setPseudo(req.getParameter("pseudo_i"));
    persInscription.setNom(req.getParameter("nom"));
    persInscription.setPrenom(req.getParameter("prenom"));
    persInscription.setMotDePasse(req.getParameter("mdp_i"));
    persInscription.setEmail(req.getParameter("email"));
    persInscription.setRole("et");
    persInscription.setNumVersion(1);
    persInscription = persUcc.inscription(persInscription);
    persInscription.setNumVersion(0);
    persInscription.setMotDePasse("");
    persInscription.setEmail("");
    resp.getOutputStream().write(genson.serialize(persInscription).getBytes());
    resp.setStatus(200);
  }

  /**
   * Renvoie une personne a partir de son Id.
   * 
   * @Param idPersonne id de la personne que l on cherche
   * 
   * @return json de la personne recherche
   * 
   */
  public String getPersonneParId(int idPersonne) {
    String json = null;
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setIdPersonne(idPersonne);
    PersonneDto ret = persUcc.getPersonneById(persDto);
    json = genson.serialize(ret);
    return json;
  }

  /**
   * Permet de visualiser tous les utilisateurs.
   * 
   * @Param resp reponse de la methode
   * 
   */
  public void visualiserTousLesUtilisateurs(HttpServletResponse resp) throws IOException {
    List<PersonneDto> list = persUcc.visualiserTousLesUtilisateurs();
    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Permet l augmentation d un professeur.
   * 
   * @Param resp reponse de la methode
   * 
   * @Param req requete de la methode
   */
  public void augmenterProfesseur(HttpServletResponse resp, HttpServletRequest req)
      throws IOException {
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setEmail(req.getParameter("email"));
    PersonneDto ret = persUcc.augmenterProfesseur(persDto);
    String json = genson.serialize(ret);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Permet de visualiser un utilisateur.
   * 
   * @Param resp reponse de la methode
   * 
   * @Param req requete de la methode
   */
  public void visualiserUnUtilisateur(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    checkNull(req.getParameter("nom"));
    checkNull(req.getParameter("prenom"));

    PersonneDto persDto = bizzFactory.getPersonne();
    System.out.println("le nom est :" + req.getParameter("nom"));
    System.out.println("le prenom est :" + req.getParameter("prenom"));
    persDto.setNom(req.getParameter("nom"));
    persDto.setPrenom(req.getParameter("prenom"));
    persDto = persUcc.visualiserUnUtilisateur(persDto);
    String json = genson.serialize(persDto);
    System.out.println(json);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Permet de renvoyer la personne connectee a partir de son Id.
   * 
   * @param persCo la personne connectee
   * 
   * @Param resp reponse de la methode
   * 
   */
  public void getPersonnesParId(PersonneDto persCo, HttpServletResponse resp) throws IOException {

    PersonneDto persDto = persUcc.getPersonneById(persCo);
    String json = genson.serialize(persDto);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Permet d encoder les donnees d un utilisateur.
   * 
   * @param req la requete de la methode
   * 
   * @param persCo la personne connectee
   * 
   * @Param resp reponse de la methode
   * 
   */
  public void encodageDonnees(HttpServletRequest req, PersonneDto persCo, HttpServletResponse resp)
      throws IOException {

    checkNull(req.getParameter("civilite"));
    checkNull(req.getParameter("sexe"));
    checkNull(req.getParameter("telephone"));
    checkNull(req.getParameter("date_naissance"));
    checkNull(req.getParameter("no_compte"));
    checkNull(req.getParameter("nb_annees_reussies"));
    checkNull(req.getParameter("titulaire_compte"));
    checkNull(req.getParameter("banque"));
    checkNull(req.getParameter("code_bic"));
    checkNull(req.getParameter("departement"));
    checkNull(req.getParameter("rue"));
    checkNull(req.getParameter("no"));
    checkNull(req.getParameter("code_postal"));
    checkNull(req.getParameter("ville"));
    checkNull(req.getParameter("commune"));
    checkNull(req.getParameter("boite"));
    checkNull(req.getParameter("region"));
    checkNull(req.getParameter("nationalite"));
    checkNull(req.getParameter("num_version_personne"));


    // remplissage
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setIdPersonne(persCo.getIdPersonne());
    persDto.setSexe(req.getParameter("sexe"));
    persDto.setTelephone(req.getParameter("telephone"));
    persDto.setCivilite(req.getParameter("civilite"));
    persDto.setDateNaissance(req.getParameter("date_naissance"));
    persDto.setNoCompte(req.getParameter("no_compte"));
    persDto.setNbAnneeReussies(Integer.parseInt(req.getParameter("nb_annees_reussies")));
    persDto.setTitulaireCompte(req.getParameter("titulaire_compte"));
    persDto.setBanque(req.getParameter("banque"));
    persDto.setCodeBic(req.getParameter("code_bic"));
    persDto.setDepartement(req.getParameter("departement"));
    PaysDto paysDto = bizzFactory.getPays();
    paysDto.setNom(req.getParameter("nationalite"));
    persDto.setNationalite(paysDto);
    persDto.setNumVersion(Integer.parseInt(req.getParameter("num_version_personne")));
    AdresseDto adrDto = bizzFactory.getAdresse();
    adrDto.setRue(req.getParameter("rue"));
    adrDto.setNumero(Integer.parseInt(req.getParameter("no")));
    adrDto.setCodePostal(req.getParameter("code_postal"));
    adrDto.setVille(req.getParameter("ville"));
    adrDto.setCommune(req.getParameter("commune"));
    adrDto.setBoite(req.getParameter("boite"));
    adrDto.setRegion(req.getParameter("region"));
    persDto.setAdresse(adrDto);

    PersonneDto persDtoReturn = persUcc.encodageDonnees(persDto);

    String json = genson.serialize(persDtoReturn);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Permet de verifier si une personne a ses donnees encodees.
   * 
   * @Param resp reponse de la methode
   * 
   * @param req requete de la methode
   * 
   */
  public void estEncodePersonne(HttpServletResponse resp, HttpServletRequest req,
      PersonneDto persCo) throws IOException {
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setIdPersonne(persCo.getIdPersonne());

    PersonneDto dtoReturn = persUcc.getDonnees(persDto);
    String json = genson.serialize(dtoReturn);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de rechercher un partenaire.
   * 
   * @Param resp reponse de la methode
   * 
   * @param req requete de la methode
   * 
   */
  public void recherchePartenaires(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    PartenaireDto partenaire = bizzFactory.getPartenaire();
    partenaire.setNomComplet(req.getParameter("nom"));
    PaysDto pays = bizzFactory.getPays();
    pays.setNom(req.getParameter("nom_pays"));
    partenaire.setPays(pays);
    AdresseDto adrDto = bizzFactory.getAdresse();
    adrDto.setVille(req.getParameter("ville"));
    partenaire.setAdresse(adrDto);

    List<PartenaireDto> list = persUcc.recherchePartenaires(partenaire);

    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de verifier si la string en parametre est a null.
   * 
   * @Param champ ce que l on souhaite verifier.
   * 
   */
  private void checkNull(String champ) {
    if (champ == null) {
      throw new IhmException("requete mal formee");
    }
  }

  /**
   * Permet de rechercher un etudiant.
   * 
   * @Param resp reponse de la methode
   * 
   * @param req requete de la methode
   * 
   */
  public void rechercheEtudiant(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setNom(req.getParameter("nom"));
    persDto.setPrenom(req.getParameter("prenom"));

    List<PersonneDto> list = persUcc.recherchePersonnes(persDto);
    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de faire une completion du nom et du prenom de l etudiant recherche.
   * 
   * @Param resp reponse de la methode
   * 
   * @param req requete de la methode
   * 
   */
  public void completionNomPrenomEtudiantRecherche(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {

    List<PersonneDto> list = persUcc.completionNomPrenomEtudiantRecherche();

    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Permet de faire une completion du nom du partenaire du pays et de la ville de l etudiant
   * recherche.
   * 
   * @Param resp reponse de la methode
   * 
   * @param req requete de la methode
   * 
   */
  public void completionNomPartenairePaysVilleEtudiantRecherche(HttpServletRequest req,
      HttpServletResponse resp) throws IOException {
    List<PartenaireDto> list = persUcc.completionNomPartenairePaysVilleEtudiantRecherche();

    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }



}
