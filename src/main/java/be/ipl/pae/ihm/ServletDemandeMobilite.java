package be.ipl.pae.ihm;

import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.ucc.interfaces.DemandeMobiliteUcc;
import be.ipl.pae.exceptions.IhmException;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletDemandeMobilite {

  private BizzFactory bizzFactory;
  private DemandeMobiliteUcc demMobUcc;
  private Genson genson;

  /**
   * Constructeur de la ServletDemandeMobilite.
   * 
   * @param bizzFactory Une bizz factory
   * 
   * @param demMobUcc une demande mobilite ucc
   * 
   */
  public ServletDemandeMobilite(BizzFactory bizzFactory, DemandeMobiliteUcc demMobUcc) {
    super();
    this.bizzFactory = bizzFactory;
    this.demMobUcc = demMobUcc;
    this.genson = new GensonBuilder().useRuntimeType(true).create();
  }

  /**
   * Visualisation des demandes de mobilite de la personne connecte.
   * 
   * @param resp reponse de la methode
   * 
   * @param persCo la personne connectee
   * 
   */
  public void visualiserSesDemandes(HttpServletResponse resp, PersonneDto persCo)
      throws IOException {
    List<DemandeMobiliteDto> list = demMobUcc.visualiserSesDemandes(persCo);
    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }



  /**
   * Declaration d une demande de mobilite.
   * 
   * @param resp reponse de la methode
   * 
   * @param req contient les donnees de la mobilite
   * 
   * @param personCo persCo la personne connectee
   * 
   */
  public void declarerDemandeMobilite(HttpServletRequest req, HttpServletResponse resp,
      PersonneDto personCo) throws IOException {

    checkNull(req.getParameter("ordre"));
    checkNull(req.getParameter("programme"));
    checkNull(req.getParameter("code"));
    checkNull(req.getParameter("pays"));


    PartenaireDto partDto = bizzFactory.getPartenaire();
    partDto.setNomComplet(req.getParameter("partExistString"));
    DemandeMobiliteDto demandeMobiliteDto = bizzFactory.getDemandeMobilite();
    demandeMobiliteDto.setPartenaire(partDto);
    demandeMobiliteDto.setPersonne(personCo);
    PaysDto paysDto = demMobUcc.getIsoParPays(req.getParameter("pays"));
    demandeMobiliteDto.setPays(paysDto);
    demandeMobiliteDto.setConfirme(false);
    demandeMobiliteDto.setProgramme(req.getParameter("programme"));
    demandeMobiliteDto.setCode(req.getParameter("code"));
    int quadrimestreInt = Integer.parseInt(req.getParameter("quadri"));
    demandeMobiliteDto.setQuadrimestre(quadrimestreInt);
    int ordreInt = Integer.parseInt(req.getParameter("ordre"));
    demandeMobiliteDto.setOrdrePreference(ordreInt);
    demandeMobiliteDto.setAnneeAcademique(Calendar.getInstance().get(Calendar.YEAR));

    DemandeMobiliteDto demRep = demMobUcc.declarerDemandeMobilite(demandeMobiliteDto);
    String json = genson.serialize(demRep);

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Demande de visualisation de toutes les demandes de mobilites.
   * 
   * @param resp reponse de la methode
   */
  public void visualiserToutesLesDemandes(HttpServletResponse resp) throws IOException {
    List<DemandeMobiliteDto> list = demMobUcc.visualiserToutesLesDemandes();
    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }

  /**
   * Recherche de tout les pays en base de donnees.
   * 
   */
  public String getPays() {
    // for each pays liste Pays json.serialize paysDto
    List<PaysDto> list = demMobUcc.getAllPays();
    List<String> listRet = new ArrayList<String>();

    for (PaysDto p : list) {
      listRet.add(p.getNom());
    }
    String json = genson.serialize(listRet.toArray(new String[listRet.size()]));
    return json;
  }

  /**
   * Recherche de tout les programmes en base de donnees.
   * 
   */
  public String getProgrammes() {
    List<String> listRet = new ArrayList<String>();
    List<String> list = demMobUcc.getProgrammes();
    for (String p : list) {
      listRet.add(p);
    }
    String json = genson.serialize(listRet.toArray(new String[listRet.size()]));
    return json;
  }

  /**
   * Recherche de tout les codes partenaire en base de donnees.
   * 
   * @param resp reponse de la methode
   * 
   */
  public void getCodesPartenaires(HttpServletResponse resp) throws IOException {
    List<PartenaireDto> list = demMobUcc.getPartenaires();
    List<String> listRet = new ArrayList<String>();

    for (PartenaireDto p : list) {
      if (!listRet.contains(p.getCode())) {
        listRet.add(p.getCode());
      }
    }
    String json = genson.serialize(listRet.toArray(new String[listRet.size()]));
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * Recherche de tout les nom partenaire en base de donnees.
   * 
   * @param resp reponse de la methode
   * 
   */
  public void getNomsPartenaires(HttpServletResponse resp) throws IOException {
    List<PartenaireDto> list = demMobUcc.getPartenaires();
    List<String> listRet = new ArrayList<String>();

    for (PartenaireDto p : list) {
      listRet.add(p.getNomComplet());
    }
    String json = genson.serialize(listRet.toArray(new String[listRet.size()]));

    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);
  }


  /**
   * Check si le parametre est null.
   * 
   * @param ce qu on veut verifier
   */
  private void checkNull(String champ) {
    if (champ == null) {
      throw new IhmException("requete mal formee");
    }
  }

  /**
   * Recherche des demandes de mobilite sur une annee.
   * 
   * @param resp reponse de la methode
   * 
   * @param req contient l annee recherche
   * 
   * 
   */
  public void getDemandeMobiParAnnee(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    DemandeMobiliteDto demDto = bizzFactory.getDemandeMobilite();
    demDto.setAnneeAcademique(Integer.parseInt(req.getParameter("annee_academique")));

    List<DemandeMobiliteDto> list = demMobUcc.getDemandeMobiParAnnee(demDto);
    String json = genson.serialize(list);
    resp.getOutputStream().write(json.getBytes());
    resp.setStatus(200);

  }

  /**
   * extrait les demandes de mobilite en format csv.
   * 
   */
  public void extraireFormatCsv(HttpServletResponse resp) throws IOException {
    StringBuilder stringBuilder = demMobUcc.extraireFormatCsv();

    resp.setContentType("text/csv");
    resp.setHeader("Content-Disposition", "attachment; filename=\"liste_demandes.csv\"");
    resp.getOutputStream().write(stringBuilder.toString().getBytes());
    resp.setStatus(200);

  }


}
