package be.ipl.pae.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Config {
  private Properties properties;

  /**
   * constructeur de Config.
   */
  public Config(String fichier) {
    this.properties = new Properties();
    try {
      properties.load(new FileInputStream(new File(fichier)));
    } catch (FileNotFoundException exception) {
      exception.printStackTrace();
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }

  public String getProperties(String key) {
    return properties.getProperty(key);
  }
}
