package be.ipl.pae.main;

import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.factory.BizzFactoryImpl;
import be.ipl.pae.bizz.ucc.impl.DemandeMobiliteUccImpl;
import be.ipl.pae.bizz.ucc.impl.MobiliteEtudianteUccImpl;
import be.ipl.pae.bizz.ucc.impl.PersonneUccImpl;
import be.ipl.pae.bizz.ucc.interfaces.DemandeMobiliteUcc;
import be.ipl.pae.bizz.ucc.interfaces.MobiliteEtudianteUcc;
import be.ipl.pae.bizz.ucc.interfaces.PersonneUcc;
import be.ipl.pae.dal.dao.impl.AdresseDaoImpl;
import be.ipl.pae.dal.dao.impl.DemandeMobiliteDaoImpl;
import be.ipl.pae.dal.dao.impl.MobiliteEtudianteDaoImpl;
import be.ipl.pae.dal.dao.impl.PartenaireDaoImpl;
import be.ipl.pae.dal.dao.impl.PaysDaoImpl;
import be.ipl.pae.dal.dao.impl.PersonneDaoImpl;
import be.ipl.pae.dal.dao.interfaces.AdresseDao;
import be.ipl.pae.dal.dao.interfaces.DemandeMobiliteDao;
import be.ipl.pae.dal.dao.interfaces.MobiliteEtudianteDao;
import be.ipl.pae.dal.dao.interfaces.PartenaireDao;
import be.ipl.pae.dal.dao.interfaces.PaysDao;
import be.ipl.pae.dal.dao.interfaces.PersonneDao;
import be.ipl.pae.dal.services.DalServicesImpl;
import be.ipl.pae.ihm.AppServlet;
import be.ipl.pae.ihm.ServletDemandeMobilite;
import be.ipl.pae.ihm.ServletMobiliteEtudiante;
import be.ipl.pae.ihm.ServletPersonne;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppContext;

import javax.servlet.http.HttpServlet;


public class Main {
  /**
   * main.
   * 
   * @param args args
   * 
   * @throws Exception exception
   */
  public static void main(String[] args) throws Exception {


    Config config = new Config("prod.properties");
    BizzFactory bizFact = new BizzFactoryImpl();
    DalServicesImpl ds = new DalServicesImpl(config);
    PersonneDao persDao = new PersonneDaoImpl(ds, bizFact);
    AdresseDao adrDao = new AdresseDaoImpl(ds);
    PaysDao paysDao = new PaysDaoImpl(ds, bizFact);
    PartenaireDao partDao = new PartenaireDaoImpl(ds, bizFact);
    PersonneUcc persUcc = new PersonneUccImpl(persDao, ds, adrDao, paysDao, partDao);


    DemandeMobiliteDao demMobDao = new DemandeMobiliteDaoImpl(ds, bizFact);
    DemandeMobiliteUcc demMobUcc = new DemandeMobiliteUccImpl(demMobDao, paysDao, ds, partDao);

    MobiliteEtudianteDao mobEtDao = new MobiliteEtudianteDaoImpl(ds, bizFact);
    MobiliteEtudianteUcc mobEtUcc =
        new MobiliteEtudianteUccImpl(mobEtDao, ds, demMobDao, partDao, paysDao, adrDao);

    ServletDemandeMobilite demandeMobiServlet = new ServletDemandeMobilite(bizFact, demMobUcc);
    ServletMobiliteEtudiante mobiEtudServlet = new ServletMobiliteEtudiante(bizFact, mobEtUcc);
    ServletPersonne persServlet = new ServletPersonne(persUcc, bizFact, config);
    HttpServlet appServlet =
        new AppServlet(bizFact, config, demandeMobiServlet, mobiEtudServlet, persServlet);


    WebAppContext context = new WebAppContext(); // objet de configuration
    context.setInitParameter("cacheControl", "no-store,nocache,must-revalidate");

    ServletHolder servletHolder = new ServletHolder(appServlet);
    context.addServlet(servletHolder, "/all/*");
    context.addServlet(servletHolder, "/");
    context.setResourceBase("www"); // répertoire des fichiers html etc

    String port = config.getProperties("port");
    Server server = new Server(Integer.parseInt(port)); // serveur sur port 8380 => Port 8080 OQP

    server.setHandler(context); // donne la configuration au serveur
    server.start(); // démarrage

  }

}
