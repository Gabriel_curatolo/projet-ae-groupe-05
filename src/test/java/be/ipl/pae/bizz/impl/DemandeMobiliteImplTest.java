package be.ipl.pae.bizz.impl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.factory.BizzFactoryStub;
import be.ipl.pae.exceptions.BizException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class DemandeMobiliteImplTest {

  private static BizzFactory bizzFactory;
  private DemandeMobiliteImpl demMob;

  @BeforeAll
  public static void setup() throws Exception {
    bizzFactory = new BizzFactoryStub();
  }

  @BeforeEach
  public void reset() throws Exception {
    this.demMob = (DemandeMobiliteImpl) bizzFactory.getDemandeMobilite();
  }

  @DisplayName("Test methode checkDeclarerDemandeMobilites correcte")
  @Test
  public void checkDeclarerDemandeMobilites1() {
    assertTrue(demMob.checkDeclarerDemandeMobilites());
  }

  @DisplayName("Test methode checkDeclarerDemandeMobilites champs vides")
  @Test
  public void checkDeclarerDemandeMobilites2() {
    demMob.setProgramme("");
    demMob.setCode("");
    assertThrows(BizException.class, () -> demMob.checkDeclarerDemandeMobilites());
  }

  @DisplayName("Test methode checkDeclarerDemandeMobilites incorrecte")
  @Test
  public void checkDeclarerDemandeMobilites3() {
    demMob.setOrdrePreference(-1);
    demMob.setProgramme("Hello");
    demMob.setCode("salut");
    demMob.setConfirme(true);
    demMob.setQuadrimestre(3);
    demMob.setPersonne(null);
    assertThrows(BizException.class, () -> demMob.checkDeclarerDemandeMobilites());
  }

}
