package be.ipl.pae.bizz.impl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.factory.BizzFactoryStub;
import be.ipl.pae.exceptions.BizException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MobiliteEtudianteImplTest {

  private static BizzFactory bizzFactory;
  private MobiliteEtudianteImpl mob;

  @BeforeAll
  public static void setup() throws Exception {
    bizzFactory = new BizzFactoryStub();
  }

  @BeforeEach
  public void reset() throws Exception {
    this.mob = (MobiliteEtudianteImpl) bizzFactory.getMobiliteEtudiante();
  }

  @DisplayName("Test methode checkConfirmerMobilite correcte")
  @Test
  public void checkConfirmerMobilite1() {
    assertTrue(mob.checkConfirmerMobilite());
  }

  @DisplayName("Test methode checkConfirmerMobilite champs vides")
  @Test
  public void checkConfirmerMobilite2() {
    mob.setEtat("");
    assertThrows(BizException.class, () -> mob.checkConfirmerMobilite());
  }

  @DisplayName("Test methode checkConfirmerMobilite champs incorrects")
  @Test
  public void checkConfirmerMobilite3() {
    mob.setEtat("encoursdetraitement");
    mob.setQuadrimestre(3);
    assertThrows(BizException.class, () -> mob.checkConfirmerMobilite());
  }
}


