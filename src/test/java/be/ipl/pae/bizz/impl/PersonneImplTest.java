package be.ipl.pae.bizz.impl;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.factory.BizzFactoryStub;
import be.ipl.pae.exceptions.BizException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
// import org.junit.Test;
import org.junit.jupiter.api.Test;

public class PersonneImplTest {

  private static BizzFactory bizzFactory;
  private PersonneImpl personne;

  @BeforeAll
  public static void setup() throws Exception {
    bizzFactory = new BizzFactoryStub();
  }

  @BeforeEach
  public void reset() throws Exception {
    this.personne = (PersonneImpl) bizzFactory.getPersonne();
  }

  @DisplayName("Test methode checkPwd avec mot de passe correct")
  @Test
  public void checkPwd1() {
    personne.hashPwd();
    assertTrue(personne.checkPwd("123"));
  }

  @DisplayName("Test methode checkPwd avec mot de passe errone")
  @Test
  public void checkPwd2() {
    personne.hashPwd();
    assertFalse(personne.checkPwd("aaa"));
  }

  @DisplayName("Test methode checkPwd avec mot de passe null")
  @Test
  public void checkPwd3() {
    personne.hashPwd();
    assertThrows(BizException.class, () -> personne.checkPwd(""));
  }

  @DisplayName("Test methode hashPwd avec hashage correct")
  @Test
  public void hashPwd1() {
    personne.setMotDePasse("test");
    personne.hashPwd();
    assertAll(() -> assertTrue(!personne.getMotDePasse().equals("test")),
        () -> assertTrue(personne.checkPwd("test")));
  }

  @DisplayName("Test methode checkInscription correcte")
  @Test
  public void checkInscription1() {
    assertTrue(personne.checkInscription());
  }

  @DisplayName("Test methode checkInscription champs vides")
  @Test
  public void checkInscription2() {
    personne.setMotDePasse("");
    personne.setEmail("");
    personne.setNom("");
    personne.setPrenom("");
    personne.setPseudo("");
    personne.setRole("");
    assertThrows(BizException.class, () -> personne.checkInscription());
  }

  @DisplayName("Test methode checkInscription incorrecte")
  @Test
  public void checkInscription3() {
    personne.setEmail("deh.deu");
    personne.setNom("roada5667");
    personne.setPrenom("dehd587.");
    personne.setRole("slthde");
    assertThrows(BizException.class, () -> personne.checkInscription());
  }


  @DisplayName("Test methode checkEncodageDonnees correct")
  @Test
  public void checkEncodageDonnees1() {
    assertTrue(personne.checkEncodageDonnees());
  }

  @DisplayName("Test methode checkEncodageDonnees incorrect")
  @Test
  public void checkEncodageDonnees2() {
    personne.setNoCompte("BE5456");
    personne.setBanque("hell5565");
    personne.setDepartement("654fefef6");
    personne.setNbAnneeReussies(-1);
    personne.setTitulaireCompte("opdep5587");

    assertThrows(BizException.class, () -> personne.checkEncodageDonnees());
  }

  @DisplayName("Test methode checkEncodageDonnees champs vides")
  @Test
  public void checkEncodageDonnees3() {
    personne.setNoCompte("");
    personne.setBanque("");
    personne.setCivilite("");
    personne.setDateNaissance("");
    personne.setDepartement("");
    personne.setSexe("");
    personne.setTelephone("");
    personne.setTitulaireCompte("");
    personne.setCodeBic("");

    assertThrows(BizException.class, () -> personne.checkEncodageDonnees());
  }

}
