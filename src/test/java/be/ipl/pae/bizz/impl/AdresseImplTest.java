package be.ipl.pae.bizz.impl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.factory.BizzFactoryStub;
import be.ipl.pae.exceptions.BizException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AdresseImplTest {

  private static BizzFactory bizzFactory;
  private AdresseImpl adr;

  @BeforeAll
  public static void setup() throws Exception {
    bizzFactory = new BizzFactoryStub();
  }

  @BeforeEach
  public void reset() throws Exception {
    this.adr = (AdresseImpl) bizzFactory.getAdresse();
  }

  @DisplayName("Test methode checkEncodageAdresses correcte")
  @Test
  public void checkDeclarerDemandeMobilites1() {
    assertTrue(adr.checkEncodageAdresses());
  }

  @DisplayName("Test methode checkEncodageAdresses champs vides ")
  @Test
  public void checkDeclarerDemandeMobilites2() {
    adr.setRue("");
    adr.setVille("");
    assertThrows(BizException.class, () -> adr.checkEncodageAdresses());
  }

  @DisplayName("Test methode checkEncodageAdresses champs incorrects ")
  @Test
  public void checkDeclarerDemandeMobilites3() {
    adr.setRue("Rue de 55749");
    adr.setVille("Bruxe558");
    assertThrows(BizException.class, () -> adr.checkEncodageAdresses());
  }
}
