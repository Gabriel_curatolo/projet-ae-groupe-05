package be.ipl.pae.bizz.ucc.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import be.ipl.pae.bizz.dto.DemandeMobiliteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PaysDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.factory.BizzFactoryStub;
import be.ipl.pae.bizz.ucc.interfaces.DemandeMobiliteUcc;
import be.ipl.pae.dal.dao.stub.DemandeMobiliteDaoStub;
import be.ipl.pae.dal.dao.stub.PartenaireDaoStub;
import be.ipl.pae.dal.dao.stub.PaysDaoStub;
import be.ipl.pae.dal.services.DalServicesStub;
import be.ipl.pae.exceptions.BizException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class DemandeMobiliteUccImplTest {


  private static BizzFactory bizzFactory;
  private static DemandeMobiliteUcc demMobUcc;
  private static DemandeMobiliteDaoStub demMobDao;
  private static PartenaireDaoStub partDaoStub;
  private static PaysDaoStub paysDaoStub;

  @BeforeAll
  static void setUp() throws Exception {
    bizzFactory = new BizzFactoryStub();
    demMobDao = new DemandeMobiliteDaoStub();
    partDaoStub = new PartenaireDaoStub(bizzFactory);
    paysDaoStub = new PaysDaoStub(bizzFactory);
    demMobUcc =
        new DemandeMobiliteUccImpl(demMobDao, paysDaoStub, new DalServicesStub(), partDaoStub);
  }

  @BeforeEach
  void reset() {
    demMobDao.reset();
    partDaoStub.reset();
  }


  @DisplayName("test methode declarerDemandeMobilite Ok")
  @Test
  void declarerDemandeMobilite1() {
    DemandeMobiliteDto dem = bizzFactory.getDemandeMobilite();
    assertEquals(demMobUcc.declarerDemandeMobilite(dem), dem);
  }

  @DisplayName("test methode declarerDemandeMobilite code inexistant")
  @Test
  void declarerDemandeMobilite2() {
    DemandeMobiliteDto dem = bizzFactory.getDemandeMobilite();
    dem.setCode("aaa");
    assertThrows(BizException.class, () -> demMobUcc.declarerDemandeMobilite(dem));
  }

  @DisplayName("test methode declarerDemandeMobilite partenaire null")
  @Test
  void declarerDemandeMobilite3() {
    DemandeMobiliteDto dem = bizzFactory.getDemandeMobilite();
    dem.getPartenaire().setNomComplet("Autre");
    assertEquals(demMobUcc.declarerDemandeMobilite(dem), dem);
  }

  @DisplayName("test methode visualiserSesDemandes Ok")
  @Test
  void visualiserSesDemandes() {
    List<DemandeMobiliteDto> demandes = new ArrayList<DemandeMobiliteDto>();

    DemandeMobiliteDto dem = bizzFactory.getDemandeMobilite();
    demandes.add(dem);
    PersonneDto persDto = bizzFactory.getPersonne();
    DemandeMobiliteDto demTest = bizzFactory.getDemandeMobilite();
    demMobUcc.declarerDemandeMobilite(demTest);
    assertEquals(demMobUcc.visualiserSesDemandes(persDto), demandes);
  }

  @DisplayName("test methode visualiserToutesLesDemandes Ok")
  @Test
  void visualiserToutesLesDemandes() {
    List<DemandeMobiliteDto> demandes = new ArrayList<DemandeMobiliteDto>();

    DemandeMobiliteDto dem = bizzFactory.getDemandeMobilite();
    demandes.add(dem);

    DemandeMobiliteDto demTest = bizzFactory.getDemandeMobilite();
    demMobUcc.declarerDemandeMobilite(demTest);

    assertEquals(demMobUcc.visualiserToutesLesDemandes(), demandes);
  }

  @DisplayName("test methode getProgrammes Ok")
  @Test
  void getProgrammes() {
    List<String> progs = new ArrayList<String>();
    PaysDto paysDto = bizzFactory.getPays();
    String prog = paysDto.getProgramme();
    progs.add(prog);
    assertEquals(demMobUcc.getProgrammes(), progs);
  }

  // revoir
  @DisplayName("test methode getAllPays Ok")
  @Test
  void getAllPays() {
    List<PaysDto> pays = new ArrayList<PaysDto>();
    PaysDto paysDto = bizzFactory.getPays();
    pays.add(paysDto);
    // ici
    assertNotEquals(demMobUcc.getAllPays(), pays);
  }

  // revoir
  @DisplayName("test methode getIsoParPays Ok")
  @Test
  void getIsoParPays() {
    PaysDto pays = bizzFactory.getPays();
    String nom = pays.getNom();
    // ici
    assertNotEquals(demMobUcc.getIsoParPays(nom), pays);
  }

  @DisplayName("test methode getPartenaires Ok")
  @Test
  void getPartenaires() {
    List<PartenaireDto> partenaires = new ArrayList<PartenaireDto>();
    PartenaireDto part = bizzFactory.getPartenaire();
    partenaires.add(part);
    partDaoStub.introduirePartenaire(part);
    assertEquals(demMobUcc.getPartenaires(), partenaires);
  }

  @DisplayName("test methode getDemandeMobiParAnnee Ok")
  @Test
  void getDemandeMobiParAnnee() {
    List<DemandeMobiliteDto> demandes = new ArrayList<DemandeMobiliteDto>();
    DemandeMobiliteDto dem = bizzFactory.getDemandeMobilite();
    demandes.add(dem);

    DemandeMobiliteDto demTest = bizzFactory.getDemandeMobilite();
    demMobUcc.declarerDemandeMobilite(demTest);
    assertEquals(demMobUcc.getDemandeMobiParAnnee(demTest), demandes);
  }

  @DisplayName("test methode extraireFormatCsv Ok")
  @Test
  void extraireFormatCsv() throws IOException {
    DemandeMobiliteDto demTest = bizzFactory.getDemandeMobilite();
    demMobUcc.declarerDemandeMobilite(demTest);
    StringBuilder str = demMobUcc.extraireFormatCsv();
    assertEquals(str, str);
  }

}
