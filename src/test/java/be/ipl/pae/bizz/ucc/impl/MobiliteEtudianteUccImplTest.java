package be.ipl.pae.bizz.ucc.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import be.ipl.pae.bizz.dto.AdresseDto;
import be.ipl.pae.bizz.dto.MobiliteEtudianteDto;
import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.factory.BizzFactoryStub;
import be.ipl.pae.bizz.ucc.interfaces.MobiliteEtudianteUcc;
import be.ipl.pae.dal.dao.stub.AdresseDaoStub;
import be.ipl.pae.dal.dao.stub.DemandeMobiliteDaoStub;
import be.ipl.pae.dal.dao.stub.MobiliteEtudianteDaoStub;
import be.ipl.pae.dal.dao.stub.PartenaireDaoStub;
import be.ipl.pae.dal.dao.stub.PaysDaoStub;
import be.ipl.pae.dal.services.DalServicesStub;
import be.ipl.pae.exceptions.BizException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class MobiliteEtudianteUccImplTest {

  private static BizzFactory bizzFactory;
  private static MobiliteEtudianteUcc mobEtudUcc;
  private static MobiliteEtudianteDaoStub mobDaoStub;

  @BeforeAll
  static void setUp() throws Exception {
    bizzFactory = new BizzFactoryStub();
    mobDaoStub = new MobiliteEtudianteDaoStub(bizzFactory);
    mobEtudUcc = new MobiliteEtudianteUccImpl(mobDaoStub, new DalServicesStub(),
        new DemandeMobiliteDaoStub(), new PartenaireDaoStub(bizzFactory),
        new PaysDaoStub(bizzFactory), new AdresseDaoStub());
  }

  @BeforeEach
  public void reset() throws Exception {
    mobDaoStub.reset();
  }

  @DisplayName("Test methode confirmerDemandeMobilite ok")
  @Test
  void confirmerDemandeMobilite1() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    assertEquals(mobEtudUcc.confirmerDemandeMobilite(mob), mob);
  }

  @DisplayName("Test methode confirmerDemandeMobilite avec partenaire existant par après")
  @Test
  void confirmerDemandeMobilite6() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.getPartenaire().setIdPartenaire(-1);
    assertEquals(mobEtudUcc.confirmerDemandeMobilite(mob), mob);
  }

  @DisplayName("Test methode confirmerDemandeMobilite avec mobilite deja existante")
  @Test
  void confirmerDemandeMobilite2() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setQuadrimestre(0);
    mobDaoStub.creerMobiliteEtudiante(mob);
    assertThrows(BizException.class, () -> mobEtudUcc.confirmerDemandeMobilite(mob));
  }

  @DisplayName("Test methode confirmerDemandeMobilite avec nouveau partenaire ok")
  @Test
  void confirmerDemandeMobilite3() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    PartenaireDto part = bizzFactory.getPartenaire();
    AdresseDto adr = bizzFactory.getAdresse();
    part.setAdresse(adr);
    part.setIdPartenaire(0);
    mob.setPartenaire(part);
    MobiliteEtudianteDto mobTest = bizzFactory.getMobiliteEtudiante();
    assertEquals(mobEtudUcc.confirmerDemandeMobilite(mob), mobTest);
  }

  @DisplayName("Test methode confirmerDemandeMobilite avec mobi deja existante")
  @Test
  void confirmerDemandeMobilite4() {
    PartenaireDto part = bizzFactory.getPartenaire();
    AdresseDto adr = bizzFactory.getAdresse();
    MobiliteEtudianteDto mobTest = bizzFactory.getMobiliteEtudiante();
    part.setAdresse(adr);
    mobTest.setPartenaire(part);

    mobDaoStub.creerMobiliteEtudiante(mobTest);
    assertThrows(BizException.class, () -> mobEtudUcc.confirmerDemandeMobilite(mobTest));
  }

  @DisplayName("Test methode confirmerDemandeMobilite ok5")
  @Test
  void confirmerDemandeMobilite5() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setQuadrimestre(1);
    mobDaoStub.creerMobiliteEtudiante(mob);
    MobiliteEtudianteDto mobTest = bizzFactory.getMobiliteEtudiante();
    assertEquals(mobEtudUcc.confirmerDemandeMobilite(mobTest), mob);
  }

  @DisplayName("Test visualiserToutesLesMobilitees ok")
  @Test
  void visualiserToutesLesMobilitees1() {
    List<MobiliteEtudianteDto> mobs = new ArrayList<MobiliteEtudianteDto>();
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    mobs.add(mob);
    assertEquals(mobEtudUcc.visualiserToutesLesMobilites(), mobs);
  }

  @DisplayName("Test visualiserToutesLesMobilitees avec pas meme liste")
  @Test
  void visualiserToutesLesMobilitees2() {
    List<MobiliteEtudianteDto> mobs = new ArrayList<MobiliteEtudianteDto>();
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    assertNotEquals(mobEtudUcc.visualiserToutesLesMobilites(), mobs);
  }



  @DisplayName("Test methode getMobilitesParEtudiant ok")
  @Test
  void getMobilitesParEtudiant1() {
    List<MobiliteEtudianteDto> mobs = new ArrayList<MobiliteEtudianteDto>();
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    PersonneDto pers = bizzFactory.getPersonne();
    mob.setPersonne(pers);

    mobDaoStub.creerMobiliteEtudiante(mob);
    mobs.add(mob);
    assertEquals(mobEtudUcc.getMobilitesParEtudiant(pers), mobs);
  }

  @DisplayName("Test methode annulerMobilite ok")
  @Test
  void annulerMobilite1() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    assertEquals(mobEtudUcc.annulerMobilite(mob), mob);
  }


  @DisplayName("Test methode indiquerEnvoiDemandeDePaiement ok")
  @Test
  void indiquerEnvoieDemandePaiement1() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    assertEquals(mobEtudUcc.indiquerEnvoieDemandePaiement(mob), mob);
  }


  @DisplayName("Test methode getPartenaireParEtudiant ok")
  @Test
  void getPartenaireParEtudiant1() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    assertEquals(mobEtudUcc.getPartenaireParEtudiant(mob), mob);
  }

  @DisplayName("Test methode getPartenaireParEtudiant avec etudiant inexistant")
  @Test
  void getPartenaireParEtudiant2() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    MobiliteEtudianteDto mobtest = bizzFactory.getMobiliteEtudiante();
    mobtest.getPersonne().setIdPersonne(-1);
    assertNull(mobEtudUcc.getPartenaireParEtudiant(mobtest));
  }

  @DisplayName("Test methode indiquerEnvoieDeuxiemeDemandePaiement ok")
  @Test
  void indiquerEnvoieDeuxiemeDemandePaiement1() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    assertEquals(mobEtudUcc.indiquerEnvoieDeuxiemeDemandePaiement(mob), mob);
  }


  @DisplayName("Test methode indiquerDonneesMobiEncodeesExternes ok")
  @Test
  void indiquerDonneesMobiEncodeesExternes1() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    assertEquals(mobEtudUcc.indiquerDonneesMobiEncodeesExternes(mob), mob);
  }


  @DisplayName("Test methode confirmerDocumentsEtudiantRemplis ok")
  @Test
  void confirmerDocumentsEtudiantRemplis1() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setCharteEtudiant(true);
    assertEquals(mobEtudUcc.confirmerDocumentsEtudiantRemplis(mob), mob);
  }

  @DisplayName("Test methode confirmerDocumentsEtudiantRemplis ok2")
  @Test
  void confirmerDocumentsEtudiantRemplis2() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setCharteEtudiant(true);
    mob.setContratBourse(true);
    mob.setPreuveTestLinguistique(true);
    mob.setConventionStageEtude(true);
    mob.setDocumentEngagement(true);
    mob.getDemande().setProgramme("Erasmus+");
    assertEquals(mobEtudUcc.confirmerDocumentsEtudiantRemplis(mob), mob);
  }

  @DisplayName("Test methode confirmerDocumentsEtudiantRemplis ok4")
  @Test
  void confirmerDocumentsEtudiantRemplis4() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setCharteEtudiant(true);
    mob.setContratBourse(true);
    mob.setPreuveTestLinguistique(true);
    mob.setConventionStageEtude(false);
    mob.setDocumentEngagement(true);
    mob.getDemande().setProgramme("Erasmus+");
    assertEquals(mobEtudUcc.confirmerDocumentsEtudiantRemplis(mob), mob);
  }


  @DisplayName("Test methode confirmerDocumentsEtudiantRemplis ok3")
  @Test
  void confirmerDocumentsEtudiantRemplis3() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setCharteEtudiant(true);
    assertEquals(mobEtudUcc.confirmerDocumentsEtudiantRemplis(mob), mob);
  }

  @DisplayName("Test methode confirmerDocumentsEtudiantRemplis ok5")
  @Test
  void confirmerDocumentsEtudiantRemplis5() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setCharteEtudiant(true);
    mob.setDocumentEngagement(true);
    mob.setConventionStageEtude(true);
    mob.getDemande().getPays().setCodeIso("CHE");
    assertEquals(mobEtudUcc.confirmerDocumentsEtudiantRemplis(mob), mob);
  }

  @DisplayName("Test methode confirmerDocumentsEtudiantRemplis ok6")
  @Test
  void confirmerDocumentsEtudiantRemplis6() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setContratBourse(true);
    mob.getDemande().getPays().setCodeIso("CHE");
    assertThrows(BizException.class, () -> mobEtudUcc.confirmerDocumentsEtudiantRemplis(mob));
  }



  @DisplayName("Test methode preciserDocumentsEtudiantRetour ok")
  @Test
  void preciserDocumentsEtudiantRetour1() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    assertEquals(mobEtudUcc.preciserDocumentsEtudiantRetour(mob), mob);
  }

  @DisplayName("Test methode preciserDocumentsEtudiantRetour ok2")
  @Test
  void preciserDocumentsEtudiantRetour2() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setAttestationSejourRecu(true);
    mob.setReleveNoteRecu(true);
    mob.setRapportFinal(true);
    mob.setPreuveTestLinguistiqueRetour(true);
    mob.getDemande().setProgramme("Erasmus+");
    assertEquals(mobEtudUcc.preciserDocumentsEtudiantRetour(mob), mob);
  }

  @DisplayName("Test methode preciserDocumentsEtudiantRetour ok3")
  @Test
  void preciserDocumentsEtudiantRetour3() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setAttestationSejourRecu(true);
    mob.setReleveNoteRecu(true);
    mob.setRapportFinal(false);
    mob.getDemande().setProgramme("Erasmus+");
    assertEquals(mobEtudUcc.preciserDocumentsEtudiantRetour(mob), mob);
  }

  @DisplayName("Test methode preciserDocumentsEtudiantRetour pas ok")
  @Test
  void preciserDocumentsEtudiantRetour4() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setPreuveTestLinguistiqueRetour(true);
    assertThrows(BizException.class, () -> mobEtudUcc.preciserDocumentsEtudiantRetour(mob));
  }

  @DisplayName("Test methode preciserDocumentsEtudiantRetour pas ok2")
  @Test
  void preciserDocumentsEtudiantRetour5() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setReleveNoteRecu(true);
    mob.setCertificatStageRecu(true);
    assertThrows(BizException.class, () -> mobEtudUcc.preciserDocumentsEtudiantRetour(mob));
  }

  @DisplayName("Test methode indiquerMobiliteEnRemboursement ok")
  @Test
  void indiquerMobiliteEnRemboursement1() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    assertEquals(mobEtudUcc.indiquerMobiliteEnRemboursement(mob), mob);
  }

  @DisplayName("Test methode getMobilitesNonAnnulees pas ok")
  @Test
  void getMobilitesNonAnnulees1() {
    List<MobiliteEtudianteDto> mobs = new ArrayList<MobiliteEtudianteDto>();
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setEtat("refuse");
    mobDaoStub.creerMobiliteEtudiante(mob);
    mobs.add(mob);
    assertNotEquals(mobEtudUcc.getMobilitesNonAnnulees(), mobs);
  }

  @DisplayName("Test methode getMobilitesNonAnnulees ok")
  @Test
  void getMobilitesNonAnnulees2() {
    List<MobiliteEtudianteDto> mobs = new ArrayList<MobiliteEtudianteDto>();
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    mobs.add(mob);
    assertEquals(mobEtudUcc.getMobilitesNonAnnulees(), mobs);
  }

  @DisplayName("Test methode getListePaiements ok")
  @Test
  void getListePaiements1() {
    List<MobiliteEtudianteDto> mobs = new ArrayList<MobiliteEtudianteDto>();
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mob.setEnvoieDemandePaiement1(true);
    mobDaoStub.creerMobiliteEtudiante(mob);
    mobs.add(mob);
    assertEquals(mobEtudUcc.getListePaiements(mob), mobs);
  }

  @DisplayName("Test methode rechercheMobi annee academique ok")
  @Test
  void rechercheMobi1() {
    List<MobiliteEtudianteDto> mobs = new ArrayList<MobiliteEtudianteDto>();
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    mob.setEtat("");
    mobs.add(mob);
    assertEquals(mobEtudUcc.rechercheMobi(mob), mobs);
  }

  @DisplayName("Test methode rechercheMobi etat ok")
  @Test
  void rechercheMobi2() {
    List<MobiliteEtudianteDto> mobs = new ArrayList<MobiliteEtudianteDto>();
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    mob.getDemande().setAnneeAcademique(0);
    mobs.add(mob);
    assertEquals(mobEtudUcc.rechercheMobi(mob), mobs);
  }

  @DisplayName("Test methode rechercheMobi annee academique + etat ok")
  @Test
  void rechercheMobi3() {
    List<MobiliteEtudianteDto> mobs = new ArrayList<MobiliteEtudianteDto>();
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    mobs.add(mob);
    assertEquals(mobEtudUcc.rechercheMobi(mob), mobs);
  }

  @DisplayName("Test methode rechercheMobi annee academique + etat vide")
  @Test
  void rechercheMobi4() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    mob.getDemande().setAnneeAcademique(0);
    mob.setEtat("");
    assertThrows(BizException.class, () -> mobEtudUcc.rechercheMobi(mob));
  }

  @DisplayName("Test methode preciserEnvoyerDemandeHe ok")
  @Test
  void preciserEnvoyerDemandeHe() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    assertEquals(mobEtudUcc.preciserEnvoyerdemandeHe(mob), mob);
  }

  @DisplayName("Test methode getMobiParId ok")
  @Test
  void getMobiParId() {
    MobiliteEtudianteDto mob = bizzFactory.getMobiliteEtudiante();
    mobDaoStub.creerMobiliteEtudiante(mob);
    assertEquals(mobEtudUcc.getMobiParId(mob), mob);
  }
}
