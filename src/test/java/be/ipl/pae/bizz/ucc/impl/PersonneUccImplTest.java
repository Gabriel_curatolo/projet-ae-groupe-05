package be.ipl.pae.bizz.ucc.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import be.ipl.pae.bizz.dto.PartenaireDto;
import be.ipl.pae.bizz.dto.PersonneDto;
import be.ipl.pae.bizz.factory.BizzFactory;
import be.ipl.pae.bizz.factory.BizzFactoryStub;
import be.ipl.pae.bizz.ucc.interfaces.PersonneUcc;
import be.ipl.pae.dal.dao.stub.AdresseDaoStub;
import be.ipl.pae.dal.dao.stub.PartenaireDaoStub;
import be.ipl.pae.dal.dao.stub.PaysDaoStub;
import be.ipl.pae.dal.dao.stub.PersonneDaoStub;
import be.ipl.pae.dal.services.DalServicesStub;
import be.ipl.pae.exceptions.BizException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
// import org.junit.Test;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class PersonneUccImplTest {

  private static BizzFactory bizzFactory;
  private static PersonneUcc persUcc;
  private static PersonneDaoStub persDao;
  private static PartenaireDaoStub partDaoStub;

  @BeforeAll
  static void setup() throws Exception {
    bizzFactory = new BizzFactoryStub();
    persDao = new PersonneDaoStub();
    partDaoStub = new PartenaireDaoStub(bizzFactory);
    persUcc = new PersonneUccImpl(persDao, new DalServicesStub(), new AdresseDaoStub(),
        new PaysDaoStub(bizzFactory), partDaoStub);

  }

  @BeforeEach
  public void reset() throws Exception {
    persDao.reset();
    partDaoStub.reset();
  }


  @DisplayName("Test methode checkConnexion ok")
  @Test
  void checkConnexion1() {
    PersonneDto persDto = bizzFactory.getPersonne();
    persUcc.inscription(persDto);
    PersonneDto persDto1 = bizzFactory.getPersonne();
    assertEquals(persUcc.checkConnexion(persDto1), persDto);
  }


  @DisplayName("Test methode checkConnexion mauvais pseudo")
  @Test
  void checkConnexion2() {
    PersonneDto persDto = bizzFactory.getPersonne();
    persUcc.inscription(persDto);
    PersonneDto persDto1 = bizzFactory.getPersonne();
    persDto1.setPseudo("aucun");
    assertThrows(BizException.class, () -> persUcc.checkConnexion(persDto1));
  }

  @DisplayName("Test methode checkConnexion pseudo.length==0")
  @Test
  void checkConnexion3() {
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setPseudo("");
    persDto.setMotDePasse("123");
    assertThrows(BizException.class, () -> persUcc.checkConnexion(persDto));
  }


  @DisplayName("Test methode checkConnexion mdp==chaine vide")
  @Test
  void checkConnexion4() {
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setPseudo("JeanFouFou");
    persDto.setMotDePasse("");
    assertThrows(BizException.class, () -> persUcc.checkConnexion(persDto));
  }


  @DisplayName("Test methode checkConnexion mauvais mdp")
  @Test
  void checkConnexion5() {
    PersonneDto persDto = bizzFactory.getPersonne();
    persDto.setMotDePasse("111");
    assertThrows(BizException.class, () -> persUcc.checkConnexion(persDto));
  }



  @DisplayName("Test methode inscription ok")
  @Test
  void checkInscription1() {
    PersonneDto pers = bizzFactory.getPersonne();
    assertEquals(persUcc.inscription(pers), pers);
  }

  @DisplayName("Test methode inscription incorrect existant")
  @Test
  void checkInscription2() {
    PersonneDto pers = bizzFactory.getPersonne();
    persUcc.inscription(pers);
    assertThrows(BizException.class, () -> persUcc.inscription(pers));
  }


  @DisplayName("Test methode encodageDonnees ok")
  @Test
  void encodageDonnees1() {
    PersonneDto pers = bizzFactory.getPersonne();
    persUcc.inscription(pers);
    assertEquals(persUcc.encodageDonnees(pers), pers);
  }

  @DisplayName("Test methode visualiserToutLesUtilisateurs ok")
  @Test
  void visualiserTousLesUtilisateurs1() {
    List<PersonneDto> list = new ArrayList<PersonneDto>();
    PersonneDto pers = bizzFactory.getPersonne();
    persUcc.inscription(pers);
    list.add(pers);
    assertEquals(persUcc.visualiserTousLesUtilisateurs(), list);
  }

  @DisplayName("Test methode visualiserToutLesUtilisateurs pas bonne liste")
  @Test
  void visualiserTousLesUtilisateurs2() {
    List<PersonneDto> list = new ArrayList<PersonneDto>();
    PersonneDto pers = bizzFactory.getPersonne();
    persUcc.inscription(pers);
    assertNotEquals(persUcc.visualiserTousLesUtilisateurs(), list);
  }

  @DisplayName("Test methode visualiser un utilisateur")
  @Test
  void visualiserUnUtilisateur1() {
    PersonneDto pers = bizzFactory.getPersonne();
    persUcc.inscription(pers);
    assertEquals(persUcc.visualiserUnUtilisateur(pers), pers);
  }


  @DisplayName("Test methode getPersonneById")
  @Test
  void getPersonneById1() {
    PersonneDto pers = bizzFactory.getPersonne();
    persUcc.inscription(pers);
    assertEquals(persUcc.getPersonneById(pers), pers);
  }

  @DisplayName("Test methode augmenter prof")
  @Test
  void augmenterProf1() {
    PersonneDto pers = bizzFactory.getPersonne();
    pers.setRole("et");
    persUcc.inscription(pers);
    PersonneDto persTest = bizzFactory.getPersonne();
    persTest.setRole("p");
    assertEquals(persUcc.augmenterProfesseur(pers), persTest);
  }

  @DisplayName("Test methode augmenter prof avec deja prof")
  @Test
  void augmenterProf2() {
    PersonneDto pers = bizzFactory.getPersonne();
    persUcc.inscription(pers);
    pers.setRole("p");
    assertThrows(BizException.class, () -> persUcc.augmenterProfesseur(pers));
  }

  @DisplayName("Test methode augmenter prof avec pers prof responsable")
  @Test
  void augmenterProf3() {
    PersonneDto pers = bizzFactory.getPersonne();
    persUcc.inscription(pers);
    pers.setRole("pr");
    assertThrows(BizException.class, () -> persUcc.augmenterProfesseur(pers));
  }

  @DisplayName("Test methode getDonnees ok")
  @Test
  void getDonnees1() {
    PersonneDto pers = bizzFactory.getPersonne();
    persUcc.inscription(pers);
    assertEquals(persUcc.getDonnees(pers), pers);
  }

  @DisplayName("Test methode getDonnees")
  @Test
  void getDonnees2() {
    PersonneDto pers = bizzFactory.getPersonne();
    pers.getAdresse().setIdAdresse(0);
    persUcc.inscription(pers);
    assertNull(persUcc.getDonnees(pers));
  }

  @DisplayName("Test methode recherchePartenaire ville ok")
  @Test
  void recherchePartenaire1() {
    PartenaireDto part = bizzFactory.getPartenaire();
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    partDaoStub.introduirePartenaire(part);
    listPart.add(part);
    part.setNomComplet("");
    part.getPays().setNom("");
    assertEquals(persUcc.recherchePartenaires(part), listPart);
  }

  @DisplayName("Test methode recherchePartenaire nomPays ok")
  @Test
  void recherchePartenaire2() {
    PartenaireDto part = bizzFactory.getPartenaire();
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    partDaoStub.introduirePartenaire(part);
    listPart.add(part);
    part.setNomComplet("");
    part.getAdresse().setVille("");
    assertEquals(persUcc.recherchePartenaires(part), listPart);
  }

  @DisplayName("Test methode recherchePartenaire nom ok")
  @Test
  void recherchePartenaire3() {
    PartenaireDto part = bizzFactory.getPartenaire();
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    partDaoStub.introduirePartenaire(part);
    listPart.add(part);
    part.getAdresse().setVille("");
    part.getPays().setNom("");
    assertEquals(persUcc.recherchePartenaires(part), listPart);
  }

  @DisplayName("Test methode recherchePartenaire ville et pays ok")
  @Test
  void recherchePartenaire4() {
    PartenaireDto part = bizzFactory.getPartenaire();
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    partDaoStub.introduirePartenaire(part);
    listPart.add(part);
    part.setNomComplet("");
    assertEquals(persUcc.recherchePartenaires(part), listPart);
  }

  @DisplayName("Test methode recherchePartenaire ville et nom ok")
  @Test
  void recherchePartenaire5() {
    PartenaireDto part = bizzFactory.getPartenaire();
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    partDaoStub.introduirePartenaire(part);
    listPart.add(part);
    part.getPays().setNom("");
    assertEquals(persUcc.recherchePartenaires(part), listPart);
  }

  @DisplayName("Test methode recherchePartenaire nom et pays ok")
  @Test
  void recherchePartenaire6() {
    PartenaireDto part = bizzFactory.getPartenaire();
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    partDaoStub.introduirePartenaire(part);
    listPart.add(part);
    part.getAdresse().setVille("");
    assertEquals(persUcc.recherchePartenaires(part), listPart);
  }

  @DisplayName("Test methode recherchePartenaire ville, nom et pays ok")
  @Test
  void recherchePartenaire7() {
    PartenaireDto part = bizzFactory.getPartenaire();
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    partDaoStub.introduirePartenaire(part);
    listPart.add(part);
    assertEquals(persUcc.recherchePartenaires(part), listPart);
  }

  @DisplayName("Test methode recherchePartenaire ville, nom et pays vides")
  @Test
  void recherchePartenaire8() {
    PartenaireDto part = bizzFactory.getPartenaire();
    partDaoStub.introduirePartenaire(part);
    part.getAdresse().setVille("");
    part.getPays().setNom("");
    part.setNomComplet("");
    assertThrows(BizException.class, () -> persUcc.recherchePartenaires(part));
  }

  @DisplayName("Test methode recherchePersonne nom ok")
  @Test
  void recherchePersonne1() {
    PersonneDto pers = bizzFactory.getPersonne();
    List<PersonneDto> listPers = new ArrayList<PersonneDto>();
    persDao.inscription(pers);
    listPers.add(pers);
    pers.setPrenom("");
    assertEquals(persUcc.recherchePersonnes(pers), listPers);
  }

  @DisplayName("Test methode recherchePersonne prenom ok")
  @Test
  void recherchePersonne2() {
    PersonneDto pers = bizzFactory.getPersonne();
    List<PersonneDto> listPers = new ArrayList<PersonneDto>();
    persDao.inscription(pers);
    listPers.add(pers);
    pers.setNom("");
    assertEquals(persUcc.recherchePersonnes(pers), listPers);
  }

  @DisplayName("Test methode recherchePersonne nom et prenom ok")
  @Test
  void recherchePersonne3() {
    PersonneDto pers = bizzFactory.getPersonne();
    List<PersonneDto> listPers = new ArrayList<PersonneDto>();
    persDao.inscription(pers);
    listPers.add(pers);
    assertEquals(persUcc.recherchePersonnes(pers), listPers);
  }

  @DisplayName("Test methode recherchePersonne nom et prenom vides")
  @Test
  void recherchePersonne4() {
    PersonneDto pers = bizzFactory.getPersonne();
    persDao.inscription(pers);
    pers.setNom("");
    pers.setPrenom("");
    assertThrows(BizException.class, () -> persUcc.recherchePersonnes(pers));
  }


  @DisplayName("Test methode completionNomPrenomEtudiantRecherche  ok")
  @Test
  void completionNomPrenomEtudiantRecherche() {
    PersonneDto pers = bizzFactory.getPersonne();
    List<PersonneDto> listPers = new ArrayList<PersonneDto>();
    persDao.inscription(pers);
    listPers.add(pers);
    assertEquals(persUcc.completionNomPrenomEtudiantRecherche(), listPers);
  }

  @DisplayName("Test methode completionNomPartenairePaysVilleEtudiantRecherche  ok")
  @Test
  void completionNomPartenairePaysVilleEtudiantRecherche() {
    PartenaireDto part = bizzFactory.getPartenaire();
    List<PartenaireDto> listPart = new ArrayList<PartenaireDto>();
    partDaoStub.introduirePartenaire(part);
    listPart.add(part);
    assertEquals(persUcc.completionNomPartenairePaysVilleEtudiantRecherche(), listPart);
  }
}


