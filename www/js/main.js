$(function(){ 
	//refresh();
	
	$('#bouton_deconnexion').on('click',function(){             
		deconnexion();   
	});    

	/*$('#bouton_page_professeur').on('click',function(){
		//console.log($('#bouton_page_professeur').val());               
		refresh($('#bouton_page_professeur').val());   
	}); 
	$('#bouton_page_etudiant').on('click',function(){
		//console.log($('#bouton_page_etudiant').val());  
		refresh($('#bouton_page_etudiant').val());
	})   
	$("#bouton_page_responsable").on('click',function(){
		//console.log($('#bouton_page_responsable').val());  
		refresh($('#bouton_page_responsable').val());
	})*/
	//vue recherche
	$('#bouton_page_professeur').on('click',function(){
		
		professeurVue(); 
	}); 
	$('#bouton_page_etudiant').on('click',function(){
		
		etudiantVue();
	})   
	$("#bouton_page_responsable").on('click',function(){
		
		profRespVue();
	})
	$("#bouton_page_recherche").on('click',function(){
		rechercheVue();
	})
	$("#bouton_page_infos").on('click',function(){
		console.log("bouton page infos cliqué");
		infosVue();
	})
	
	$.ajax({        
		url:'http://localhost:8680/',       
		type:'POST',         
		data:{ action:"refresh"},       
		success: function(resp){
			if(resp.role=="et"){
				FrontEnd.displayPage("etudiant");
			}else if(resp.role=="p"){
				FrontEnd.displayPage("professeur");
			}else if(resp.role=="pr"){
				FrontEnd.displayPage("professeur-responsable");
			}
			FrontEnd.displayPage("etudiant");
		},            
		error:function(e){           
			FrontEnd.displayPage("connexion-inscription");  
		}    
	});

})

/*function refresh(page){

	$.ajax({         
		url:'http://localhost:8680/api',
		type:'POST',     
		data:{action:"refresh"},   
		success: function(resp){  
			console.log(JSON.parse(resp));
			//let re=JSON.parse(resp);
			//console.log(re);
			console.log("page:"+page);
			if(resp.role=="et"){
				FrontEnd.displayPage("etudiant");
			}else if(resp.role=="p"){
				if(page=="undefined"){
					FrontEnd.displayPage("professeur");
				}else if(page!="professeur-responsable"){
					FrontEnd.displayPage(page);
				}
			}else if(resp.role=="pr"){
				if(page!=null){
					Frontend.displayPage(page);
					
				}else{
					Frontend.displayPage("professeur-responsable");
				}
			}
			FrontEnd.displayPage("etudiant");		

		},error:function(e){         
			FrontEnd.displayPage("connexion-inscription");    
		}    
	});
}*/

function deconnexion(){    
	$.ajax({         
		url:'http://localhost:8680/api',        
		type:'POST',     
		data:{ action:"disconnect"},   
		success: function(resp){          
			console.log("deconnexion a reussie");     
			FrontEnd.displayPage("connexion-inscription");    
		},error:function(e){         
			console.log("failure deconnexions");    
		}    
	});     
}

function professeurVue(){    
	$.ajax({         
		url:'http://localhost:8680/api',        
		type:'POST',     
		data:{ action:"professeur"},   
		success: function(resp){ 
			var json = JSON.parse(resp);
			console.log("on veut la vue d un professeur ")
			if(json.role === 'et'){
				$('#alertAccesPage').text("Désolé, seul un professeur peut accéder à cet onglet !");
				console.log("un etudiant veut se connecter en tant que prof => IL NE  PEUT PAS ");
			}else{
				
				console.log("un professeur veut allez sur sa page => AJAX");     
				FrontEnd.displayPage("professeur");    
			}
		},
		error:function(e){         
			console.log("failure voir vue professeur");    
		}    
	});     
}

function etudiantVue(){
	$.ajax({         
		url:'http://localhost:8680/api',        
		type:'POST',   
		data:{action:"etudiant"},  
		success: function(resp){  
			$('#alertAccesPage').text("");
			FrontEnd.displayPage("etudiant");     
		},
		error:function(e){         
			console.log("failure voir vue professeur");    
		}    
	});
}

function rechercheVue(){
	$.ajax({         
		url:'http://localhost:8680/api',        
		type:'POST',   
		data:{action:"recherche"},  
		success: function(resp){
			$('#alertAccesPage').text("");
			FrontEnd.displayPage("page_recherche");     
		},
		error:function(e){       
			console.log("failure chargement page recherche");    
		}    
	});
}

function profRespVue(){
	$.ajax({         
		url:'http://localhost:8680/api',        
		type:'POST',     
		data:{ action:"prof_responsable"},   
		success: function(resp){ 
			var json = JSON.parse(resp);
			if(json.role==='et'){
				$('#alertAccesPage').text("Désolé, seul un professeur peut accéder à cet onglet !");
				console.log("etudiant veut acceder page");
			}
			if(json.role==='p'){
				$('#alertAccesPage').text("Désolé, seul un professeur responsable peut accéder à cet onglet !");
				console.log("prof veut acceder page");
			}

			if(json.role==='pr'){
				FrontEnd.displayPage("professeur-responsable");
			}
		},
		error:function(e){         
			console.log("failure voir vue professeurResp");    
		}    
	});
}

function infosVue(){
	console.log("on rentre dans la page infos");
	$.ajax({         
		url:'http://localhost:8680/api',        
		type:'POST',   
		data:{action:"mes_infos"},  
		success: function(resp){
			$('#alertAccesPage').text("");
			FrontEnd.displayPage("page_infos");     
		},
		error:function(e){       
			console.log("failure chargement page infos");    
		}    
	});
}