FrontEnd.onInitPage("page_infos",function(){
	
	
	/*
	------------------------------------------------------------------------------------------------------------------------------
	<---------------------Partie JQUERY pour appeler les fonctions contenant des requetes AJAX---------------------------------->
	------------------------------------------------------------------------------------------------------------------------------
	*/

	//cache la table des mobilitées lors de l'affichage de la page
	$("#tableMob").hide();

	//voir la table des mobilitées de la personne connéctée
	$('#voirMobTable').on('click','button',function(event){
		const buttClick=$(this);
		voirUneMobilite(buttClick);
		$('#voirUneMobi').show();	
	})

	//bouton qui permet de changer ses informations
	$('#button_encodage_donnees').on('click', function(){
		if($('#civilite_encoderDonnee').val()=="" || $('#sexe_encoderDonnee').val()=="" || $('#departement_encoderDonnee').val()==""|| 
			$('#dateNaissance_encoderDonnee').val()=="" || $('#nationalite_encoderDonnee').val()=="" 
			|| $('#rue_encoderDonnee').val()=="" || $('#numero_encoderDonnee').val()==""|| $('#codePostal_encoderDonnee').val()==""
			|| $('#telephone_encoderDonnee').val()==""
			||$('#ville_encoderDonnee').val()=="" || $('#email_encoderDonnee').val()==""
			|| $('#nbAnneesReussies_encoderDonnee').val()=="" || $('#noCompte_encoderDonnee').val()=="" || $('#titulaireCompte_encoderDonnee').val()==""
			|| $('#banque_encoderDonnee').val()=="" || $('#codeBic_encoderDonnee').val()==""){
			$('#alertEncodageDonnees').text("Vous devez remplir tout les champs");
		} else {
			$('#alertEncodageDonnees').text("");
			encoderDonnees();
		}

	});

	//bouton qui fait apparaitre les informations de l'utilisateur connécté pour qu'elles puissent être changées
	$('#button_changer_donnees').on('click', function(){
		$('#voirSesDonnees').hide();
		$('#encoderSesDonnees').show();
	});

	/*
		-----------------------------------------------------------------------------------
		<------------Chargement des données utiles au chargement de la page--------------->
		-----------------------------------------------------------------------------------
	*/
	FrontEnd.onDisplayPage("page_infos", function() {
		voirSesMobilites();
		$("#page-header").show();
		$("#footer").show();
		$("#etudiant").hide();
		$("#connexion-inscription").hide();
		$('#inscription').hide();
		$("#professeur").hide();
		$('#page_recherche').hide();
		$('#voirUneMobi').hide();
		$('#estEncodeEtAMobiConfirmeView').show();
		$("#tableMob").hide();
		estEncode();
	})  

	/*
		-----------------------------------------------------------------------------------
		<------------Vidage des tableaux et des données au changement de page------------->
		-----------------------------------------------------------------------------------
	*/
	FrontEnd.onLeavePage("page_infos",function(){
		$("#page_infos").hide();
		$("#voirMobTable").empty();

	})

});



/*
	-------------------------------------------------------------------------------------------------------------------------------------
	<----------------------------------------------------------------APPELS AJAX ------------------------------------------------------->
	-------------------------------------------------------------------------------------------------------------------------------------
*/

/*-----------------------------------------------------------------
AJAX qui renvoie un tableau des mobilitées de la personne connectée
------------------------------------------------------------------*/
function voirSesMobilites(){
		
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"get_mobilite_etudiant_connecte"},
		success:function(resp){
		console.log(resp);
			for(let i=0;i<resp.length;i++){
				
				$("#voirMobTable").append(
					'<tr id="g'+i+'">' + 
					'<td>' + resp[i].demande.pays.nom + '</td>' +
					'<td>' + resp[i].demande.anneeAcademique + '</td>' +
					'<td>' + resp[i].demande.programme + '</td>'+
					'<td>' + resp[i].partenaire.nomComplet + '</td>' +
					'<td>' + resp[i].demande.quadrimestre + '</td>'+
					'<td>' + resp[i].etat + '</td>' +
					'<td>' + '<button class="btn btn-primary" value='+i+'>Voir</button>' + '</td>'+
					'</tr>'
					);
			}
		},
		error:function(e){
		}
	});
}

/*-----------------------------------------------------------------------------
AJAX qui renvoie les informations concernant la mobilité en cours de l'étudiant
-------------------------------------------------------------------------------*/
function voirUneMobilite(bouton){
	let numLigne=bouton.val();

	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"get_mobilite_etudiant_connecte"},
		success:function(resp){
			
			$("#etatDeLaMobChoisie").text("État de la mobilité : " + resp[numLigne].etat);
			if(resp[numLigne].contratBourse===true){
				$('#bourseVision').prop('checked',true);
			}
			if(resp[numLigne].preuveTestLinguistique===true){
				$('#preuve_passageVision').prop('checked',true);
			}
			if(resp[numLigne].documentEngagement===true){
				$('#doc_engagementVision').prop('checked',true);
			}
			if(resp[numLigne].charteEtudiant===true){
				$('#charteVision').prop('checked',true);
			}
			if(resp[numLigne].conventionStageEtude===true){
				$('#conventionVision').prop('checked',true);
			}
			if(resp[numLigne].attestationSejourRecu===true){
				$('#attSejourVision').prop('checked',true);
			}
			if(resp[numLigne].releveNoteRecu===true){
				$('#relNotesVision').prop('checked',true);
			}
			if(resp[numLigne].certificatStageRecu===true){
				$('#certifStageVision').prop('checked',true);
			}
			if(resp[numLigne].preuveTestLinguistiqueRetour===true){
				$('#preuve_passageApMobVision').prop('checked',true);
			}
			if(resp[numLigne].rapportFinal===true){
				$('#rapFinalVision').prop('checked',true);
			}
			if(resp[numLigne].raisonsAnnulationRefus != null){
				$('#annuleVision').prop('checked',true);
				$("#raisonsAnnulationVision").text(resp[numLigne].raisonsAnnulationRefus);
			}
		},
		error:function(e){
		}
	});
}

/*-------------------------------------------------------------------------
AJAX qui indique si les informations de l'utilisateur ont déjà été encodées
---------------------------------------------------------------------------*/
function estEncode(){

	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"est_encode_personne"},
		success:function(resp){
				if(resp==null){
					console.log("Pas de données encodées");
					$('#voirSesDonnes').hide();
					$('#encoderSesDonnees').show();
				} else {
					console.log("l'étudiant a encodé ses données");
					$("#voir_civilite_encoderDonnee").attr("value",resp.civilite);
					$("#voir_sexe_encoderDonnee").attr("value",resp.sexe);
					$("#voir_dateNaissance_encoderDonnee").attr("value",resp.dateNaissance);
					$("#voir_departement_encoderDonnee").attr("value",resp.departement);
					$("#voir_nationalite_encoderDonnee").attr("value",resp.nationalite.nom);
					$("#voir_rue_encoderDonnee").attr("value",resp.adresse.rue);
					$("#voir_numero_encoderDonnee").attr("value",resp.adresse.numero);
					$("#voir_boite_encoderDonnee").attr("value",resp.adresse.boite);
					$("#voir_codePostal_encoderDonnee").attr("value",resp.adresse.codePostal);
					$("#voir_ville_encoderDonnee").attr("value",resp.adresse.ville);
					$("#voir_region_encoderDonnee").attr("value",resp.adresse.region);
					$("#voir_commune_encoderDonnee").attr("value",resp.adresse.commune);
					$("#voir_telephone_encoderDonnee").attr("value",resp.telephone);
					$("#voir_nbAnneesReussies_encoderDonnee").attr("value",resp.nbAnneeReussies);
					$("#voir_noCompte_encoderDonnee").attr("value",resp.noCompte);
					$("#voir_titulaireCompte_encoderDonnee").attr("value",resp.titulaireCompte);
					$("#voir_banque_encoderDonnee").attr("value",resp.banque);
					$("#voir_codeBic_encoderDonnee").attr("value",resp.codeBic);

					$('#encoderSesDonnees').hide();
					$('#voirSesDonnees').show();
				}
		},
		error:function(e){
			console.log("erreur estEncode()");
			console.log(e);
		}
	});
}

/*---------------------------------------------------
AJAX qui permet à l'utilisateur d'encoder ses données
Va d'abord chercher le numVersion, pour ensuite l'envoyer dans les donnees encodees
-----------------------------------------------------*/
function encoderDonnees(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{ action:"get_personne"},
		success: function(resp){
			
			//2eme appel ajax
			$.ajax({
				url:'http://localhost:8680/api',
				type:'POST',
				data:{ action:"encodage_donnees",
				civilite:$('#civilite_encoderDonnee').val(),
				sexe:$('#sexe_encoderDonnee').val(),
				date_naissance:$('#dateNaissance_encoderDonnee').val(),
				nationalite:$('#nationalite_encoderDonnee').val(),
				departement:$('#departement_encoderDonnee').val(),
				rue:$('#rue_encoderDonnee').val(),
				no:$('#numero_encoderDonnee').val(),
				boite:$('#boite_encoderDonnee').val(),
				code_postal:$('#codePostal_encoderDonnee').val(),
				commune:$('#commune_encoderDonnee').val(),
				telephone:$('#telephone_encoderDonnee').val(),
				region:$('#region_encoderDonnee').val(),
				ville:$('#ville_encoderDonnee').val(),
				email:$('#email_encoderDonnee').val(),
				nb_annees_reussies:$('#nbAnneesReussies_encoderDonnee').val(),
				no_compte:$('#noCompte_encoderDonnee').val(),
				titulaire_compte:$('#titulaireCompte_encoderDonnee').val(),
				banque:$('#banque_encoderDonnee').val(),
				code_bic:$('#codeBic_encoderDonnee').val(),
				num_version_personne:resp.numVersion
			},
			success: function(resp2){
				$('#alertEncodageDonnees').text("Données encodées");
			},	
			error:function(e){
				console.log("failure encodage de donnees");
				$('#alertEncodageDonnees').text(e.responseText);
			}
			});	
		},	
		error:function(e){
			console.log("echec get numVersion");
		}
	});
}
