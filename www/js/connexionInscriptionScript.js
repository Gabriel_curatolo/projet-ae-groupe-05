FrontEnd.onInitPage("connexion-inscription",function(){
	
	/*---------------------------------------------------------------------------------------------------------------------------
	<---------------------Partie JQUERY pour appeler les fonctions contenant des requetes AJAX----------------------------------->
	----------------------------------------------------------------------------------------------------------------------------*/

	//BOUTON DE CONNEXION POUR CONNECTER UN UTILISATEUR À l'APPLICATION
	//champ pseudo et mdp doivent être remplis
	$('#bouton_connexion').on('click',function(){
		if ($("#pseudo").val() == "" && $("#mdp").val() != "") {
			$('#alertCon').text("Vous devez indiquer un pseudo");
		} else {
			if($("#pseudo").val() != "" && $("#mdp").val() == ""){
				$('#alertCon').text("Vous devez indiquer votre mot de passe");
			} else {
				if($("#pseudo").val() == "" || $("#mdp").val() == ""){
					$('#alertCon').text("Les 2 champs doivent etre remplis");
				} else {
					connexion();
				}
			}
		}
	});

	//BOUTON D'INSCRIPTION POUR UN UTILISATEUR
	//les champs pseudo,nom,prenom,email et mdp doivent être remplis
	$('#bouton_inscription').on('click',function(){
		if($("#pseudo_i").val()==""){
			$('#alertInsc').text("Vous devez indiquer un peudo");
		} else if($("#nom").val()=="" || !validationLetters($("#pseudo_i").val())){
			$('#alertInsc').text("Vous devez indiquer un nom valide");
		} else if($("#prenom").val()=="" || !validationLetters($("#pseudo_i").val())){
			$('#alertInsc').text("Vous devez indiquer un prenom valide");
		} else if($("#email").val()=="" || !validationMail($("#email").val())){
			$('#alertInsc').text("Vous devez indiquer un email correct");
		} else if($("#mdp_i").val()==""){
			$('#alertInsc').text("Vous devez indiquer un mot de passe");
		} else {
			inscription();
		}
	});


	/*---------------------------------------------------------------------------------
	<------------Chargement des données utiles au chargement de la page--------------->
	---------------------------------------------------------------------------------*/
	FrontEnd.onDisplayPage("connexion-inscription", function() {
		$("#connexion-inscription").show();
		$("#inscription").show();
		$("#page-header").hide();
		$("#footer").hide();
		$("#etudiant").hide();
		$("#professeur").hide();
		$('#page_recherche').hide();
		$("#professeur-responsable").hide();
		$("#page_infos").hide();

	})
	
	/*-------------------------------------------------------------------------------------------------------
	<-----------------------Cache la vue connexion-inscription lorsqu'on quitte la page--------------------->
	--------------------------------------------------------------------------------------------------------*/
	FrontEnd.onLeavePage("connexion-inscription", function() {
		$("#connexion-inscription").hide();
	})
});



/*-------------------------------------------------------------------------------------------------------------------------------------
<----------------------------------------------------------------APPELS AJAX ------------------------------------------------------->
-------------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------
AJAX pour connecter un utilisateur à l'application
-------------------------------------------------*/
function connexion(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		data:{action:"connect",pseudo:$('#pseudo').val(),mdp:$('#mdp').val()},
		success:function(resp){
			let role = JSON.parse(resp).role;
			if(role=="et"){
				FrontEnd.displayPage("etudiant");
			}
			if(role=="pr"||role=="p"){
				FrontEnd.displayPage("professeur");
			}
		},
		error:function(e){
			$('#alertCon').text("Pseudo et/ou mot de passe incorrect");
		}
	});
}

/*------------------------------------------------
AJAX pour inscrire une personne dans l'application
-> 1ere personne inscrite sera un prof_resposable
--------------------------------------------------*/
function inscription(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		data:{action:"inscription",
			pseudo_i:$('#pseudo_i').val(),
			nom:$('#nom').val(), 
			prenom:$('#prenom').val(),
			email:$('#email').val(),
			mdp_i:$('#mdp_i').val()
			},
		success:function(resp,status){
			$('#alertInsc').text("L'inscription de "+$('#prenom').val()+" "+$('#nom').val()+" a réussi.");
		},
		error:function(e){
			$('#alertInsc').text(e.responseText);
		}
	});
}

//METHODE POUR VALIDER L'EMAIL SELON LE BON FORMAT LORS DE L'INSCRIPTION
function validationMail (email) {
	return /\S+@\S+\.\S+/.test(email)
}

//METHODE POUR VALIDER LETTRE POUR PRENOm ET NOM LORS DE L'INSCRIPTON
function validationLetters(mot) {
	return /[a-zA-Z]+/.test(mot)
}