FrontEnd.onInitPage("professeur",function(){


	/*
	------------------------------------------------------------------------------------------------------------------------------
	<---------------------Partie JQUERY pour appeler les fonctions contenant des requetes AJAX---------------------------------->
	------------------------------------------------------------------------------------------------------------------------------
	*/

	/*-----------------------------------------------
	Cache les divers pages utiles à la manipulation 
	des états de la mobilité au chargement de la page
	-------------------------------------------------*/
	$("#Créée").hide();
	$("#en_preparation").hide();
	$("#envoyer_demande_he").hide();
	$("#en_cours").hide();
	$("#a_payer").hide();
	$('#a_payer_solde').hide();
	$("#div_refus").hide();
	$('#div_annulation').hide();
	$('#CrééeSuisse').hide();
	$('#en_preparationSuisse').hide();
	$('#envoyer_demande_heSuisse').hide();
	$('#mobiliteTerminee').hide();
	$('#mobiliteRefusee').hide();
	$('#mobiliteAnnulee').hide();
	
	//APPEL FONCTION POUR CONFIRMER UNE MOBILITÉ
	$('#demandesProf').on('click','button',function(event){
		const buttClicke=$(this);
		confirmerMobilite(buttClicke);
	})

	//APPEL FONCTION POUR EXTRAIRE UNE DEMANDE
	$('#exporterDonnees').on('click',function(event){
		exporterDemandes();
	})


	//APPEL FONCTION POUR LISTER LES DEMANDES DE PAYEMENT PAR ANNEE ACADEMIQUE
	$('#bouton_lister_demande_payement').on('click', function(){
		listerDemandePayementParAnnee();	
	})

	//AFFICHAGE DU TEXTAREA POUR LES MOTIVATIONS DE L'ANNULATION DE MOBILITÉ
	$('button[name="annuler_mobilite"]').on('click',function(){
		$('#div_annulation').show();
		$('#div_refus').hide();
	});

	//ENVOIE DE LA MOTIVATION D'ANNULATION
	$('#envoi_annulation').on('click',function(){
		annulerRefuserMobilite("annule");
	})

	//AFFICHAGE DU TEXTAREA POUR LES MOTIVATIONS DU REFUS DE MOBILITÉ
	$('button[name="refuser_mobilite"]').on('click',function(){
		$('#div_refus').show();
		$('#div_annulation').hide();
	});

	//ENVOIE DE LA MOTIVATION DE REFUS
	$('#envoi_refus').on('click',function(){
		annulerRefuserMobilite("refuse");
	})

	//APPEL DE LA MÉTHODE POUR GÉRER UNE MOBILIÉ
	$('#gererMobEtTable').on('click','button',function(event){
		const buttClick=$(this);
		gererMobilite(buttClick);
	})

	//CACHE TOUTE LES PAGES POUR GÉRER LA MOBILITÉ LORSQU'ON VEUT REVENIR SUR LE HUB DU PROFESSEUR
	$('button[name="retour"]').on('click',function(event){
		$("#Créée").hide();
		$("#CrééeSuisse").hide();
		$("#en_preparationSuisse").hide();
		$("#envoyer_demande_heSuisse").hide();

		$("#mobiliteTerminee").hide();
		$("#mobiliteRefusee").hide();
		$("#mobiliteAnnulee").hide();
		$("#en_preparation").hide();
		$("#vueGenerale").show();
		$('#div_refus').hide();
		$('#div_annulation').hide();
		$('#a_payer').hide();
		$("#envoyer_demande_he").hide();
		$('#a_payer_solde').hide();
	})
	
	//SAUVEGARDER LES DOCUMENTS REçUS ET INDIQUER SI LES DONNÉES ONT ÉTÉE ENCODÉES DANS UN AUTRE LOGICIELS
	$('button[name="sauver_documents_pre"]').on('click',function(event){
		confirmerDocEtudiantRempli();
		//preciserDonneesEncodeesLogicielExternes();
	})

	//ENVOYER LA PREMIÈRE DEMANDE DE PAIEMENT
	$('button[name="envoyer_demande"]').on('click',function(){
		indiquerEnvoiDemandePaiement();
	})

	//SAUVEGARDER LES DOCUMENTS REçUS AU RETOUR DE L'ÉTUDIANT
	$('#savEnvoyeHE').on('click',function(){
		preciserDocumentRetourEtudiantRecus();
	})
	
	//INDIQUER QUE LA MOBILITÉE EST À L'ÉTAT "en_remboursement"
	$('#demRemb').on('click',function(){
		enRemboursement();
	})

	//SUISSE
	//SAUVEGARDER LES DOCUMENTS REçUS AU RETOUR DE L'ÉTUDIANT
	$('#savEnvoyeHESuisse').on('click',function(){
		preciserDocumentRetourEtudiantRecus();
	})
	
	//INDIQUER QUE LA MOBILITÉE EST À L'ÉTAT "en_remboursement"
	$('#demRembSuisse').on('click',function(){
		enRemboursement();
	})



	//INDIQUER L'ENVOI DE LA DEUXIEME DEMANDE DE PAIEMENT
	$('#deuxiemeDemandePayement').on('click',function(){
		indiquerEnvoieDeuxiemeDemandePaiement();
	})

	/*
		-----------------------------------------------------------------------------------
		<------------Chargement des données utiles au chargement de la page--------------->
		-----------------------------------------------------------------------------------
		*/
		FrontEnd.onDisplayPage("professeur", function() {
			getDemandesMobilitees();
			getUtilisateurs();
			getMobilitees();
			getMobiliteesNonAnnulees();

			$("#page-header").show();
			$("#footer").show();
			$("#etudiant").hide();
			$("#connexion-inscription").hide();
			$('#inscription').hide();
			$('#page_recherche').hide();
			$("#professeur-responsable").hide();
		})  

	/*
		-----------------------------------------------------------------------------------
		<------------Vidage des tableaux et des données au changement de page------------->
		-----------------------------------------------------------------------------------
		*/
		FrontEnd.onLeavePage("professeur",function(){
			$("#professeur").hide();
			$("#demandesProf").empty();
			$("#allUtilisateurs").empty();
			$("#mobilitesProf").empty();
			$("#gererMobEtTable").empty();
		})

	});

/*
	-------------------------------------------------------------------------------------------------------------------------------------
	<----------------------------------------------------------------APPELS AJAX ------------------------------------------------------->
	-------------------------------------------------------------------------------------------------------------------------------------
	*/

/*-------------------------------------------------------------------------------------------------------------
AJAX pour indiquer le deuxième envoie de la demande de paiement lorsqu'on se trouve dans l'état "A Payer Solde"
--------------------------------------------------------------------------------------------------------------*/
function indiquerEnvoieDeuxiemeDemandePaiement(){
	console.log("attestation séjour : " + $('#attSejour').prop('checked'));
	console.log("Relevé de notes : " + $('#relNotes').prop('checked'));
	console.log("certificat de stage : " + $('#certifStage').prop('checked'));
	console.log("rapport final : " + $('#rapFinal').prop('checked'));
	console.log("preuve test : " + $('#preuve_passageApMob').prop('checked'));
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"indiquer_envoie_deuxieme_demande_paiement",
			id_mobilite_etudiante:$('#idMobi').val(), // ON L'AURA GET AVANT VU QUE LE PROF DOIT CLIQUER DESSUS
			num_version:$('#numVersMob').val(),
			attestation_sejour:$('#attSejour').prop('checked'),
			releve_notes:$('#relNotes').prop('checked'),
			certificat_stage:$('#certifStage').prop('checked'),
			rapport_final:$('#rapFinal').prop('checked'),
			preuve_passage_langue:$('#preuve_passageApMob').prop('checked')
		},
		success:function(resp){
			console.log('indiquer envoie deuxieme demande paiement  OK');
		},
		error:function(e){
			console.log('indiquer envoie deuxieme demande paiement  PAS OK');
		}
	});
}

/*---------------------------------------------------------
AJAX pour mettre la mobilité dans l'état "en_remboursement"
-----------------------------------------------------------*/
function enRemboursement(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"indiquer_mobilite_en_remboursement",
		id_mobilite:$('#idMobi').val(),
		num_version:$('#numVersMob').val()
	},
	success:function(resp){
		console.log('remboursement ok');
	},
	error:function(e){
		console.log('remboursement echec');
	}
});
}

/*-----------------------------------------------------------------------------------------------
AJAX permettant de récuperer la mobilité et de faire passer la mobilité dans ses différents états
------------------------------------------------------------------------------------------------*/
function gererMobilite(bouton){

	let numLigne=bouton.val();
	console.log($("#g"+numLigne).find('td[name="mobiAGerer"]').text());
	let paysMobi = $("#g"+numLigne).find('td[name="paysMobiAGerer"]').text();
	console.log(paysMobi);
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"get_mobi_par_id",id_mobi:$("#g"+numLigne).find('td[name="mobiAGerer"]').text()},
		success:function(resp){
			
			console.log(resp);
			
			$('#idMobi').val(resp.idMobiliteEtudiant);
			$('#numVersMob').val(resp.numVersion);
			$('#programmeDemMob').val(resp.demande.programme);
			$('#preuve_passageApMob').val(resp.demande.programme);
			$('#paysRenvoi').val(resp.demande.pays.codeIso);

			console.log($('#paysRenvoi').val());

			// CAS SUISSE
			if(resp.demande.pays.codeIso === "CHE"){
				
				console.log("ON A UNE MOBI SUISSE");
				
				$('#vueGenerale').hide();

				if(resp.etat === "Créée"){
					$('#CrééeSuisse').show();
					
					if(resp.preuveTestLinguistique===true){
						$('input[name="preuve_passageSuisse"]').prop('checked',true);
						$('input[name="preuve_passageSuisse"]').hide();
						$('#preuve_passageSuisse').prop('checked',true);
					}
					if(resp.documentEngagement===true){
						$('input[name="doc_engagementSuisse"]').prop('checked',true);
						$('input[name="doc_engagementSuisse"]').hide();
						$('#doc_engagementSuisse').prop('checked',true);
					}
					if(resp.charteEtudiant===true){
						$('input[name="charteSuisse"]').prop('checked',true);
						$('input[name="charteSuisse"]').hide();
						$('#charteSuisse').prop('checked',true);
					}
					if(resp.conventionStageEtude===true){
						$('input[name="conventionSuisse"]').prop('checked',true);
						$('input[name="conventionSuisse"]').hide();
						$('#conventionSuisse').prop('checked',true);
					}if(resp.encodeProEco===true){
						$('#pro_ecoSuisse').prop('checked',true);
						$('#pro_ecoSuisse').hide();
					}
					if(resp.encodeMobilityTool===true){
						$('#mobility_toolSuisse').prop('checked',true);
						$('#mobility_toolSuisse').hide();
						$('#mobiSuisse').hide();	
					}
					if(resp.encodeMobi===true){
						$('#mobiSuisse').prop('checked',true);
						$('#mobiSuisse').hide();
						$('#mobility_toolSuisse').hide();
					}
				}
				if(resp.etat === "en_preparation"){
					console.log("bb");
					$('#en_preparationSuisse').show();
					if(resp.preuveTestLinguistique===true){
						$('input[name="preuve_passageSuisse"]').prop('checked',true);
						$('input[name="preuve_passageSuisse"]').hide();
						$('#preuve_passage2Suisse').prop('checked',true);
						$('#preuve_passage2Suisse').hide();
					}
					if(resp.documentEngagement===true){
						$('input[name="doc_engagementSuisse"]').prop('checked',true);
						$('input[name="doc_engagementSuisse"]').hide();
						$('#doc_engagement2Suisse').prop('checked',true);
						$('#doc_engagement2Suisse').hide();
					}
					if(resp.charteEtudiant===true){
						$('input[name="charteSuisse"]').prop('checked',true);
						$('input[name="charteSuisse"]').hide();
						$('#charte2Suisse').prop('checked',true);
						$('#charte2Suisse').hide();
					}
					if(resp.conventionStageEtude===true){
						$('input[name="conventionSuisse"]').prop('checked',true);
						$('input[name="conventionSuisse"]').hide();
						$('#convention2Suisse').prop('checked',true);
						$('#convention2Suisse').hide();
					}
					if(resp.encodeProEco===true){
						$('input[name="pro_eco"]').prop('checked',true);
						$('input[name="pro_eco"]').hide();
						$('#pro_eco2Suisse').prop('checked',true);
						$('#pro_eco2Suisse').hide();
					}
					if(resp.encodeMobilityTool===true){
						$('input[name="mobility_tool"]').prop('checked',true);
						$('input[name="mobility_tool"]').hide();
						$('#mobility_tool2Suisse').prop('checked',true);
						$('#mobility_tool2Suisse').hide();
						$('#mobi2Suisse').hide();	
					}
					if(resp.encodeMobi===true){
						$('input[name="mobi"]').prop('checked',true);
						$('input[name="mobi"]').hide();

						$('#mobi2Suisse').prop('checked',true);
						$('#mobi2Suisse').hide();
						$('#mobility_tool2Suisse').hide();
					}
				}
				if(resp.etat === "en_cours"){
					$('#envoyer_demande_heSuisse').show();
					if(resp.attestationSejourRecu===true){
						$('#attSejourSuisse').prop('checked',true);
						$('#attSejourSuisse').hide();
					}
					if(resp.releveNoteRecu===true){
						$('#relNotesSuisse').prop('checked',true);
						$('#relNotesSuisse').hide();
					}
					if(resp.certificatStageRecu===true){
						$('#certifStageSuisse').prop('checked',true);
						$('#certifStageSuisse').hide();
					}
					if(resp.rapportFinal===true){
						$('#rapFinalSuisse').prop('checked',true);
						$('#rapFinalSuisse').hide();
					}

					//ENCODER EXTERNE SUISSE
					if(resp.encodeProEco===true){
						$('#pro_ecoCSuisse').prop('checked',true);
						$('#pro_ecoCSuisse').hide();
					}
					if(resp.encodeMobilityTool===true){
						$('#mobility_toolCSuisse').prop('checked',true);
						$('#mobility_toolCSuisse').hide();
						$('#mobiCSuisse').hide();
					}
					if(resp.encodeMobi===true){
						$('#mobiCSuisse').prop('checked',true);
						$('#mobiCSuisse').hide();
						$('#mobility_toolCSuisse').hide();
					}

				}
				if(resp.etat === "Termine"){
					$('#mobiliteTerminee').show();
				}
				if(resp.etat === "refuse"){
					$('#mobiliteRefusee').show();
				}
				if(resp.etat === "annule"){
					$('#mobiliteAnnulee').show();
				}
				
			}// PAS LE CAS SUISSE
			else{

				$('#'+resp.etat).show();
				$('#vueGenerale').hide();
				

				if(resp.contratBourse===true){
					$('input[name="bourse"]').prop('checked',true);
					$('input[name="bourse"]').hide();
					$('#bourse').prop('checked',true);
				}
				if(resp.preuveTestLinguistique===true){
					$('input[name="preuve_passage"]').prop('checked',true);
					$('input[name="preuve_passage"]').hide();
					$('#preuve_passage').prop('checked',true);
				}
				if(resp.documentEngagement===true){
					$('input[name="doc_engagement"]').prop('checked',true);
					$('input[name="doc_engagement"]').hide();
					$('#doc_engagement').prop('checked',true);
				}
				if(resp.charteEtudiant===true){
					$('input[name="charte"]').prop('checked',true);
					$('input[name="charte"]').hide();
					$('#charte').prop('checked',true);
				}
				if(resp.conventionStageEtude===true){
					$('input[name="convention"]').prop('checked',true);
					$('input[name="convention"]').hide();
					$('#convention').prop('checked',true);
				}
				if(resp.attestationSejourRecu===true){
					$('#attSejour').prop('checked',true);
					$('#attSejour').hide();
				}
				if(resp.releveNoteRecu===true){
					$('#relNotes').prop('checked',true);
					$('#relNotes').hide();
				}
				if(resp.certificatStageRecu===true){
					$('#certifStage').prop('checked',true);
					$('#certifStage').hide();
				}
				if(resp.preuveTestLinguistiqueRetour===true){
					$('#preuve_passageApMob').prop('checked',true);
					$('#preuve_passageApMob').hide();
				}
				if(resp.rapportFinal===true){
					$('#rapFinal').prop('checked',true);
					$('#rapFinal').hide();
				}
				if(resp.encodeProEco===true){
					$('#pro_eco').prop('checked',true);
					$('#pro_eco2').prop('checked',true);
					$('#pro_eco').hide();
					$('#pro_eco2').hide();
					$('#pro_eco_retour').prop('checked',true);
					$('#pro_eco_retour').hide();
				}
				if(resp.encodeMobilityTool===true){
					$('#mobility_tool').prop('checked',true);
					$('#mobility_tool').hide();
					$('#mobility_tool2').prop('checked',true);
					$('#mobility_tool2').hide();
					$('#mobi').hide();
					$('#mobi2').hide();
					$('#mobility_tool_retour').prop('checked',true);
					$('#mobility_tool_retour').hide();
					$('#mobi_retour').hide();
				}
				if(resp.encodeMobi===true){
					$('#mobi').prop('checked',true);
					$('#mobi').hide();
					$('#mobi2').prop('checked',true);
					$('#mobi2').hide();
					$('#mobility_tool').hide();
					$('#mobility_tool2').hide();
					$('#mobi_retour').prop('checked',true);
					$('#mobi_retour').hide();
					$('#mobility_tool_retour').hide();
				}


				console.log("reussite getMobiliteParId");
			}
		},error:function(){
			console.log("erreur getMobiliteParId");
		}
	});
}

/*------------------------------------------------------
AJAX permettant de confirmer les documents de l'étudiant
--------------------------------------------------------*/
function confirmerDocEtudiantRempli(){
	

	//Regarde si les différents fichiers ont bien été cochés + PARTIE SUISSE
	if($('#bourse2').prop('checked')===true){
		$('#bourse').prop('checked',true);
	}
	if($('#convention2').prop('checked')===true){
		$('#convention').prop('checked',true);
	}
	if($('#charte2').prop('checked')===true){
		$('#charte').prop('checked',true);
	}
	if($('#preuve_passage2').prop('checked')===true){
		$('#preuve_passage').prop('checked',true);
	}
	if($('#doc_engagement2').prop('checked')===true){
		$('#doc_engagement').prop('checked',true);
	}

	//cas où false
	if($('#preuve_passage2').prop('checked')===false){
		$('#preuve_passage').prop('checked',false);
	}

	//ENCODER EXTERNE
	if($('#pro_eco2').prop('checked')===true){
		$('#pro_eco').prop('checked',true);
	}
	if($('#mobility_tool2').prop('checked')===true){
		$('#mobility_tool').prop('checked',true);
	}
	if($('#mobi2').prop('checked')===true){
		$('#mobi').prop('checked',true);
	}
	if($('#pro_eco2').prop('checked')===false){
		$('#pro_eco').prop('checked',false);
	}
	if($('#mobility_tool2').prop('checked')===false){
		$('#mobility_tool').prop('checked',false);
	}
	if($('#mobi2').prop('checked')===false){
		$('#mobi').prop('checked',false);
	}


	//PARTIE SUISSE
	if($('#convention2Suisse').prop('checked')===true || $('#conventionSuisse').prop('checked')===true){
		$('input[name="convention"]').prop('checked',true);
	}
	if($('#charte2Suisse').prop('checked')===true || $('#charteSuisse').prop('checked')===true){
		$('input[name="charte"]').prop('checked',true);
	}
	if($('#preuve_passage2Suisse').prop('checked')===true || $('#preuve_passageSuisse').prop('checked')===true){
		$('input[name="preuve_passage"]').prop('checked',true);
	}
	if($('#doc_engagement2Suisse').prop('checked')===true || $('#doc_engagementSuisse').prop('checked')===true){
		$('input[name="doc_engagement"]').prop('checked',true);
	}

	//ENCODER EXTERNE SUISSE
	if($('#pro_eco2Suisse').prop('checked')===true || $('#pro_ecoSuisse').prop('checked')===true){
		$('input[name="pro_eco"]').prop('checked',true);
	}
	if($('#mobility_tool2Suisse').prop('checked')===true || $('#mobility_toolSuisse').prop('checked')===true){
		$('input[name="mobility_tool"]').prop('checked',true);
	}
	if($('#mobi2Suisse').prop('checked')===true || $('#mobiSuisse').prop('checked')===true){
		$('input[name="mobility_tool"]').prop('checked',true);
	}

	//Requete AJAX
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"confirmer_document_etudiant_rempli",
		contrat_bourse:$('input[name="bourse"]').prop("checked"),
		convention_stage_etude:$('input[name="convention"]').prop("checked"),
		charte_etudiant:$('input[name="charte"]').prop("checked"),
		preuve_test_linguistique:$('input[name="preuve_passage"]').prop("checked"),
		document_engagement:$('input[name="doc_engagement"]').prop("checked"),
		id_mobilite:$('#idMobi').val(),
		num_version:$('#numVersMob').val(),
		programme:$('#programmeDemMob').val(),
		pays:$('#paysRenvoi').val()
	},
	success:function(resp){

		console.log('confirmation docs étudiant OK');
		$('#alertRechercherPartenaire').text("La sauvegarde a fonctionnée");
		
		//REQUETE AJAX POUR PRECISER DONNEES ENCODEES DANS LOGICIEL EXTERNE
		$.ajax({
			url:'http://localhost:8680/api',
			type:'POST',
			dataType:'JSON',
			data:{action:"preciser_donnees_encodees_logiciel_externes",
			id_mobilite:$('#idMobi').val(),
			encode_pro_eco:$('input[name="pro_eco"]').prop('checked'),
			encode_mobility_tool:$('#mobility_tool').prop('checked'),
			encode_mobi:$('#mobi').prop('checked'),
			num_version:$('#numVersMob').val()
		},
		success:function(resp){
			console.log('confirmation externes OK');
		},
		error:function(e){
			console.log('confirmation externes PAS OK');
		}
	});
		$('#alertSauvegardeCree').text("La sauvegarde a fonctionnée");

	},
	error:function(e){
		console.log('confirmation doc étudiant PAS OK');
		$('#alertRechercherPartenaire').text("La sauvegarde n'a pas fonctionnée");
		$('#alertSauvegardeCree').text("La sauvegarde n'a fonctionnée");
	}
});
}

/*-----------------------------------------------------------------------------------
AJAX  permettant de préciser la bonne réception des documents au retour de l'étudiant 
------------------------------------------------------------------------------------*/
function preciserDocumentRetourEtudiantRecus(){
	
	//PARTIE SUISSE
	if($('#attSejourSuisse').prop('checked')){
		$('#attSejour').prop("checked",true);
	}
	if($('#relNotesSuisse').prop('checked')){
		$('#relNotes').prop("checked",true);
	}
	if($('#certifStageSuisse').prop('checked')){
		$('#certifStage').prop("checked",true);
	}
	if($('#rapFinalSuisse').prop('checked')){
		$('#rapFinal').prop('checked',true);
	}


	//ENCODER EXTERNE SUISSE
	if($('#pro_ecoCSuisse').prop('checked')===true){
		$('#pro_eco_retour').prop('checked',true);
	}
	if($('#mobility_toolCSuisse').prop('checked')===true){
		$('#mobility_tool_retour').prop('checked',true);
	}
	if($('#mobiCSuisse').prop('checked')===true){
		$('#mobi_retour').prop('checked',true);
	}

	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"preciser_document_etudiant_retour_recu",
		attestation_sejour_recu:$('#attSejour').prop("checked"),
		releve_note_recu:$('#relNotes').prop("checked"),
		charte_etudiant:$('#certifStage').prop("checked"),
		preuve_test_linguistique_retour:$('#preuve_passageApMob').prop("checked"),
		rapport_final:$('#rapFinal').prop('checked'),
		id_mobilite:$('#idMobi').val(),
		num_version:$('#numVersMob').val(),
		programme:$('#programmeDemMob').val(),
		pays:$('#paysRenvoi').val()
	},
	success:function(resp){
		console.log('precision document etudiant retour recus OK');
		
		//PARTIE DONNEES ENCODEES EN EXTERNE
		$.ajax({
			url:'http://localhost:8680/api',
			type:'POST',
			dataType:'JSON',
			data:{action:"preciser_donnees_encodees_logiciel_externes",
			id_mobilite:$('#idMobi').val(),
			encode_pro_eco:$('#pro_eco_retour').prop('checked'),
			encode_mobility_tool:$('#mobility_tool_retour').prop('checked'),
			encode_mobi:$('#mobi_retour').prop('checked'),
			num_version:$('#numVersMob').val()
		},
		success:function(resp){
			console.log('confirmation externes OK');
			$('#alertEnvoyerDemandeHE').text("La sauvegarde a fonctionnée");
		},
		error:function(e){
			console.log('confirmation externes PAS OK');
		}
	});
		
	},
	error:function(e){
		console.log('precision document etudiant retour recus PAS OK');
		$('#alertEnvoyerDemandeHE').text("La sauvegarde n'a fonctionnée");
	}
});
}

/*--------------------------------------------------------------------------------------
AJAX permettant d'envoyer la première demande de paiement pour passer à l'état "a_payer"
---------------------------------------------------------------------------------------*/
function indiquerEnvoiDemandePaiement(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"preciser_envoyer_demande_he",
		num_version:$('#numVersMob').val(),
		id_mobilite:$("#idMobi").val()
	},
	success:function(resp){
		console.log(' preciser he ok');
	},
	error:function(e){
		console.log('preciser he echec');
	}
});	
}

/*------------------------------------------------------------------------
AJAX qui complète un tableau avec les mobilitées en cours (CRÉE->TERMINÉE) 
sur lesquelles on peut indiquer les divers fichier reçus etc...
--------------------------------------------------------------------------*/
function getMobiliteesNonAnnulees(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"get_mobilites_non_annulees"},
		success:function(resp){
			for(let i=0;i<resp.length;i++){
				
				$("#gererMobEtTable").append(
					'<tr id="g'+i+'">' + 
					'<td>' + resp[i].personne.pseudo + '</td>' +
					'<td>' + ((resp[i].demande.pays === null) ? 'Non défini' : resp[i].demande.pays.nom)+'</td>' +
					'<td>' + resp[i].demande.anneeAcademique + '</td>' +
					'<td>' + resp[i].demande.programme + '</td>'+
					'<td>' + resp[i].partenaire.nomComplet + '</td>' +
					'<td>' + resp[i].demande.quadrimestre + '</td>'+
					'<td>' + resp[i].etat + '</td>' +
					'<td>' + '<button class="btn btn-primary" value='+i+'>Gérer</button>' + '</td>'+
					'<td name="mobiAGerer" style="display:none;">'+resp[i].idMobiliteEtudiant+'</td>'+
					'<td name="paysMobiAGerer" style="display:none;">'+resp[i].demande.pays.codeIso+'</td>' +
					'</tr>'
					);
			}
		},error:function(){
			console.log("erreur getMobiliteNonAnnulee");
		}
	});
}

/*----------------------------------------------------
AJAX permettant d'annuler ou de refuser une mobilité
Paramètre : annule ou refuse
----------------------------------------------------*/
function annulerRefuserMobilite(_etat_mob){

	//DECLARATION DES VARIABLES
	var _dernier_etat;
	var _num_version;
	var _id_mobilite_etudiante;

	//RECUPERATION DES MOTIVATIONS DE REFUS/ANNULATION
	if(_etat_mob==="refuse"){
		var _raison_annulation_refus=$('#raison_refus').val();
	}else{
		var _raison_annulation_refus=$('#raison_annulation').val();
	}

	//REQUETE AJAX permettant de récuperer l'etat, le num_version ainsi que l'ID de la mobilité
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"get_mobi_par_id",id_mobi:$('#idMobi').val()},
		success:function(resp){
			_dernier_etat=resp.etat;
			_num_version=resp.numVersion;
			_id_mobilite_etudiante=resp.idMobiliteEtudiant;

			//AJAX PERMETTANT D'ANNULER/REFUSER UNE MOBILITÉ
			$.ajax({
				url:'http://localhost:8680/api',
				type:'POST',
				dataType:'JSON',
				data:{action:"annuler_refuser_mobilite_etudiante",etat:_etat_mob,dernier_etat:_dernier_etat,num_version:_num_version,
				id_mobilite_etudiante:_id_mobilite_etudiante,raison_annulation_refus:_raison_annulation_refus},
				success:function(resp){
					$('#alertRefus').text("Vous avez refusé/annulé avec succès");
					$('#div_annulation').hide();
					$('#div_refus').hide();
				},
				error:function(e){
					console.log("erreur annuler/refuser");
				}
			});
		},
		error:function(e){
			console.log("pas de mobi confirme");
		}
	});
}
/*---------------------------------------------------------------------------------------------
RECUPERATION DES MOBILITÉES ACCEPTEES POUR CACHER OU MONTRER le bouton pour encoder ses données
-----------------------------------------------------------------------------------------------*/
function getMobiliteAcceptee(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"get_mobilite_non_annulees"},
		success:function(resp){
			$('#button_encoder_mes_donnees').show();
		},
		error:function(e){
			$('#button_encoder_mes_donnees').hide();
		}
	});
}

// POURQUOI RENOMMER LACTION AVEC PROFESSEUR A LA FIN ?
function confirmerMobiEtIntroduireNouveauPartenaireProfesseur(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		data:{ action:"confirmer_demande_mobilite_avec_nouveau_partenaire_professeur",
		pays:$('#pays_partenaire').val(),
		nom_legal:$('#nomLegal_partenaire').val(),
		nom_affaires:$('#nomAffaires_partenaire').val(),
		nom_complet:$('#nomComplet_partenaire').val(),
		rue:$('#rue_partenaire').val(),
		numero:$('#numero_partenaire').val(),
		boite:$('#boite_partenaire').val(),
		code_postal:$('#codePostal_partenaire').val(),
		commune:$('#commune_partenaire').val(),
		region:$('#region_partenaire').val(),
		code:$('#code_partenaire').val(),
		type_organisation:$('#type_organisation_partenaire').val(),
		nb_employes:nbEmplR,
		email:$('#email_partenaire').val(),
		site_web:$('#siteWeb_partenaire').val(),
		telephone:$('#telephone_partenaire').val(),
		departement:$('#departement_partenaire').val(),
		ville:$('#ville_partenaire').val(),
		id_demande:$('#idDemande').val(),
		quadri:$('#quadri').val(),
		num_version_demande_mobilite:$('#numVersionDemandeMobi').val(),
		id_etudiant:$('#id_etudiant').val()
	},
	success: function(resp){
		$('#alertEncodagePartenaire').text("Encodage effectué");
	},	
	error:function(e){
		console.log("failure encodage de partenaire");
		$('#alertEncodagePartenaire').text("problème d'encodage d'un partenaire");
	}
});	
}

/*-----------------------------------
RECUPERATION DE LA MOBILITÉ PAR ANNEE
-------------------------------------*/
function getDemandeMobiParAnnee(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"get_demande_mobi_par_annee",
		annee:$('#annee').val()	
	},
	success:function(resp){
		console.log('reçus demande mobilite etudiant par annnee  OK');
	},
	error:function(e){
		console.log('reçus demande mobilite etudiant par annnee  PAS OK');
	}
});
}

/*-------------------------------------------------------------------------
AJAX PERMETTANT DE RECHERCHER LA LISTE DES DEMANDES DE PAYEMENT PAR ANNEE
---------------------------------------------------------------------------*/
function listerDemandePayementParAnnee(){
	
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'json',
		data:{action:"get_liste_paiements",
		annee_academique:$('#anneeListePayement').val()
	},
	success:function(resp){
		console.log(resp);
		for(let i=0;i<resp.length;i++){
				//visuel de la demande une
				let demandeUne = resp[i].envoieDemandePaiement1;
				if(demandeUne==true){
					demandeUne = "Envoyée";
				} else {
					demandeUne = "pas envoyée";
				}
				//visuel de la demande deux
				let demandeDeux = resp[i].envoieDemandePaiement2;
				if(demandeDeux==true){
					demandeDeux = "Envoyée";
				} else {
					demandeDeux = "pas envoyée";
				}
				
				$("#tableListedemandePayementBody").append(
					'<tr>' + 
					'<td>' + resp[i].personne.pseudo + '</td>' +
					'<td>' + ((resp[i].demande.pays === null) ? 'Non défini' : resp[i].demande.pays.nom) + '</td>' +
					'<td>' + resp[i].demande.anneeAcademique + '</td>' +
					'<td>' + resp[i].demande.programme + '</td>' +
					'<td>' + resp[i].partenaire.nomComplet + '</td>'+
					'<td>' + resp[i].demande.quadrimestre + '</td>'+
					'<td>' + demandeUne + '</td>'+
					'<td>' + demandeDeux + '</td>'+

					'</tr>'
					);
			}
			console.log("reussi lister payement");
		},
		error:function(e){
			console.log("failure lister payement");
		}
	});

}

/*---------------------------------------------------------------------------------------------
AJAX permettant DE RECUPERER TOUTE LES MOBILITEES ET AFFICHER LEUR INFORMATIONS DANS UN TABLEAU
-----------------------------------------------------------------------------------------------*/
function getMobilitees(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'json',
		data:{action:"visualiser_toutes_les_mobilites"},
		success:function(resp){
			console.log(resp);
			for(let i=0;i<resp.length;i++){
				
				let quadriRespons = resp[i].quadrimestre;
				if(quadriRespons==0){
					quadriRespons="Non défini";
				}
				
				let nomPart=resp[i].partenaire.nomComplet;
				if(nomPart==null){
					nomPart="Non défini";
				}

				$("#mobilitesProf").append(
					'<tr>' + 
					'<td>' + resp[i].personne.pseudo + '</td>' +
					'<td>' + ((resp[i].demande.pays === null) ? 'Non défini' : resp[i].demande.pays.nom) + '</td>' +
					'<td>' + nomPart + '</td>' +
					'<td>' + resp[i].etat + '</td>' +
					'<td>' + quadriRespons + '</td>'+
					'</tr>'
					);
			}
			console.log("reussi lister mob");
		},
		error:function(e){
			console.log("failure lister mob");
		}
	});
}

/*----------------------------------------------------------------------------------------------------
AJAX permettant DE RECUPERER TOUTE LES DEMANDES DE MOBILITEES ET D'AFFICHER LEUR INFOS DANS UN TABLEAU
------------------------------------------------------------------------------------------------------*/
function getDemandesMobilitees(){
	//--------------------------------ajax-----------------------------------------------
	var listeNomsPartenaires = '<select id="pet-select"><option value="">Veuillez selectionner un partenaire</option>';

	//AUTO-COMPLETION
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"completion_nom_partenaire_pays_ville_recherche"			
	},
	success:function(resp){
		console.log(resp);
		console.log("Succès completion partenaire");
		for(let i=0;i<resp.length;i++){				
			listeNomsPartenaires += '<option value="' + resp[i].idPartenaire + '">' + resp[i].nomComplet + '</option>';
		}
		listeNomsPartenaires += '</select>';
	},
	error:function(e){
		console.log("Erreur de completion partenaire");
	}
});
	//--------------------------------ajax-----------------------------------------------

	
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'json',
		data:{action:"visualiser_toutes_les_demandes"},
		success:function(resp){
			console.log(resp);
			for(let i=0;i<resp.length;i++){

				let quadriR = resp[i].quadrimestre;
				if(quadriR==0){
					quadriR ='<select id="pet-select"><option value="">Veuillez selectionner un quadrimestre</option><option value="1">1</option><option value="2">2</option></select>';
				}
				let departement =resp[i].partenaire.departement;
				if(departement===null){
					departement="Non défini";
				}

				let nomAff=resp[i].partenaire.nomComplet;

				if(nomAff==null){
					nomAff=listeNomsPartenaires;
				}
				$("#demandesProf").append(
					'<tr id='+i+'>' + 
					'<td name="idPers" style="display:none;">' + resp[i].personne.idPersonne + '</td>' +
					'<td>' + resp[i].numOrdreCandidature + '</td>' +
					'<td>' + resp[i].personne.nom + '</td>' +
					'<td>' + resp[i].personne.prenom + '</td>' +
					'<td>' + resp[i].ordrePreference + '</td>' +
					'<td>' + resp[i].programme + '</td>'+
					'<td>' + resp[i].code + '</td>'+
					'<td name="quad">' + quadriR + '</td>'+
					'<td name="idPart" >' + nomAff + '</td>'+
					'<td name="validerDemande">' + '<button class="btn btn-primary" value='+i+'>Valider</button>' + '</td>' +
					'<td name="numVersion" style="display:none;">' + resp[i].numVersion + '</td>'+
					'<td name="numDemande" style="display:none;">' + resp[i].idDemande + '</td>'+
					'<td name="part" style="display:none;">' + resp[i].partenaire.nomComplet + '</td>'+
					'</tr>'
					);
			}
			console.log("reussi lister demande mob");
		},
		error:function(e){
			console.log("failure lister mob");
		}
	});
}

/*----------------------------------------------------------------------
AJAX permettant D'EXPORTER UNE DEMANDE DE MOBILITÉ
------------------------------------------------------------------------*/
function exporterDemandes(){
	//REQUETE AJAX
	$.ajax({
		url: 'http://localhost:8680/api',
		method: 'POST',
		xhrFields: {
			responseType: 'text'
		},
		data:{action:"extraire_csv"},
		success: function (data) {
			var a = document.createElement('a');
			var file = new Blob([data], {type: 'text/csv;charset=UTF-8'});
			var url = URL.createObjectURL(file);
			a.href = url;
			a.download = 'demandesMobilitées.csv';
			a.click();
			window.URL.revokeObjectURL(url);
			console.log("reussi exporter demande mobilite");
		},
		error:function(e){
			console.log("failure exporter demande mobilite");
		}
	});
}

/*----------------------------------------------------------------------
AJAX permettant DE CONFIRMER UNE MOBILITEE ET DE LA PASSER A L'ÉTAT CRÉE
------------------------------------------------------------------------*/
function confirmerMobilite(bouton){
	
	let numLigne=bouton.val();

	let nVersion=$("#"+numLigne).find('td[name="numVersion"]').text();
	let part= $("#"+numLigne).find('td[name="idPart"]').find(":selected").text();
	let idPers=$("#"+numLigne).find('td[name="idPers"]').text();
	let idDem=$("#"+numLigne).find('td[name="numDemande"]').text();
	let quad=$("#"+numLigne).find('td[name="quad"]').find(":selected").val();
	if(quad==="Non défini"){
		quad=0;
	}
	//si partenaire est déjà donné
	if(part===""){
		part=$("#"+numLigne).find('td[name=part]').text();
	}
	if(quad===undefined){
		quad=$("#"+numLigne).find('td[name="quad"]').text();
	}
	
	//REQUETE AJAX
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"confirmer_demande_mobilite",
		id_demande:idDem,
		num_version_demande_mobilite:nVersion,
		id_partenaire:part,
		id_personne:idPers,
		quadri:quad},
		success:function(resp){
			$('#alertConfirmMobilite').text("La mobilite a bien ete confirmee");
			console.log("reussi confirmer la demande de mobilite");
		},
		error:function(e){
			$('#alertConfirmMobilite').text("La mobilite ne peut pas etre confirmee");
			console.log("failure confirmation demande mobilite");
		}
	});
}


/*---------------------------------------------------------
AJAX permettant D'AFFICHER LALISTE DE TOUT LES UTILISATEURS
-----------------------------------------------------------*/
function getUtilisateurs(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"visualiser_tous_les_utilisateurs"},
		success:function(resp){
			for(let i=0;i<resp.length;i++){
				
				let roleAffiche;
				let departementAffiche=resp[i].departement;
				if(resp[i].role=="et"){
					roleAffiche="Etudiant";
				}if(resp[i].role=="pr"){
					roleAffiche="Professeur Responsable";
				}if(resp[i].role=="p"){
					roleAffiche="Professeur";
				}
				if(resp[i].departement==null){
					departementAffiche="Non défini";
				}
				date=resp[i].dateInscription.dayOfMonth+"/"+resp[i].dateInscription.monthValue+"/"+resp[i].dateInscription.year;
				$("#allUtilisateurs").append(
					'<tr>' + 
					'<td>' + resp[i].idPersonne + '</td>' +
					'<td>' + resp[i].nom + '</td>' +
					'<td>' + resp[i].prenom + '</td>' +
					'<td>' + resp[i].pseudo + '</td>' +
					'<td>' + resp[i].email + '</td>'+
					'<td>' + date + '</td>'+
					'<td>' + roleAffiche + '</td>'+
					'<td>' + departementAffiche + '</td>'+
					'<td>' + resp[i].nbAnneeReussies + '</td>'+
					'</tr>'
					);
			}
			console.log("reussi lister utilisateurs");
		},
		error:function(e){
			console.log("failure lister utilisateurs");
		}
	});
}

/*------------------------------------------------------------------------------------------------------------
AJAX permettant DE RÉCUPERER TOUT LES PARTENAIRES ET DE LES AFFICHER DANS UN SELECT EN HTML (TABLEAU DEMANDES)
-------------------------------------------------------------------------------------------------------------*/
function getNomIdPartenaires(){
	var listeNomsPartenaires = '<select id="pet-select"><option value="">Veuillez selectionner un partenaire</option>';
	
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"completion_nom_partenaire_pays_ville_recherche"			
	},
	success:function(resp){
		console.log("Succès completion partenaire");

		for(let i=0;i<resp.length;i++){				
			listeNomsPartenaires += '<option value="' + resp[i].idPartenaire + '">' + resp[i].nomComplet + '</option>';
		}
	},
	error:function(e){
		console.log("Erreur de completion partenaire");
	}
});

	listeNomsPartenaires += '</select>';

	return listeNomsPartenaires;
}
