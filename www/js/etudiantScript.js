var nombre_mobi = 0;
var a_une_mobi_confirme = false;
var a_encode_ses_donnees = false;
FrontEnd.onInitPage("etudiant",function(){
	
    //ajoute une limite de form max
    var maxForm = 5;
	
	$("#nouveauPartenaire").hide();
	$("#tableMob").hide();
	// USELESS MAINTENANT ! Ca fonctionne sans !
	/*if(a_une_mobi_confirme===true){
		if(a_encode_ses_donnees===false){
			console.log("il a deja encode ses donnees");
		}else{
			$('#encoderSesDonnees').show();
		}
		
	}else{
		$('#encoderSesDonnees').hide();
	}*/ 
	/*$('#bouton_conserver_demande').on('click',function(){
    	if($('#programme').val()=="" || $('#code').val()=="" || $('#pays').val()==""
    		|| $('#ordre').val()==""){
    		$('#alertMob').text("Vous devez remplir tout les champs");
    	}else{
			$('#alertMob').text("");
    		declarer_demande();
    	}
    });*/

	//Encoder nouveau partenaire
	/*$('#button_nvPart').on('click',function(){
		$("#nouveauPartenaire").show();
		
		$("#encoderNvPart").on('click',function(){
			$('#alertEncodagePartenaire').text("Encodage effectué");
			console.log("encoderPartenaireGo");
			encoderNouveauPartenaire();
		});
	});*/
	$('tbody').on('click','button',function(event){
		const buttClicke=$(this);
		$("#nouveauPartenaire").show();

		$("#confirmer_nouveau_partenaire").on('click',function(){
			if($('#pays_partenaire').val()=="" || $('#nomLegal_partenaire').val()=="" || $('#nomAffaires_partenaire').val()==""|| 
				$('#nomComplet_partenaire').val()=="" || $('#rue_partenaire').val()=="" 
				|| $('#numero_partenaire').val()=="" || $('#codePostal_partenaire').val()==""|| $('#ville_partenaire').val()==""
				|| $('#code_partenaire').val()=="" || $('#email_partenaire').val()=="" || $('#telephone_partenaire').val()==""){
				$("#alertEncodagePart").text("Vous devez remplir les champs suivis d'une '*'");
		} else {
			encoderNouveauPartenaire(buttClicke);
		}
	});

	});


	//lister toutes ses mobilites
	$('#button_demande_mes_mobilites').on('click',function(){
		var clicks = $(this).data('clicks');
		if (clicks) {
			if (nombre_mobi == 0){
				console.log(nombre_mobi);
				$('#alertViewMob').text("Vous n'avez pas de mobilite");
				return;
			}else{
				$("#tableMob").show();

			}
		} else {
			$("#tableMob").hide();
		}
		$(this).data("clicks", !clicks);
	});

	//conserve toute les lignes et ne sera plus modifiables pars apres
    //Pas fonctionnel
    $('#bouton_sauvegarde_demande').on('click',function(){
    	if($('#programme').val()=="" || $('#code').val()=="" || $('#pays').val()=="" || $('#ordre').val()==""){
    		$('#alertMob').text("Vous devez remplir les 4 premiers champs");
    	}else{
    		
    		$('#alertViewMobPasDeMob').text("");
    		declarer_demande();	
    	}
    	$('#inscription').hide();
    });

	$('#encoderNouvelleMobilite').on('click',function(){
		$('#encoderMobi').show();
	})

    /*ajoute des form row
    $("#addRow").click(function(){

    	if($('body').find('.form-row').length < maxForm){
    		var fieldHTML = '<div class="form-row">'+$(".form-row").html()+'</div>';
    		$('body').find('.form-row:last').after(fieldHTML);
    	}else{
    		alert('Le maximum ('+maxForm+') nombre de destination a été atteint');
    	}
    });*/

    FrontEnd.onDisplayPage("etudiant", function() {
		getPays();
		getProgramme();
		getPartenairesNom();
		getPartenairesCode();
		getMesMobilites();
		getPaysEncoderDonnees();
		getPaysEncoderPartenaires();
		
		$("#page-header").show();
		$("#footer").show();
		$("#nouveauPartenaire").hide();
    	$("#connexion-inscription").hide();
    	$('#inscription').hide();
    	$("#professeur").hide();
    	$("#professeur-responsable").hide();
		$('#page_recherche').hide();
		$('#voirUneMobi').hide();
		$("#page_infos").hide();


    })  
    FrontEnd.onLeavePage("etudiant",function(){
    	$("#etudiant").hide();
		$("#listeDemandeMobilite").empty();
		$("#voirMobTable").empty();
    })

});

/*function getMobilite(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{ action:"get_mobilite_etudiant_connecte"},
		success: function(resp){
			//$("#numVersionHtml").text(resp.numVersion);
			console.log(resp);
			return resp;
		},	
		error:function(e){
			console.log("echec getMobilite");
		}
	});
}*/


//annulerSaMobilite -> 
function annulerRefuserMobilite(_etat_mob){

	var _dernier_etat;
	var _num_version;
	var _id_mobilite_etudiante;
	if(_etat_mob==="refuse"){
		var _raison_annulation_refus=$('#raison_refus').val();
	}else{
		var _raison_annulation_refus=$('#raison_annulation').val();
	}
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"get_mobilite_etudiant_connecte"},
		success:function(resp){
			console.log(resp);
			_dernier_etat=resp[0].etat;
			_num_version=resp[0].numVersion;
			_id_mobilite_etudiante=resp[0].idMobiliteEtudiant;
			console.log(_etat_mob);
			$.ajax({
				url:'http://localhost:8680/api',
				type:'POST',
				dataType:'JSON',
				data:{action:"annuler_refuser_mobilite_etudiante",etat:_etat_mob,dernier_etat:_dernier_etat,num_version:_num_version,
				id_mobilite_etudiante:_id_mobilite_etudiante,raison_annulation_refus:_raison_annulation_refus},
				success:function(resp){
					$('#alertRefus').text("Vous avez refusé/annulé avec succès");
					$('#div_annulation').hide();
					$('#div_refus').hide();
				},
				error:function(e){
					console.log("erreur annuler/refuser");
				}
			});
		},
		error:function(e){
			
			console.log("pas de mobi confirme");
		}
	});
	
}


//Remplis la table des mobilites (fonction appellee a l'appuis sur le bouton)
function getMesMobilites(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{ action:"visualiser_ses_demandes"},
		success: function(resp){
			if(resp.length > 0){
				nombre_mobi = resp.length;
				//console.log('il  y a des mobilites');
				//console.log(nombre_mobi);
			}
			if(resp.length === 0){
				//console.log('il n y a pas de mobilites');
				//nombre_mobi = 0;
				//console.log(nombre_mobi);
				$('#alertViewMobPasDeMob').text("Vous n'avez pas de mobilité, veuillez introduire une demande");
			}
			for(let i=0;i<resp.length;i++){
				let nomAff=resp[i].partenaire.nomComplet;
				if(nomAff==null){
					nomAff="<button type=\"button\" class=\"btn btn-outline-secondary\" value=\""+i+"\">Encoder un nouveau partenaire</button>";
				}
				$("#listeDemandeMobilite").append(
					'<tr id=demMob'+i+'>' + 
					'<td name="idDem" style="display : none">' + resp[i].idDemande + '</td>' +
					'<td name="numVers" style="display : none">' + resp[i].numVersion + '</td>' +
					'<td name="idPers" style="display : none">' + resp[i].personne.idPersonne + '</td>' +
					'<td name="ordrePref">' + resp[i].ordrePreference + '</td>' +
					'<td name="programme">' + resp[i].programme + '</td>'+
					'<td name="code">' + resp[i].code + '</td>'+
					'<td name="pays">' + ((resp[i].pays === null) ? 'Non défini' : resp[i].pays.nom) + '</td>'+
					'<td name="quadri">' + resp[i].quadrimestre + '</td>'+
					'<td>' + nomAff + '</td>'+
					'</tr>'
					);
			}
		},	
		error:function(e){
			console.log("failure get ma mobilite");
		}
	});		
}

function getPartenairesNom(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"get_noms_partenaires"},
		success:function(resp){
			for(let i=0;i<resp.length;i++){
				$("#partExist").append($('<option>').attr('value',resp[i]).text(resp[i]));
			}
		},
		error:function(e){
			console.log("failure get nom Partenaire");
		}
	});
}
function getPartenairesCode(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"get_codes_partenaires"},
		success:function(resp){
			for(let i=0;i<resp.length;i++){
				$("#code").append($('<option>').attr('value',resp[i]).text(resp[i]));
			}
		},
		error:function(e){
			console.log("failure get code Partenaire");
		}
	});
}

function getPays(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'json',
		data:{action:"get_pays"},
		success:function(resp){
			for(let i=0;i<resp.length;i++){
				$("#pays").append($('<option>').attr('value',resp[i]).text(resp[i]));
			}
		},
		error:function(e){
			console.log("failure getPays");
		}
	});
}
function getPaysEncoderPartenaires(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'json',
		data:{action:"get_pays"},
		success:function(resp){
			for(let i=0;i<resp.length;i++){
				$("#pays_partenaire").append($('<option>').attr('value',resp[i]).text(resp[i]));
			}
		},
		error:function(e){
			console.log("failure getPaysEncoderPartenaire");
		}
	});
}
function getPaysEncoderDonnees(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'json',
		data:{action:"get_pays"},
		success:function(resp){
			for(let i=0;i<resp.length;i++){
				$("#nationalite_encoderDonnee").append($('<option>').attr('value',resp[i]).text(resp[i]));
			}
		},
		error:function(e){
			console.log("failure getPaysEncoderDonnees");
		}
	});
}
function getProgramme(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'json',
		data:{action:"get_programme"},
		success:function(resp){
			for(let i=0;i<resp.length;i++){
				$("#programme").append($('<option>').attr('value',resp[i]).text(resp[i]));
			}
		},
		error:function(e){
			console.log("failure getProgramme");
		}
	});
}
function declarer_demande(){
	var quadriResponse = $('#quadri').val();
	if($('#quadri').val()=="/"){
		quadriResponse = 0;
	}

	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		data:{
			action:"declarer_demande",
			ordre:$('#ordre').val(),
			programme:$('#programme').val(), 
			code:$('#code').val(),
			pays:$('#pays').val(),
			quadri:quadriResponse,
			partExistString:$('#partExist').val()
		},
		success:function(resp,status){
			console.log("déclaration de demande réussie");
			$('#alertMob').text("Demande envoyée!");
		},
		error:function(e){
			console.log(e.getMessage);
			console.log("FAIL DECLARATION DE DEMANDE");
			$('#alertMob').text("La demande n'a pas pu être envoyée!");
		}
	});
}


function annulerSaMobilite(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{ action:"indiquer_Annulation",
		raison:$('#raison_Annulation').val()
	},
	success: function(resp){
		console.log("OKAY indiquer refus");
	},	
	error:function(e){
		console.log("ECHEC indication du refus");
	}
});
}
function modifierEtatMobilite(){
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{ action:"modif_etat_mobi",
		raison:$('#raison_annulation_refus').val(),
		id_mobilite_etudiants:$('id_mobilite_etudiants').val(),
		etat:$('button_etat').val()
	},
	success: function(resp){
		console.log("OKAY modif etat");
	},	
	error:function(e){
		console.log("ECHEC modif etat");
	}
});
}

//Encoder un nouveau partenaire
function encoderNouveauPartenaire(bouton){
	let numLigne=bouton.val();
	let idMobR=$("#demMob"+numLigne).find('td[name="idDem"]').text();
	let quadriR=$("#demMob"+numLigne).find('td[name="quadri"]').text();
	let numVersR=$("#demMob"+numLigne).find('td[name="numVers"]').text();
	let idPersR=$("#demMob"+numLigne).find('td[name="idPers"]').text();


	let nbEmplR=$('#nbEmployees_partenaire').val();
	if(nbEmplR==''){
		nbEmplR = 0;
	}
	
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		data:{ action:"confirmer_demande_mobilite_avec_nouveau_partenaire",
		pays:$('#pays_partenaire').val(),
		nom_legal:$('#nomLegal_partenaire').val(),
		nom_affaires:$('#nomAffaires_partenaire').val(),
		nom_complet:$('#nomComplet_partenaire').val(),
		rue:$('#rue_partenaire').val(),
		numero:$('#numero_partenaire').val(),
		boite:$('#boite_partenaire').val(),
		code_postal:$('#codePostal_partenaire').val(),
		commune:$('#commune_partenaire').val(),
		region:$('#region_partenaire').val(),
		code:$('#code_partenaire').val(),
		type_organisation:$('#type_organisation_partenaire').val(),
		nb_employes:nbEmplR,
		email:$('#email_partenaire').val(),
		site_web:$('#siteWeb_partenaire').val(),
		telephone:$('#telephone_partenaire').val(),
		departement:$('#departement_partenaire').val(),
		ville:$('#ville_partenaire').val(),
		id_demande:idMobR,
		quadri:quadriR,
		num_version_demande_mobilite:numVersR,
		id_personne:idPersR
	},
	success: function(resp){
		$('#alertEncodagePart').text("Encodage effectué");
	},	
	error:function(e){
		$('#alertEncodagePart').text(e.responseText);
	}
});		
}



