FrontEnd.onInitPage("page_recherche",function(){
	
	/*
		-----------------------------------------------------------------------------------
		<------------Chargement des données utiles au chargement de la page--------------->
		-----------------------------------------------------------------------------------
	*/
	FrontEnd.onDisplayPage("page_recherche", function() {
		$("#page-header").show();
        $("#footer").show();
		$("#etudiant").hide();
		$("#connexion-inscription").hide();
		$('#inscription').hide();
		$("#professeur").hide();
	})  

	/*---------------------------------------------------------------------------------
	<------------Vidage des tableaux et des données au changement de page------------->
	----------------------------------------------------------------------------------*/
	FrontEnd.onLeavePage("page_recherche",function(){
		$("#page_recherche").hide();
		$("#gererRechercheUtilisateur").empty();
		$("#gererRecherchePartenaire").empty();
		$("#gererRechercheMobilite").empty();
	})


	/*---------------------------------------------------------------------------------
	<---------------------PARTIE AUTO-COMPLETION-------------------------------------->
	----------------------------------------------------------------------------------*/

	//-----------------Etudiant------------------
	var listeNomEtudiants = completionNomsEtudiants();
	$('#nomListeEtudiant').autocomplete({
		source : listeNomEtudiants
	});
	var listePrenomEtudiants = completionPrenomsEtudiants();
	$('#prenomListeEtudiant').autocomplete({
		source : listePrenomEtudiants
	});
	
	//-----------------Partenaire-------------
	var listeNomsPartenaires = completionNomsPartenaires();
	$('#nomListePartenaire').autocomplete({
		source : listeNomsPartenaires
	});
	var listePaysPartenaires = completionPaysPartenaires();
	$('#paysListePartenaire').autocomplete({
		source : listePaysPartenaires
	});
	var listeVillesPartenaires = completionVillesPartenaires();
	$('#villeListePartenaire').autocomplete({
		source : listeVillesPartenaires
	});

	//------------------Mobilité---------------
	var listeEtatsMobilite = [
		"cree",
		"en_preparation",
		"a_payer",
		"en_cours",
		"a_payer_solde",
		"annule",
		"refuse",
		"en_remboursement"
	];
	$('#etatListeMobilite').autocomplete({
		source : listeEtatsMobilite
	});
	//----------------------Fin autocompletion------------------------------------------------


	/*
	----------------------------------------------------------------------------------------------------------------------------
	<---------------------Partie JQUERY pour appeler les fonctions contenant des requetes AJAX---------------------------------->
	----------------------------------------------------------------------------------------------------------------------------
	*/

	//BOUTON POUR RECHERCHER UN ÉTUDIANT
	$('#bouton_recherche_etudiant').on('click',function(){
		console.log("bouton recherche cliqué");
    	if($('#nomListeEtudiant').val()=="" && $('#prenomListeEtudiant').val()==""){
    		$('#alertRechercherEtudiant').text("Vous devez remplir au moins 1 champ");
    	}else{
    		$('#alertRechercherEtudiant').text("");
    		rechercheEtudiant();	
    	}
	});
	
	//Rechercher un partenaire
	$('#bouton_recherche_partenaire').on('click',function(){
		console.log("bouton partenaire cliqué");
    	if($('#nomListePartenaire').val()=="" && $('#paysListePartenaire').val()=="" && $('#villeListePartenaire').val()==""){
    		$('#alertRechercherPartenaire').text("Vous devez remplir au moins 1 champ");
    	}else{
    		$('#alertRechercherPartenaire').text("");
    		recherchePartenaire();	
    	}
	});
	
	//Rechercher une mobilité
	$('#bouton_recherche_mobilite').on('click',function(){
		console.log("bouton mobilite cliqué");
    	if($('#anneeListeMobilite').val()=="" && $('#etatListeMobilite').val()==""){
    		$('#alertRechercherMobilite').text("Vous devez remplir au moins 1 champ");
    	}else{
    		$('#alertRechercherMobilite').text("");
    		rechercheMobilite();	
    	}
	});
	

}); // fin du onInitPage



/*----------------------------------------------------------------------------------------------------------------------------------
<----------------------------------------------------------------APPELS AJAX ------------------------------------------------------->
-----------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------
AJAX pour rechercher un étudiant
--------------------------------*/
function rechercheEtudiant(){
	
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"recherche_etudiant",			
			nom:$('#nomListeEtudiant').val(),
			prenom:$('#prenomListeEtudiant').val()
		},
		success:function(resp){
			console.log("Succès recherche étudiant");

			$("#gererRechercheUtilisateur").empty();
			for(let i=0;i<resp.length;i++){
				$("#gererRechercheUtilisateur").append(
					'<tr>' + 
					'<td>' + resp[i].civilite + '</td>' +
					'<td>' + resp[i].nom + '</td>' +
					'<td>' + resp[i].prenom + '</td>' +
					'<td>' + resp[i].pseudo + '</td>' +
					'<td>' + resp[i].sexe + '</td>'+
					'<td>' + resp[i].dateNaissance + '</td>' +
					'<td>' + resp[i].email + '</td>' +
					'<td>' + resp[i].departement + '</td>' +
					'</tr>'
					);
			} if(resp.length === 0){
				$('#alertPasEtudiant').text("Aucun étudiant n'a été trouvé");
			}

		},
		error:function(e){
			console.log("Erreur recherche étudiant");
		}
	});
}

/*-------------------------------------------------------------
AJAX pour rechercher un partenaire et afficher ses informations
--------------------------------------------------------------*/
function recherchePartenaire(){
	
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"recherche_partenaire",			
			nom:$('#nomListePartenaire').val(),
			nom_pays:$('#paysListePartenaire').val(),
			ville:$('#villeListePartenaire').val()
		},
		success:function(resp){
			console.log("Succès recherche partenaire");
			$("#gererRecherchePartenaire").empty();
			for(let i=0;i<resp.length;i++){
				$("#gererRecherchePartenaire").append(
					'<tr>' + 
					'<td>' + resp[i].nomComplet + '</td>' +
					'<td>' + resp[i].pays.nom + '</td>' +
					'<td>' + resp[i].email + '</td>' +
					'<td>' + resp[i].departement + '</td>' +
					'<td>' + resp[i].telephone + '</td>' +
					'<td>' + resp[i].code + '</td>' +
					'</tr>'
					);
			}if(resp.length === 0){
				$('#alertPasPartenaire').text("Aucun partenaire n'a été trouvé");
			}
		},
		error:function(e){
			$('#alertPasPartenaire').text("Aucun partenaire n'a été trouvé");
			console.log("Erreur recherche partenaire");
		}
	});
}

/*------------------------------------------------------------
AJAX pour rechercher une mobilité et afficher ses informations
-------------------------------------------------------------*/
function rechercheMobilite(){
	let anneeR = $('#anneeListeMobilite').val();
	if(anneeR == ""){
		anneeR = 0;
	}
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"recherche_mobilite",			
			annee_academique:anneeR,
			etat:$('#etatListeMobilite').val()
		},
		success:function(resp){
			console.log("Succès recherche mobilite");
			$("#gererRechercheMobilite").empty();
			for(let i=0;i<resp.length;i++){
				$("#gererRechercheMobilite").append(
					'<tr>' + 
					'<td>' + resp[i].demande.anneeAcademique + '</td>' +
					'<td>' + resp[i].personne.pseudo + '</td>' +
					'<td>' + resp[i].demande.pays.nom + '</td>' +
					'<td>' + resp[i].partenaire.adresse.ville + '</td>' +
					'<td>' + resp[i].partenaire.nomComplet + '</td>'+
					'<td>' + resp[i].etat + '</td>' +
					'</tr>'
					);
			}
			if(resp.length === 0){
				$('#alertPasMobi').text("Aucune mobilité n'a été trouvée");
			}
		},
		error:function(e){
			console.log("Erreur recherche mobilite");
		}
	});
}
//-------------------fin des methodes de recherche-----------------------




/*
	-------------------------------------------------------------------------------------------------------------------------------------
	<------------------------------------------------------MÉTHODES D'AUTOCOMPLETION----------------------------------------------------->
	-------------------------------------------------------------------------------------------------------------------------------------
*/

/*---------------------------------------
Auto-completion pour le nom d'un étudiant
-----------------------------------------*/
function completionNomsEtudiants(){
	var listeNomEtudiants = [];
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"completion_nom_prenom_etudiant_recherche"			
		},
		success:function(resp){
			console.log("Succès completion étudiant");
			for(let i=0;i<resp.length;i++){
				listeNomEtudiants[i] = resp[i].nom;
			}
		},
		error:function(e){
			console.log("Erreur de completion etudiant");
		}
	});

	return listeNomEtudiants;
}

/*---------------------------------------
Auto-completion pour le prénom d'un étudiant
-----------------------------------------*/
function completionPrenomsEtudiants(){
	var listePrenomsEtudiants = [];
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"completion_nom_prenom_etudiant_recherche"			
		},
		success:function(resp){
			console.log("Succès completion étudiant");
			for(let i=0;i<resp.length;i++){
				listePrenomsEtudiants[i] = resp[i].prenom;
			}
		},
		error:function(e){
			console.log("Erreur de completion etudiant");
		}
	});

	return listePrenomsEtudiants;
}

/*---------------------------------------
Auto-completion pour le nom d'un partenaire
-----------------------------------------*/
function completionNomsPartenaires(){
	var listeNomsPartenaires = [];
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"completion_nom_partenaire_pays_ville_recherche"			
		},
		success:function(resp){
			console.log("Succès completion partenaire");
			for(let i=0;i<resp.length;i++){
				listeNomsPartenaires[i] = resp[i].nomComplet;
			}
		},
		error:function(e){
			console.log("Erreur de completion partenaire");
		}
	});

	return listeNomsPartenaires;
}

/*-----------------------------------------------
Auto-completion pour le pays / section partenaire
------------------------------------------------*/
function completionPaysPartenaires(){
	var listePaysPartenaires = [];
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"completion_nom_partenaire_pays_ville_recherche"			
		},
		success:function(resp){
			console.log("Succès completion partenaire");
			for(let i=0;i<resp.length;i++){
				listePaysPartenaires[i] = resp[i].pays.nom;
			}
		},
		error:function(e){
			console.log("Erreur de completion partenaire");
		}
	});

	return listePaysPartenaires;
}

/*------------------------------------------------
Auto-completion pour la ville / section partenaire
--------------------------------------------------*/
function completionVillesPartenaires(){
	var listeVillesPartenaires = [];
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"completion_nom_partenaire_pays_ville_recherche"			
		},
		success:function(resp){
			console.log(resp);
			console.log("Succès completion partenaire");
			for(let i=0;i<resp.length;i++){
				listeVillesPartenaires[i] = resp[i].adresse.ville;
			}
		},
		error:function(e){
			console.log("Erreur de completion partenaire");
		}
	});

	return listeVillesPartenaires;
}
//-------------------fin des méthodes d'autocompletion---------------------------




