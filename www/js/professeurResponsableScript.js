FrontEnd.onInitPage("professeur-responsable",function(){
	
	/*---------------------------------------------------------------------------------------------------------------------------
	<---------------------Partie JQUERY pour appeler les fonctions contenant des requetes AJAX----------------------------------->
	----------------------------------------------------------------------------------------------------------------------------*/

	//BOUTON POUR APPELLER LA MÉTHODE QUI VA AUGMENTER LE PROFESSEUR
	//regarde si le champ du mail est rempli
	$("#boutonAugmenterProf").on('click', function(){
		if($("#inputAugmenterProf").val()==""){
			$("#alertpromotionProf").text("Vous devez rentrer un email.");
		} else {
			augmenterProfesseur();
		}
	});

	/*---------------------------------------------------------------------------------
	<------------Chargement des données utiles au chargement de la page--------------->
	---------------------------------------------------------------------------------*/
	FrontEnd.onDisplayPage("professeur-responsable", function() {
		$("#page-header").show();
		$("#footer").show();
		$("#etudiant").hide();
		$("#connexion-inscription").hide();
		$('#inscription').hide();
		$("#professeur").hide();
		$('#page_recherche').hide();
	})  
	/*--------------------------------------------------------------------------------
	<-----------------------Cache la vue professeur respaonsable--------------------->
	--------------------------------------------------------------------------------*/
	FrontEnd.onLeavePage("professeur-responsable",function(){
		$("#professeur-responsable").hide();
	})
});


/*
	-------------------------------------------------------------------------------------------------------------------------------------
	<----------------------------------------------------------------APPELS AJAX ------------------------------------------------------->
	-------------------------------------------------------------------------------------------------------------------------------------
*/

/*------------------------------------------------------------------------
AJAX pour augmenter le rôle d'un professeur en tant que "prof_responsable"
--------------------------------------------------------------------------*/
function augmenterProfesseur(){
	console.log("augmentation entrée");
	$.ajax({
		url:'http://localhost:8680/api',
		type:'POST',
		dataType:'JSON',
		data:{action:"augmenter_professeur",email:$('#inputAugmenterProf').val()},
		success:function(resp){
			$("#alertpromotionProf").text(resp.prenom + " " + resp.nom +" est maintenant professeur.");
		},
		error:function(e){
			$("#alertpromotionProf").text(e.responseText);
		}
	});
}