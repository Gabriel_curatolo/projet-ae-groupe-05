drop schema projetpae cascade;
create schema projetpae;
CREATE TABLE projetpae.adresses(
	id_adresse serial primary key,
	rue varchar(255) not null,
	numero int not null,
	code_postal varchar(20) not null,
	ville varchar(50) not null,
	commune varchar(50),
	boite varchar(50),
	region varchar(50)
); 

CREATE TABLE projetpae.pays (
	code_iso char(3) primary key,
	nom_pays varchar(50) not null, 
	programme_pays varchar(50) not null
); 

CREATE TABLE projetpae.personnes (
	id_personne serial primary key,
	pseudo varchar(50) not null,
	nom_personne varchar(50) not null,
	prenom varchar(50) not null,
	email_personne varchar(50) not null,
	mot_de_passe varchar(255) not null, 
	telephone_personne varchar(50),
	date_inscription timestamp not null,
	role varchar(2) not null,
	civilite varchar(50),
	date_naissance varchar(50),
	sexe varchar(1),
	no_compte varchar(50),
	nb_annees_reussies int,
	titulaire_compte varchar(50),
	banque varchar(50),
	code_BIC varchar(30),
	departement_personne varchar(50),
	nationalite varchar(3) references projetpae.pays(code_iso),
	adresse int references projetpae.adresses(id_adresse),
	num_version_personne int not null
);

CREATE TABLE projetpae.partenaires ( 
	id_partenaire serial primary key, 
	pays varchar(3) references projetpae.pays(code_iso) not null,
	adresse int references projetpae.adresses(id_adresse) not null, 
	nom_legal varchar(255) not null, 
	nom_affaires varchar(255) not null,
	nom_complet varchar(255) not null, 
	code_partenaire varchar(3) not null, 
	type_organisation varchar(3), 
	departement_partenaire varchar(255),
	nb_employes int, 
	email_partenaire varchar(50) not null, 
	site_web varchar(255), 
	telephone_partenaire varchar(50) not null
);

CREATE TABLE projetpae.demandes_mobilite (
	id_demande serial primary key, 
	id_partenaire int references projetpae.partenaires(id_partenaire), 
	id_personne int references projetpae.personnes(id_personne) not null, 
	pays char(3) references projetpae.pays(code_iso), 
	confirme boolean not null, 
	date_introduction timestamp not null, 
	programme_demande varchar(50) not null, 
	code_demande varchar(3) not null, 
	quadrimestre_demande int, 
	ordre_preference int, 
	annee_academique int not null, 
	num_ordre_candidature int,
	num_version_demande_mobilite int not null
); 

CREATE TABLE projetpae.mobilite_etudiants (
	id_mobilite_etudiants serial primary key, 
	id_demande int references projetpae.demandes_mobilite(id_demande) not null, 
	id_personne int references projetpae.personnes(id_personne) not null, 
	id_partenaire int references projetpae.partenaires(id_partenaire) not null, 
	etat varchar(50) not null, 
	quadrimestre_mobilite int not null, 
	envoie_demande_paiement1 boolean not null,  
	envoie_demande_paiement2 boolean not null, 
	contrat_bourse boolean not null, 
	convention_stage_etude boolean not null,
	charte_etudiant boolean not null, 
	preuve_test_linguistique boolean, 
	document_engagement boolean not null, 
	encode_pro_eco boolean not null, 
	encode_mobility_tool boolean not null,
	encode_mobi boolean not null,  
	attestation_sejour_recu boolean not null, 
	releve_note_recu boolean not null, 
	certification_stage_recu boolean not null, 
	preuve_test_linguistique_retour boolean, 
	rapport_final boolean not null, 
	dernier_etat varchar(50), 
	raisons_annulation_refus varchar(255),
	num_version_mobilite_etudiants int not null

); 


INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CHE','Suisse','Autre'); 
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BEL','Belgique','Erabel');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('DEU','Allemagne','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('AUT','Autriche','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ESP','Espagne','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('EST','Estonie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('FRA','France','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('IRL','Irlande','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ISL','Islande','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ITA','Italie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LVA','Lettonie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LUX','Luxembourg','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MKD','Macédoine','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MLT','Malte','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NOR','Norvège','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NLD','Pays-Bas','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('POL','Pologne','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PRT','Portugal','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('REU','Réunion','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ROU','Roumanie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GBR','Royaume Uni','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SRB','Serbie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SVK','Slovaquie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SVN','Slovénie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SWE','Suède','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TUR','Turquie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BGR','Bulgarie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CYP','Chypre','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('HRV','Croatie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('DNK','Danemark','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('FIN','Finlande','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GRC','Grèce','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('HUN','Hongrie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LIE','Liechtenstein','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LTU','Lituanie','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CZE','République Tchèque','Erasmus+');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('AFG','Afghanistan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ZAF','Afrique du Sud','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ALB','Albanie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('DZA','Algerie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('AND','Andorre','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('AGO','Angola','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('AIA','Anguilla','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ATA','Antarctique','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ATG','Antigua et Barbuda','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ANT','Antilles Néerlandaises','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SAU','Arabie Saoudite','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ARG','Argentine','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ARM','Arménie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ABW','Aruba','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('AUS','Australie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('AZE','Azerbaidjan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BHS','Bahamas','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BHR','Bahrein','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BGD','Bangladesh','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BRB','Barbade','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BLR','Bélarus (Biélorussie)','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BLZ','Belize','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BEN','Bénin','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BMU','Bermudes','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BTN','Bhoutan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BOL','Bolivie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BIH','Bosnie et Herzégovine','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BWA','Botswana','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BVT','Bouvet Island','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BRA','Brésil','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BRN','Brunei','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BFA','Burkina Faso','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BDI','Burundi','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('KHM','Cambodge','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CMR','Cameroun','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CAN','Canada','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CPV','Cap Vert','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CHL','Chili','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CHN','Chine','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('VAT','Cité du Vatican (Holy See)','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('COL','Colombie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('COM','Comores','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('COG','Congo, République','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('COD','Congo, République Démocratique du','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PRK','Corée du Nord','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('KOR','Corée du Sud','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CRI','Costa Rica','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CIV','Côte d Ivoire','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CUB','Cuba','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CUW','Curacao','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('DJI','Djibouti','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('DMA','Dominique','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('EGY','Egypte','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ARE','Emirats Arabes Unis','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ECU','Equateur','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ERI','Erythrée','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('USA','Etats-Unis','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ETH','Ethiopie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('FJI','Fidji','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('FXX','France, Métropolitaine','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GAB','Gabon','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GMB','Gambie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PSE','Gaza','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GEO','Géorgie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SGS','Géorgie du Sud et les îles Sandwich du Sud','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GHA','Ghana','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GIB','Gibraltar','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GRD','Grenade','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GRL','Greoenland','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GLP','Guadeloupe','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GUM','Guam','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GTM','Guatemala','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GIN','Guinée','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GNB','Guinée Bissau','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GNQ','Guinée Equatoriale','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GUY','Guyane','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GUF','Guyane Française','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('HTI','Haïti','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('HND','Honduras','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('HKG','Hong Kong','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('IMN','Ile de Man','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CYM','Iles Caïman','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CXR','Iles Christmas','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CCK','Iles Cocos','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('COK','Iles Cook','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('FRO','Iles Féroé','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('GGY','Iles Guernesey','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('HMD','Iles Heardet McDonald','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('FLK','Iles Malouines','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MNP','Iles Mariannes du Nord','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MHL','Iles Marshall','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MUS','Iles Maurice','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('UMI','Iles mineures éloignées des Etats-Unis','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NFK','Iles Norfolk','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SLB','Iles Salomon','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TCA','Iles Turques et Caïque','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('VIR','Iles Vierges des Etats-Unis','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('VGB','Iles Vierges du Royaume-Uni','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('IND','Inde','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('IDN','Indonésie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('IRN','Iran','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('IRQ','Iraq','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ISR','Israël','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('JAM','Jamaique','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('JPN','Japon','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('JEY','Jersey','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('JOR','Jordanie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('KAZ','Kazakhstan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('KEN','Kenya','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('KGZ','Kirghizistan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('KIR','Kiribati','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('XKO','Kosovo','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('KWT','Koweit','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LAO','Laos','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LSO','Lesotho','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LBN','Liban','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LBR','Libéria','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LBY','Libye','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MAC','Macao','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MDG','Madagascar','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MYS','Malaisie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MWI','Malawi','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MDV','Maldives','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MLI','Mali','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MAR','Maroc','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MTQ','Martinique','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MRT','Mauritanie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MYT','Mayotte','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MEX','Mexique','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('FSM','Micronésie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MDA','Moldavie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MCO','Monaco','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MNG','Mongolie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MNE','Monténégro','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MSR','Montserrat','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MOZ','Mozambique','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MMR','Myanmar (Birmanie)','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NAM','Namibie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NRU','Nauru','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NPL','Népal','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NIC','Nicaragua','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NER','Niger','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NGA','Nigeria','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NIU','Niue','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NCL','Nouvelle Calédonie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('NZL','Nouvelle Zélande','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('OMN','Oman','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('UGA','Ouganda','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('UZB','Ouzbékistan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PAK','Pakistan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PLW','Palau','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PAN','Panama','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PNG','Papouasie Nouvelle Guinée','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PRY','Paraguay','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PER','Pérou','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PHL','Philippines','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PCN','Pitcairn','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PYF','Polynésie Française','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('PRI','Porto Rico','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('QAT','Qatar','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('CAF','République Centraficaine','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('DOM','République Dominicaine','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('RUS','Russie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('RWA','Rwanda','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ESH','Sahara Occidental','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('BLM','Saint Barthelemy','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SHN','Saint Hélène','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('KNA','Saint Kitts et Nevis','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('MAF','Saint Martin','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SXM','Saint Martin','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SPM','Saint Pierre et Miquelon','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('VCT','Saint Vincent et les Grenadines','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LCA','Sainte Lucie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SLV','Salvador','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ASM','Samoa Americaines','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('WSM','Samoa Occidentales','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SMR','San Marin','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('STP','Sao Tomé et Principe','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SEN','Sénégal','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SYC','Seychelles','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SLE','Sierra Léone','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SGP','Singapour','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SOM','Somalie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SDN','Soudan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('LKA','Sri Lanka','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SSD','Sud Soudan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SUR','Surinam','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SJM','Svalbard et Jan Mayen','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SWZ','Swaziland','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('SYR','Syrie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TJK','Tadjikistan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TWN','Taiwan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TZA','Tanzanie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TCD','Tchad','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ATF','Terres Australes et Antarctique Françaises','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('IOT','Térritoire Britannique de l Océan Indien','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('THA','Thaïlande','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TLS','Timor-Leste','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TGO','Togo','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TKL','Tokelau','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TON','Tonga','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TTO','Trinité et Tobago','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TUN','Tunisie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TKM','Turkmenistan','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('TUV','Tuvalu','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('UKR','Ukraine','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('URY','Uruguay','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('VUT','Vanuatu','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('VEN','Venezuela','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('VNM','Vietnam','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('WLF','Wallis et Futuna','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('YEM','Yemen','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ZMB','Zambie','FAME');
INSERT INTO projetpae.pays (code_iso, nom_pays, programme_pays) values ('ZWE','Zimbabwe','FAME');


INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('Abdij van Park', '9', '3001', 'Heverlee', null, null, null);										
INSERT INTO projetpae.partenaires(pays, adresse, nom_legal, nom_affaires, nom_complet, code_partenaire, type_organisation, departement_partenaire, nb_employes, email_partenaire, site_web, telephone_partenaire) VALUES('BEL', '1', 'UC Leuven-Limburg (Leuven campus)', 'UC Leuven-Limburg (Leuven campus)', 'UC Leuven-Limburg (Leuven campus)', 'SMS',null, 'Computer Sciences', null, 'international@ucll.be', 'https://www.ucll.be/', '+32 (0)16 375 735');

INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('Mount Joy Square', '40', 'Dublin 1', 'Dublin', null, null, null);										
INSERT INTO projetpae.partenaires(pays, adresse, nom_legal, nom_affaires, nom_complet, code_partenaire, type_organisation, departement_partenaire, nb_employes, email_partenaire, site_web, telephone_partenaire) VALUES('IRL', '2', 'Technological University Dublin', 'Technological University Dublin', 'Technological University Dublin', 'SMS',null, 'Computing', null, 'erasmus@dit.ie', 'http://www.dit.ie/computing/', '+35314023404');

INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('Avenue des Hauts-Fourneaux', '7', 'L-4362', 'Esch-sur-Alzette', null, null, null);										
INSERT INTO projetpae.partenaires(pays, adresse, nom_legal, nom_affaires, nom_complet, code_partenaire, type_organisation, departement_partenaire, nb_employes, email_partenaire, site_web, telephone_partenaire) VALUES('LUX', '3', 'Université du Luxembourg', 'Université du Luxembourg', 'Université du Luxembourg', 'SMS',null, 'Computing', null, 'erasmus@uni.lu', 'https://wwwfr.uni.lu/', '(+352) 46 66 44 4000');

INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('Max-Planck-Str', '15', '97204', 'Höchberg', null, null, null);										
INSERT INTO projetpae.partenaires(pays, adresse, nom_legal, nom_affaires, nom_complet, code_partenaire, type_organisation, departement_partenaire, nb_employes, email_partenaire, site_web, telephone_partenaire) VALUES('DEU', '4', 'Wölfel Engineering GmbH', 'Wölfel Engineering GmbH', 'Wölfel Engineering GmbH', 'SMP',null, 'Data processing and analytics', null, 'tr@woelfel.de ', 'https://www.woelfel.de/home.html', '+49 (931) 49708-168');

INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('Rue de la Tambourine', '17', '1227', 'Carouge-Genève', null, null, null);										
INSERT INTO projetpae.partenaires(pays, adresse, nom_legal, nom_affaires, nom_complet, code_partenaire, type_organisation, departement_partenaire, nb_employes, email_partenaire, site_web, telephone_partenaire) VALUES('CHE', '5', 'HES-SO Haute école spécialisée de Suisse occidentale ', 'HES-SO Haute école spécialisée de Suisse occidentale ', 'HES-SO Haute école spécialisée de Suisse occidentale ', 'SMS',null, 'Business Information Systems', null, 'international@hes-so.ch', null, '0041 22 388 17 00');

INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('Chemin de Chambly', '945', 'JH4 3M6', 'Longueuil', null, null, 'Québec');										
INSERT INTO projetpae.partenaires(pays, adresse, nom_legal, nom_affaires, nom_complet, code_partenaire, type_organisation, departement_partenaire, nb_employes, email_partenaire, site_web, telephone_partenaire) VALUES('CAN', '6', 'cégep Edouard Montpetit', 'cégep Edouard Montpetit', 'cégep Edouard Montpetit', 'SMS',null, 'Techniques de l informatique', null, 'mobilites@cegepmontpetit.ca ', 'http://www.cegepmontpetit.ca/', '450 679-2631');

INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('rue Sherbrooke Est ', '3800', 'H1X 2A2', 'Montréal', null, null, 'Québec');										
INSERT INTO projetpae.partenaires(pays, adresse, nom_legal, nom_affaires, nom_complet, code_partenaire, type_organisation, departement_partenaire, nb_employes, email_partenaire, site_web, telephone_partenaire) VALUES('CAN', '7', 'Collège de Maisonneuve', 'Collège de Maisonneuve', 'Collège de Maisonneuve', 'SMS',null, 'Techniques de l informatique', null, 'international@cmaisonneuve.qc.ca ', 'https://www.cmaisonneuve.qc.ca/ ', '514 254-7131');

INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('Jacques-Cartier Est', '534', 'G7H 1Z6', 'Chicoutimi', null, null, 'Québec');										
INSERT INTO projetpae.partenaires(pays, adresse, nom_legal, nom_affaires, nom_complet, code_partenaire, type_organisation, departement_partenaire, nb_employes, email_partenaire, site_web, telephone_partenaire) VALUES('CAN', '8', 'Cégep de Chicoutimi', 'Cégep de Chicoutimi', 'Cégep de Chicoutimi', 'SMS',null, 'Techniques de l informatique', null, 'mobilitesetudiantes@cchic.ca ', 'https://cchic.ca', '418 549.9520  | Poste 1144');


INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('bri', 'Lehmann', 'Brigitte', 'brigitte.lehmann@vinci.be', '$2a$10$Qvz/.XSJbzy7OEYFbKN7IO/o6zrhZfmo3gh4EoDEEbkur3LFkrUiG','10/06/2018', 'pr', 1);
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('lau', 'Leleux', 'Laurent', 'laurent.leleux@vinci.be', '$2a$10$YeR..I4oUseU/7GNpHQycuHLEdyjYx0psVjHM8Gom39/C4RdmBlme','10/06/2018', 'p', 1);
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('chri', 'Damas', 'Christophe', 'christophe.damas@vinci.be', '$2a$10$YeR..I4oUseU/7GNpHQycuHLEdyjYx0psVjHM8Gom39/C4RdmBlme','10/06/2018', 'et', 1);
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('achil', 'Ile', 'Achille', 'ach.ile@vinci.be', '$2a$10$tFJRTBjxHK.EmTHnapk5WOC9UREUbUzqlPG6YxHuel6x2a4fst7Ee','10/06/2018', 'et', 1);
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('bazz', 'Ile', 'Basile', 'bas.ile@vinci.be', '$2a$10$j5NFy5CNeCSfMeoTmkcN9OWm9.cyVLKWYiliXNsVqv3LRmnBKvQJm','10/06/2018', 'et', 1);

INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('rue des coteaux', '27', '1300', 'Limal', null, null, null);										
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, telephone_personne, date_inscription, role, civilite, date_naissance, sexe, no_compte, nb_annees_reussies, titulaire_compte, banque, code_BIC, departement_personne, nationalite, adresse, num_version_personne) VALUES ('caro', 'Line', 'Caroline', 'car.lin@vinci.be', '$2a$10$yxa/6avfyA2BQ0GbudbVy.unBXDKrnbA/w2iFL/lGOS3KBivNn2xy','0476354921', '13/05/2018','et','Mme','13/05/1996', 'F', 'BE31 0000 0000 5555', 2, 'Caroline Line', 'Banque de la poste', 'BPOTBEB1', 'Informatique de Gestion', 'BEL', 9, 1 );

INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('laurence', 'Lasage', 'Laurence', 'lau.sage@vinci.be', '$2a$10$7EV.8TFwPLJk6FMkTbddU.47NNQU1XoUQW.5rSjGtEDi9mjox49TC', '10/06/2018','et', 1);
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('pir', 'Kiroule', 'Pierre', 'pir.pas@vinci.be', '$2a$10$AclJYGrGYBl3dNpwDAHx5ePjLd0ihfBTCB325XqKzBzfv728rNptm', '10/06/2018','et', 1);
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('mousse', 'Namassepas', 'Mousse', 'nam.mas@vinci.be', '$2a$10$Eop1O4C0nB19hKuxAnwNW.9bYERDNkG93sbTcnDzC7lkf6QzrPqn6', '10/06/2018','et', 1);
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('the', 'Tatilotetatou', 'Tonthe', 'ton.the@vinci.be', '$2a$10$KPDzpK4hR3tunqnVRBINJ.PD0W.kQ38NjxUAqiOfL8sPzGoo/NP5e','10/06/2018', 'et', 1);
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('albert', 'Frair', 'Albert', 'al.fr@vinci.be', '$2a$10$v2ADaR1rxNPZSlzhbt5OjOQZK5Czvd9qLbELdhUeojwtK2iat9zNq', '10/06/2018','et', 1);
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, num_version_personne) VALUES ('alph', 'Albert', 'Alpha', 'al.pha@vinci.be', '$2a$10$YolCHWVuspjeRzTMZHwWu.1Wqe3CmZqcA22hJ0ShbtwwHuCwCPwGu', '10/06/2018','et', 1);

INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('rue des soucis', '27', '6150', 'Anderlues', null, null, null);										
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, telephone_personne, date_inscription, role, civilite, date_naissance, sexe, no_compte, nb_annees_reussies, titulaire_compte, banque, code_BIC, departement_personne, nationalite, adresse, num_version_personne) VALUES ('louis', 'Albert', 'Louis', 'lo.ouis@vinci.be', '$2a$10$kDjQ6.eQ6NdhnJplhz2O0OEq0Al.Q82JDSWU1jh7ikklHXlntgWI2','0475895623', '13/05/2018','et','M.','12/09/1995', 'M', 'BE73 0000 0000 6060', 2, 'Louis Albert', 'Banque de la poste', 'BPOTBEB1', 'Informatique de Gestion', 'BEL', 10, 1 );

INSERT INTO projetpae.adresses(rue, numero, code_postal, ville, commune, boite, region) Values ('rue du lac', '25', '1348', 'Louvain-la-Neuve', null, 'B2', null);										
INSERT INTO projetpae.personnes(pseudo, nom_personne, prenom, email_personne, mot_de_passe, date_inscription, role, civilite, date_naissance, sexe, no_compte, nb_annees_reussies, titulaire_compte, banque, code_BIC, departement_personne, nationalite, adresse, num_version_personne) VALUES ('theo', 'Legrand', 'Théophile', 'theo.phile@vinci.be', '$2a$10$QBrk7a2isoG/6Yr/UsRzfORGm94RXP5EyDDQb6ixrHqRbFIBCu4H2', '13/05/2018','et','M.','10/12/1995', 'M', 'BE71 2100 6676', 3, 'Théophile Legrand', 'BNP PARIBAS FORTIS', 'GEBABEBB', 'Informatique de Gestion', 'BEL', 11, 1 );

INSERT INTO projetpae.demandes_mobilite(id_partenaire, id_personne, pays, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique, num_ordre_candidature, num_version_demande_mobilite) VALUES(2, 6, 'IRL', true, '25/03/2018', 'Erasmus+', 'SMS', 1, 1, 2019, 1, 1);
INSERT INTO projetpae.mobilite_etudiants(id_demande, id_personne, id_partenaire, etat, quadrimestre_mobilite, envoie_demande_paiement1, envoie_demande_paiement2, contrat_bourse, convention_stage_etude, charte_etudiant, preuve_test_linguistique, document_engagement, encode_pro_eco, encode_mobility_tool, encode_mobi, attestation_sejour_recu, releve_note_recu, certification_stage_recu, preuve_test_linguistique_retour, rapport_final, num_version_mobilite_etudiants) VALUES (1, 6, 2, 'en_cours', 1, true, false, true, true, true, true, true, false, false, false, false, false, false, false, false, 1);

INSERT INTO projetpae.demandes_mobilite(id_partenaire, id_personne, pays, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique, num_ordre_candidature, num_version_demande_mobilite) VALUES(3, 6, 'LUX', false, '25/03/2017', 'Erasmus+', 'SMS', 0, 2, 2018, 1, 1);
INSERT INTO projetpae.demandes_mobilite(id_personne, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique,num_ordre_candidature, num_version_demande_mobilite) VALUES(6, false, '25/03/2017', 'FAME', 'SMP', 0, 3, 2018, 1, 1);
INSERT INTO projetpae.demandes_mobilite(id_personne, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique,num_ordre_candidature, num_version_demande_mobilite) VALUES(4, false, '25/03/2017', 'FAME', 'SMP', 0, 1, 2018, 2, 1);

INSERT INTO projetpae.demandes_mobilite(id_personne, pays, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique,num_ordre_candidature, num_version_demande_mobilite) VALUES(4,'NZL', false, '25/03/2017', 'FAME', 'SMS', 0, 1, 2018, 3, 1);
INSERT INTO projetpae.demandes_mobilite(id_partenaire, id_personne, pays, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique, num_ordre_candidature, num_version_demande_mobilite) VALUES(2, 14, 'IRL', true, '25/03/2018', 'Erasmus+', 'SMS', 1, 1, 2019, 3, 1);
INSERT INTO projetpae.mobilite_etudiants(id_demande, id_personne, id_partenaire, etat, quadrimestre_mobilite, envoie_demande_paiement1, envoie_demande_paiement2, contrat_bourse, convention_stage_etude, charte_etudiant, preuve_test_linguistique, document_engagement, encode_pro_eco, encode_mobility_tool, encode_mobi, attestation_sejour_recu, releve_note_recu, certification_stage_recu, preuve_test_linguistique_retour, rapport_final, num_version_mobilite_etudiants) VALUES (6, 14, 2, 'en_preparation', 1, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, 1);


INSERT INTO projetpae.demandes_mobilite(id_partenaire, id_personne, pays, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique, num_ordre_candidature, num_version_demande_mobilite) VALUES(5, 9, 'CHE', false, '25/03/2018', 'Autre', 'SMS', 0, 1, 2019, 4, 1);

INSERT INTO projetpae.demandes_mobilite(id_partenaire, id_personne, pays, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique, num_ordre_candidature, num_version_demande_mobilite) VALUES(1, 10, 'BEL', false, '25/03/2018', 'Erabel', 'SMS', 0, 1, 2019, 5, 1);
INSERT INTO projetpae.demandes_mobilite(id_personne, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique, num_ordre_candidature, num_version_demande_mobilite) VALUES(10, false, '25/03/2018', 'Erasmus+', 'SMP', 0, 2, 2019, 5, 1);

INSERT INTO projetpae.demandes_mobilite(id_partenaire, id_personne, pays, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique,num_ordre_candidature, num_version_demande_mobilite) VALUES(3, 13, 'LUX', true,'25/03/2018', 'Erasmus+', 'SMS', 1, 1, 2019, 6, 1);
INSERT INTO projetpae.mobilite_etudiants(id_demande, id_personne, id_partenaire, etat, quadrimestre_mobilite, envoie_demande_paiement1, envoie_demande_paiement2, contrat_bourse, convention_stage_etude, charte_etudiant, preuve_test_linguistique, document_engagement, encode_pro_eco, encode_mobility_tool, encode_mobi, attestation_sejour_recu, releve_note_recu, certification_stage_recu, preuve_test_linguistique_retour, rapport_final, num_version_mobilite_etudiants) VALUES (10, 13, 3, 'a_payer', 1, false, false, true, true, true, true, true, false, false, false, false, false, false, false, false, 1);


INSERT INTO projetpae.demandes_mobilite(id_partenaire, id_personne, pays, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique,num_ordre_candidature, num_version_demande_mobilite) VALUES(6, 13, 'CAN', true,'25/03/2018', 'FAME', 'SMS', 2, 2, 2019, 6, 1);
INSERT INTO projetpae.mobilite_etudiants(id_demande, id_personne, id_partenaire, etat, quadrimestre_mobilite, envoie_demande_paiement1, envoie_demande_paiement2, contrat_bourse, convention_stage_etude, charte_etudiant, document_engagement, encode_pro_eco, encode_mobility_tool, encode_mobi, attestation_sejour_recu, releve_note_recu, certification_stage_recu, rapport_final, num_version_mobilite_etudiants) VALUES (11, 13, 6, 'Créée', 2, false, false, false, false, false, false, false, false, false, false, false, false, false, 1);


INSERT INTO projetpae.demandes_mobilite(id_partenaire, id_personne, pays, confirme, date_introduction, programme_demande, code_demande, quadrimestre_demande, ordre_preference, annee_academique,num_ordre_candidature, num_version_demande_mobilite) VALUES(7, 13, 'CAN', false,'25/03/2018', 'FAME', 'SMS', 0, 3, 2019, 6, 1);




