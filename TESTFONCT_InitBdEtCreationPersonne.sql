drop schema projetpae cascade;
create schema projetpae;
CREATE TABLE projetpae.adresses(
	id_adresse serial primary key,
	rue varchar(255) not null,
	numero int not null,
	code_postal int not null,
	ville varchar(50) not null,
	commune varchar(50),
	boite int,
	region varchar(50)
); 

CREATE TABLE projetpae.personnes (
	id_personne serial primary key,
	pseudo varchar(50) not null,
	nom varchar(50) not null,
	prenom varchar(50) not null,
	email varchar(50) not null,
	mot_de_passe varchar(255) not null, 
	telephone varchar(50),
	date_inscription timestamp not null,
	role varchar(2) not null,
	civilite varchar(50),
	date_naissance varchar(50),
	sexe varchar(1),
	no_compte varchar(50),
	nb_annee_reussi int,
	titulaire_compte varchar(50),
	banque varchar(50),
	code_BIC int,
	departement varchar(50),
	nationalite varchar(50),
	adresse int references projetpae.adresses(id_adresse)
);

INSERT INTO projetpae.personnes(pseudo,nom,prenom,email,mot_de_passe,date_inscription,role) VALUES('a','a','a','a@gmail.com','$2a$10$5hu4r2okjf3jhrKCm3R2kOyskoUSpldIwwDIvCusjyeb2Dx1gb442','2019-12-03','et');